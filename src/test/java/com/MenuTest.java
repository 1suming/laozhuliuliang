package com;

import com.zhuwenda.marksix.service.*;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.WxMenu;
import me.chanjar.weixin.mp.bean.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.WxMpTemplateMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class MenuTest {

    @Autowired
    private WxService wxService;

    @Value("${domain}")
    private String domain;

    @Before
    public void before(){


    }


    @Test
    public void testCreateMenu(){

        WxMenu wxMenu = new WxMenu();

        WxMenu.WxMenuButton buyViewMenu = new WxMenu.WxMenuButton();
        buyViewMenu.setName("流量充值");
        buyViewMenu.setType(WxConsts.BUTTON_VIEW);
        buyViewMenu.setUrl("http://" + domain + "/index");


        WxMenu.WxMenuButton personalViewMenu = new WxMenu.WxMenuButton();
        personalViewMenu.setName("推广中心");
        personalViewMenu.setType(WxConsts.BUTTON_VIEW);
        personalViewMenu.setUrl("http://" + domain + "/personal");

        String outerUrl = "http://gd.liuliangjia.cn:20810/wap/redBagReceive?forward=toFlowPkgActDetailsPagegd&token=F72EA84456F6E0FF734E38CBB739691425CF6EE3CCDF8401";
        WxMenu.WxMenuButton freeViewMenu = new WxMenu.WxMenuButton();
        freeViewMenu.setName("免费流量");
        freeViewMenu.setType(WxConsts.BUTTON_VIEW);
        freeViewMenu.setUrl(outerUrl);

        List<WxMenu.WxMenuButton> buttons = new ArrayList<WxMenu.WxMenuButton>();
        buttons.add(buyViewMenu);
        buttons.add(personalViewMenu);
        buttons.add(freeViewMenu);

        wxMenu.setButtons(buttons);

        try{
            System.out.println("-----------司法解释---------");
            System.out.println(wxMenu.toJson());

            wxService.menuCreate(wxMenu);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}