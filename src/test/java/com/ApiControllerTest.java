package com;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.bean.dahan.DaHanCallback;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


public class ApiControllerTest extends WebTestSupport{

    @Test
    public void testOrderlist() throws Exception{

        MvcResult result = mockMvc.perform(
                MockMvcRequestBuilders.post("/zhuowang/callback")
                        .header("Content-Type", "application/json")
                        .header("Accept", "*/*")
                        .header("Accept-Encoding", "gzip, deflate")
                        .header("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6"))

                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        System.out.println(result);
    }

}
