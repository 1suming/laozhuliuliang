package com;

import com.zhuwenda.marksix.bean.ChannelDistributionProductInfoDto;
import com.zhuwenda.marksix.enums.ChannelType;
import com.zhuwenda.marksix.service.ChannelService;
import com.zhuwenda.marksix.util.JSONUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class ChannelServiceTest {

    @Autowired
    private ChannelService channelService;


    @Test
    public void testParameter(){

        System.out.println("-------------*****************-------------");
        JSONUtil.print(channelService.queryAvailableProductForUser("ojPcIuNNMNsErIG8t1_Mn8bqi2fw", ChannelType.DIST_IN_SYSTEM));

        System.out.println("---------------------------------------");
    }

    @Test
    public void testQueryByUseridAndProductId() throws Exception{
        String userid = "ojPcIuNNMNsErIG8t1_Mn8bqi2fw";
        List<String> list = Arrays.asList("dh-lt20");
        List<ChannelDistributionProductInfoDto> ret = channelService.querySellableProducts(userid);
        JSONUtil.print(ret);
    }
}