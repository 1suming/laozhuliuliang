package com;

import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.restfulservice.Fcmsgtemplateservletresult;
import com.zhuwenda.marksix.restfulservice.PlatformProduct;
import com.zhuwenda.marksix.service.*;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.WxMenu;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.chanjar.weixin.mp.util.xml.XStreamTransformer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class Test1 {

    @Autowired
    private RechargeService rechargeService;
    @Autowired
    private WxService wxService;
    @Autowired
    private OrderService orderService;

    private String domain;

    @Test
    public void testBatchUserinfo() throws Exception{
        List<String> list = new ArrayList<>();list.add("ojPcIuNNMNsErIG8t1_Mn8bqi2fw");
        List<WxMpUser> ret = wxService.userinfoBatchget(list);
        System.out.println(ret.size());
    }

    @Test
    public void testIsvip(){
    }

    @Test
    public void testGetBalance(){

        System.out.println("-------------*****************-------------");
        System.out.println(rechargeService.getBalance());

        System.out.println("---------------------------------------");
    }
    @Test
    public void testSearchProduct(){

        System.out.println("-------------*****************-------------");
        List<PlatformProduct> list = rechargeService.searchProduct();
        System.out.println(list);
        System.out.println("---------------------------------------");
    }

    @Test
    public void testTopup(){
        //20M 000000004d9f2a6d014db6e97a2300cf
        //20M 8a28e8c2501e45630150286e08901d83
        String productid = "000000004d9f2a6d014db6e97a2300cf";
        //Map ret =rechargeService.createOrder("18576410660", "" + System.currentTimeMillis(), productid);
        //System.out.println(ret);
    }

    @Test
    public void testTemplateMsg(){
        List<Fcmsgtemplateservletresult> list =  rechargeService.searchMsgTemplate();
        System.out.println(list.size());
    }



    @Test
    public void test3(){
        /*
        PayInfo payInfo = new PayInfo();
        payInfo.setDevice_info("WEB");
        payInfo.setMch_id(mchId);
        payInfo.setNonce_str(create_nonce_str());
        payInfo.setBody(bizOrder.getProductDesc());
        payInfo.setAttach(bizOrder.getId());
        payInfo.setOut_trade_no(bizOrder.getOrderCode());
        payInfo.setTotal_fee(bizOrder.getFeeAmount());
        payInfo.setSpbill_create_ip(ip);
        payInfo.setNotify_url(notifyUrl);
        payInfo.setTrade_type("JSAPI");
        payInfo.setOpenid(openId);
        */

        String openid = "ojPcIuNNMNsErIG8t1_Mn8bqi2fw";
        String orderid = "201509280950";
        int amount = 10;
        String productDesc = "???????????";
        String tradeType = "JSAPI";
        String ip = "127.0.0.1";
        String callbackurl = "http://victor7641.eicp.net/pay/notify";
        Map<String,String> map = wxService.getJSSDKPayInfo(openid, orderid, amount,productDesc,tradeType,ip,callbackurl);
        System.out.println("--");
    }

    @Test
    public void testQueryOrder(){
        System.out.println("-------- ********** ========");
        Order order = orderService.queryOrderByOrderid("3");
        System.out.println(order.getIp());
        order = orderService.queryOrderByOrderid("4");
        System.out.println(order.getIp());
        System.out.println("-----------");
    }
    @Test
    public void testCreateOrder(){
        Order order = new Order();
        order.setOrderid("11111111111");
        order.setFeeAmount(2);
        order.setProductid("000000004d9f2a6d014db6e97a2300cf");
        order.setPayid("4");
        order.setTopupMobile("13012345678");
        order.setIp("????");
        order.setOpenid("6");
        System.out.println("????????");
        int i = orderService.createOrder(order);
        System.out.println("--------------------: "+i);
    }

    @Test
    public void testCreateMenu(){

        domain = "ll.guangxiakeji.com";


        WxMenu wxMenu = new WxMenu();

        WxMenu.WxMenuButton buyViewMenu = new WxMenu.WxMenuButton();
        buyViewMenu.setName("购买流量");
        buyViewMenu.setType(WxConsts.BUTTON_VIEW);
        buyViewMenu.setUrl("http://" + domain + "/wechat/index");

        WxMenu.WxMenuButton orderViewMenu = new WxMenu.WxMenuButton();
        orderViewMenu.setName("我的订单");
        orderViewMenu.setType(WxConsts.BUTTON_VIEW);
        orderViewMenu.setUrl("http://" + domain + "/wechat/myorder");

        List<WxMenu.WxMenuButton> buttons = new ArrayList<WxMenu.WxMenuButton>();
        buttons.add(buyViewMenu);
        buttons.add(orderViewMenu);

        wxMenu.setButtons(buttons);

        try{
            System.out.println("-----------司法解释---------");
            System.out.println(wxMenu.toJson());

            wxService.menuCreate(wxMenu);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testhello(){
        Map map = new HashMap();
        map.put("aa","jfjds");
        map.put("bb",12);
        System.out.println(XStreamTransformer.toXml(Order.class,new Order()));
    }
}