package com;


import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.UserCommission;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.UserCommissionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class UserCommissionTest {

    @Autowired
    private UserCommissionService userCommissionService;
    @Autowired
    private OrderService orderService;

    @Test
    public void testGenerateUserCommission(){
        String orderid = "201511271126581779";
        Order order = orderService.queryOrderByOrderid(orderid);
        userCommissionService.generateOrderCommission(order);
    }

    @Test
    public void testqueryAmountByGrade(){
        String openid = "2";
        long sumLevel1 = userCommissionService.queryUserCommissionByGrade(openid, UserCommission.GRADE_LEVEL1);
        long sumLevel2 = userCommissionService.queryUserCommissionByGrade(openid, UserCommission.GRADE_LEVEL2);
        long sumLevel3 = userCommissionService.queryUserCommissionByGrade(openid, UserCommission.GRADE_LEVEL3);
        System.out.println("-----------");
        System.out.println(sumLevel1);
        System.out.println(sumLevel2);
        System.out.println(sumLevel3);
        System.out.println("-----------");
    }

    @Test
    public void testUserCommission(){
        System.out.println("--------");
        System.out.println(userCommissionService.queryUserCommission("2"));
    }

}
