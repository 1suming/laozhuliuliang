package com;

import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.*;
import com.zhuwenda.marksix.dto.OrderInfoDto;
import com.zhuwenda.marksix.enums.ChannelType;
import com.zhuwenda.marksix.service.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class OrderTest extends SpringTest{

    @Autowired
    private OrderNotification orderNotification;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;

    private String domain;

    @Test
    public void testOrder() throws Exception{
        String orderid ="201601041957131246";
        Order order= orderService.queryOrderByOrderid(orderid);
        if(order != null){

                int retCode = orderService.markOrderPaySuccess(order.getOrderid());
                if(retCode == 0){
                    Product product = productService.queryByProductid(order.getProductid());
                    orderNotification.setChanged();
                    orderNotification.notify(order, product);
                }
        } else{
            System.out.println("找不到该订单号：" + orderid);
        }
    }

    @Test
    public void testCreateOrder() throws Exception{
        Order order = new Order();
        order.setOpenid("TestTestTestTestTestTest");
        Random random = new Random();
        int randomNumber = random.nextInt(999) + 1000;
        order.setOrderid(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + randomNumber);
        order.setCreateTime(new Date());
        order.setPayStatus(Constant.ORDER_PAY_STATUS_NOT_PAY);
        order.setTopupStatus(Constant.ORDER_TOPUP_STATUS_NOT_SUBMITED);
        order.setNum(3);
        order.setChannelType(ChannelType.DIST_IN_SYSTEM);
        orderService.createOrder(order);
    }

    @Test
    public void testQueryOrder() throws Exception{
        OrderQueryParam queryParam = new OrderQueryParam();
        int page = 1;
        int pageSize = 10;
        PaginationSupport<OrderInfoDto> ps = orderService.queryOrder(queryParam,page,pageSize);
        queryParam.setOpenid("TestTestTestTestTestTest");
        PaginationSupport<OrderInfoDto> ps2 = orderService.queryOrder(queryParam,page,pageSize);
        queryParam.setKeyword("2410");
        PaginationSupport<OrderInfoDto> ps3 = orderService.queryOrder(queryParam,page,pageSize);
        System.out.println(ps);
    }

    @Test
    public void testApplyRefund(){
        orderService.applyRefund("201605100006531712");
    }

    @Test
    public void testRefund(){
        orderService.refund("201605100006531712");
    }

}