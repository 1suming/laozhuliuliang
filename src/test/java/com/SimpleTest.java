package com;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.bean.dahan.DaHanCallback;
import com.zhuwenda.marksix.bean.jingnan.JingNanCallback;
import com.zhuwenda.marksix.token.NormalTokenFactory;
import com.zhuwenda.marksix.token.NormalTokenInfo;
import com.zhuwenda.marksix.token.TokenFactory;
import com.zhuwenda.marksix.token.WxTokenFactory;
import com.zhuwenda.marksix.util.HttpInterfaceUtil;
import com.zhuwenda.marksix.util.ObjectMapperFactory;
import me.chanjar.weixin.mp.bean.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.WxMpTemplateMessage;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SimpleTest {

    @Test
    public void test1() throws Exception{
        DaHanCallback daHanCallback = new DaHanCallback();
        daHanCallback.setClientOrderId("23453532");
        daHanCallback.setStatus(0);
        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println(objectMapper.writeValueAsString(daHanCallback));
    }

    @Test
    public void test2() throws Exception{
        new TemplateMsgTest().testTemplate1();
        new TemplateMsgTest().testTemplate2();
        new TemplateMsgTest().testTemplate3();
        new TemplateMsgTest().testTemplate4();
        new TemplateMsgTest().testTemplate5();
        new TemplateMsgTest().testTemplate6();
    }

    @Test
    public void test3() throws Exception{
        System.out.println(1.1 / 100);
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        String ret = decimalFormat.format(11/100.0);
        System.out.println(ret);
        BigDecimal bigDecimal = new BigDecimal("100");
        BigDecimal bigDecimal1 = new BigDecimal("100");
        String ret2 = bigDecimal.divide(bigDecimal1).toString();
        System.out.println(ret2);
        System.out.println(decimalFormat.format(bigDecimal.divide(bigDecimal1)));
        String dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        System.out.println(dateString);
    }

    @Test
    public void test4() throws Exception{
        String str = "{\"sign\":\"315C9974DE0FD8A33CBEAC1F78B2ADDE\",\"id\":\"8\",\"result\":\"1\",\"taskid\":\"jncl178201571471458188372055933751\",\"oper_time\":\"20160317150154\",\"resultdesc\":null,\"mobile\":\"17820157147\"}";
        JingNanCallback callback = new ObjectMapper().readValue(str, JingNanCallback.class);
        System.out.println(JingNanCallback.checkSing(callback,"111111"));
    }

    @Test
    public void test5() throws Exception{
        Map<String,String> map = new HashMap<>();
        map.put("c","2");
        map.put("a","1");
        map.put("b","");
        map.put("d",null);
        String a = HttpInterfaceUtil.toOrderAndNotNullValueUrlString(map);
        System.out.println(HttpInterfaceUtil.toUrlString(map));
        System.out.println(a);
    }

    @Test
    public void test6() throws Exception{
        NormalTokenInfo normalTokenInfo = new NormalTokenInfo();
        normalTokenInfo.setUserId("1");
        normalTokenInfo.setTokenTime(System.currentTimeMillis());

        NormalTokenFactory tokenFactory = new NormalTokenFactory();

        String token = tokenFactory.generateToken(normalTokenInfo);
        System.out.println(token);
        normalTokenInfo = tokenFactory.decryptToken(token);
        System.out.println(normalTokenInfo);
    }

    @Test
    public void test7() throws Exception{
        ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();
        Map<String,Object> map = new HashMap<>();
        map.put("clientOrderId", "121");
        map.put("unknowproperty", "12111");
        String str = objectMapper.writeValueAsString(map);
        System.out.println(str);
        DaHanCallback callback = objectMapper.readValue(str, DaHanCallback.class);
        System.out.println(callback);
    }
}
