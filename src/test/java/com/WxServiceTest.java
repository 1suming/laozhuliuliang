package com;


import com.zhuwenda.marksix.bean.WxRefundResult;
import com.zhuwenda.marksix.service.WxService;
import me.chanjar.weixin.mp.bean.result.WxMpTransferResult;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class WxServiceTest extends SpringTest {

    @Autowired
    private WxService wxService;

    @Test
    public void testRefund() throws Exception{
        WxRefundResult refundResult = wxService.refund("111", "3332", 10);
        System.out.println(refundResult);
    }

    @Test
    public void testTransfer() throws Exception{
        String orderid = "test2zhuwenda";
        String openid = "oniE1wxzWHDtuFS_foOvYyC1VHgQ";
        int amount = 100;
        String desc = "测试付款";
        String ip = "127.0.0.1";
        WxMpTransferResult result = wxService.transfers(orderid,openid,amount,desc,ip);
        System.out.println(result);
        Assert.assertEquals("SUCCESS",result.getResult_code());
    }
}
