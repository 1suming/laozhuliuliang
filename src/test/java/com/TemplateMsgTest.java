package com;

import com.zhuwenda.marksix.service.WxService;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.bean.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.WxMpTemplateMessage;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class TemplateMsgTest {

    @Autowired
    private WxService wxService;

    @Value("${domain}")
    private String domain;

    //订单支付成功
    @Value("${weixin.templateMsg.orderPaySuccess.id}")
    private String orderPaySuccessTemplateId;
    //订单失败提醒
    @Value("${weixin.templateMsg.orderTopupFail.id}")
    private String orderTopupFailTemplateId;
    //订单成功提醒
    @Value("${weixin.templateMsg.orderTopupSuccess.id}")
    private String orderTopupSuccessTemplateId;
    //邀请关注成功通知
    @Value("${weixin.templateMsg.inviteSubscribe.id}")
    private String inviteSubscribeTemplateId;
    //会员升级通知
    @Value("${weixin.templateMsg.beVip.id}")
    private String beVipTemplateId;
    //分销订单提成通知
    @Value("${weixin.templateMsg.getCommission.id}")
    private String getCommissionTemplateId;
    
    //private String toUserTest = "ojPcIuNNMNsErIG8t1_Mn8bqi2fw";//我的测试公众号
    private String toUserTest = "oniE1wxzWHDtuFS_foOvYyC1VHgQ";//生成公众号

    @Before
    public void before(){
    }

    @Test
    public  void test1() throws Exception{
        try {

            String templateid = "1BShMWqhXAYvG3IdkVP6zoQtJOMd0DVDsjkzkL-eZKs";
            String toUser = "ojPcIuCc3NFvM62dAwy6LzlDVfTE";
            WxMpUser wxMpUser = wxService.userInfo(toUser, null);
            System.out.println("hello");
        }catch (WxErrorException e){
            e.printStackTrace();;
        }
//        testTemplate1();
//        testTemplate2();
//        testTemplate3();
//        testTemplate4();
//        testTemplate5();
//        testTemplate6();
    }

    public void testTemplate1() throws Exception{
        //支付成功通知

        String templateId = orderPaySuccessTemplateId;
        String toUser = toUserTest;
        String first = "我们已收到您的货款，正在为您充值，请耐心等待: )";
        String orderMoneySum = "30.00元";
        String orderProductName = "我是商品名字";
        String remark = "如有问题请致电18824107788或直接在微信留言，小易将第一时间为您服务！";
        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("orderMoneySum", orderMoneySum));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("orderProductName", orderProductName));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("Remark", remark));
        String json = wxMpTemplateMessage.toJson();
        System.out.println(json);
        wxService.templateSend(wxMpTemplateMessage);

    }
    public void testTemplate2() throws Exception{
        //订单失败提醒
        String templateId = orderTopupFailTemplateId;
        String toUser = toUserTest;
        String first = "您好，您的充值已失败。";
        String keyword1 = "1233";
        String keyword2 = "30M移动";
        String keyword3 = "18824107788";
        String keyword4 = "2015-1-3 16:15:24";
        String remark = "我们将于两小时内为您退款，请注意查收。有疑问请咨询在线客服";

        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1", keyword1));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2",keyword2));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3",keyword3));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword4",keyword4));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark",remark));
        String json = wxMpTemplateMessage.toJson();
        System.out.println(json);

        wxService.templateSend(wxMpTemplateMessage);
    }

    public void testTemplate3() throws Exception{
        //订单成功提醒

        String templateId = orderTopupSuccessTemplateId;
        String toUser = toUserTest;
        String first = "亲，您的订单已经充值成功！感谢支持哦";
        String keyword1 = "1234";
        String keyword2 = "50元话费";
        String keyword3 = "318824107788";
        String keyword4 = "2015-1-3 16:15:24";
        String remark = "如有疑问，请咨询18824107788";

        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1", keyword1));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2",keyword2));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3",keyword3));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword4",keyword4));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark",remark));
        String json = wxMpTemplateMessage.toJson();
        System.out.println(json);

        wxService.templateSend(wxMpTemplateMessage);
    }
    
    public void testTemplate4() throws Exception{
        //邀请关注成功提醒
        String templateId = inviteSubscribeTemplateId;
        String toUser = toUserTest;
        String first = "您好，以下会员是通过您的二维码关注我们的：";
        String keyword1 = "被推荐人";
        String keyword2 = "2015-1-3 16:15:24";
        String keyword3 = "推荐人";
        String remark = "如有疑问，请播打我们的400号码";

        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1",keyword1));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2",keyword2));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3",keyword3));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark",remark));
        String json = wxMpTemplateMessage.toJson();
        System.out.println(json);

        wxService.templateSend(wxMpTemplateMessage);
    }

    
    public void testTemplate5() throws Exception{
        //分销订单提成通知
        //（通知三级推荐人，分成金额按照比例折算）
        String templateId = beVipTemplateId;
        String toUser = toUserTest;
        String first = "尊敬的XXX, 恭喜你成功升级为VIP会员。";
        String keyword1 = "1234567890";
        String keyword2 = "永久";
        String remark = "vip用户享受最低价充值优惠。详询18824107788。";

        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1",keyword1));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2",keyword2));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark",remark));
        String json = wxMpTemplateMessage.toJson();
        System.out.println(json);

        wxService.templateSend(wxMpTemplateMessage);
    }
    public void testTemplate6() throws Exception{
        //分销订单提成通知
        //（通知三级推荐人，分成金额按照比例折算）
        String templateId = getCommissionTemplateId;
        String toUser = toUserTest;
        String first = "亲，您又成功分销出一笔订单了";
        String keyword1 = "YDW2014989800";
        String keyword2 = "300元";
        String keyword3 = "30元";
        String keyword4 = "2015-1-3 16:15:24";
        String remark = "【掌上流量平台】感谢有您，客服热线：18824107788";

        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1", keyword1));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2",keyword2));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3",keyword3));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword4",keyword4));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark",remark));
        String json = wxMpTemplateMessage.toJson();
        System.out.println(json);

        wxService.templateSend(wxMpTemplateMessage);
    }
}