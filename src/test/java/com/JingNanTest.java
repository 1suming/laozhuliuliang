package com;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.bean.jingnan.JingNanCallback;
import com.zhuwenda.marksix.bean.jingnan.JingNanChargeResult;
import com.zhuwenda.marksix.util.DigestTools;
import me.chanjar.weixin.common.util.http.Utf8ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Assert;
import org.junit.Test;

import java.net.URLEncoder;

public class JingNanTest {
    //http://www.hfsdkj.com/flowplatform/flowservice?action=getBalance&v=1.0&account=jncl&sign=56E86F7E0718CFC2F80373EFDEEFB27A
    //http://www.hfsdkj.com/flowplatform/flowservice?action=getPackage&v=1.0&account=jncl&type=0&sign=95F65B909253168D60D35561D49A3FCE



    @Test
    public void testSign() throws Exception{
        String sign = "{\"sign\":\"315C9974DE0FD8A33CBEAC1F78B2ADDE\",\"id\":\"8\",\"result\":\"1\",\"taskid\":\"jncl178201571471458188372055933751\",\"oper_time\":\"20160317150154\",\"resultdesc\":null,\"mobile\":\"17820157147\"}";
        JingNanCallback callback = new ObjectMapper().readValue(sign, JingNanCallback.class);
        boolean b = JingNanCallback.checkSing(callback,"111111");
        System.out.println(b);
    }

    @Test
    public void testCharge() throws Exception{
        JingNanChargeResult result = charge("15913574134","24233423");
        Assert.assertNotNull(result);
    }


    public JingNanChargeResult charge(String mobile,String productid){
        try{

            String api = "http://www.hfsdkj.com/flowplatform/flowservice";
            String account = "jncl";
            String key = "111111";
            String notifyUrl = "http://zhuwenda.ngrok.natapp.cn/jingnan/callback";
            String url = api + "?action=charge&v=1.0&account="+account+"&mobile="+mobile+"&id="+productid+"&notifyurl="+URLEncoder.encode(notifyUrl);
            String toBeMd5 = "account="+account+"&id="+productid+"&mobile="+mobile+"&notifyurl="+notifyUrl+"&key="+key;
//            String url = api + "?action=charge&v=1.0&account="+account+"&mobile="+mobile+"&id="+productid;
//            String toBeMd5 = "account="+account+"&id="+productid+"&mobile="+mobile+"&key="+key;

            url += "&sign="+ DigestTools.md5String(toBeMd5.toLowerCase()).toUpperCase();
            System.out.println(url);
            HttpGet httpGet = new HttpGet(url);
            CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(httpGet);
            String responseContent = Utf8ResponseHandler.INSTANCE.handleResponse(response);
            System.out.println(responseContent);
            ObjectMapper objectMapper = new ObjectMapper();
            JingNanChargeResult result = objectMapper.readValue(responseContent,JingNanChargeResult.class);

            return result;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
