package com;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.bean.hotdata.HotDataResult;
import com.zhuwenda.marksix.util.DigestTools;
import me.chanjar.weixin.common.util.http.Utf8ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class HotdataTest {

    @Value("${liuliang.hotdata.api}")
    private String api;
    @Value("${liuliang.hotdata.openid}")
    private String openid;
    @Value("${liuliang.hotdata.secret}")
    private String secret;
    @Value("${liuliang.hotdata.appkey}")
    private String appkey;
    @Value("${liuliang.hotdata.notifyUrl}")
    private String notifyUrl;


    @Test
    public void myTest() throws Exception{


        Map<String,Object> data = new TreeMap<String,Object>();
        data.put("method", "FlowRechargePPS");
        data.put("openid", openid);
        data.put("appkey", appkey);
        data.put("mobile", "13414559185");
        data.put("notify_url", URLEncoder.encode(notifyUrl));
        data.put("product_id", "10001");
        data.put("product_value", "10");
        data.put("timestamp", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        System.out.println("请求参数:");
        data.entrySet().stream().forEach(System.out::println);
        String str = data.entrySet().stream()
                .sorted((a,b)-> a.getKey().compareTo(b.getKey()))
                .map(a->a.toString())
                .reduce("", (a, b) -> a.concat("&").concat(b))
                ;
        String concatParamString = str.substring(1, str.length());
        System.out.println("升序排序并用&连接："+concatParamString);
        String concatSecretString = concatParamString + secret;
        System.out.println("拼接双方约定的密钥："+concatSecretString);
        String lowerString = concatSecretString.toLowerCase();
        System.out.println("转化为小写:"+lowerString);
        String sign = DigestTools.md5String(lowerString);
        System.out.println("计算签名字符串："+sign);

        data.put("sign",sign);

        ObjectMapper ob = new ObjectMapper();
        String postData = ob.writeValueAsString(data);

        HttpPost httpPost = new HttpPost(api);

        List<BasicNameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("data",postData));
        ;
        System.out.println("请求实体：");
        System.out.println(data);
        httpPost.setEntity(new UrlEncodedFormEntity(param,"UTF-8"));
        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(httpPost);
            String responseContent = Utf8ResponseHandler.INSTANCE.handleResponse(response);
            System.out.println("响应报文：-----------------");
            System.out.println(responseContent);
            ObjectMapper objectMapper = new ObjectMapper();
            HotDataResult result = objectMapper.readValue(responseContent, HotDataResult.class);
            System.out.println(result.getCode());
            System.out.println("响应报文：-----------------");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
