package com;


import com.zhuwenda.marksix.bean.Withdraw;
import com.zhuwenda.marksix.service.WithdrawService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class WithdrawTest {

    @Autowired
    private WithdrawService withdrawService;


    @Test
    public void testAddUser(){
        Withdraw withdraw = new Withdraw();
        withdraw.setOpenid("ojPcIuNNMNsErIG8t1_Mn8bqi2fw");
        withdraw.setAccount("123@qq.com");
        withdraw.setAmount(100);
        withdraw.setStatusMsg("applying");
        withdraw.setCreateTime(new Date());
        withdraw.setStatus(Withdraw.STATUS_APPLYING);
        Assert.assertEquals(0,withdrawService.add(withdraw));
    }

    @Test
    public void testFrozenBalance(){
        System.out.println("------------");
        System.out.println(withdrawService.queryUserFrozenBalance("2"));
    }

}
