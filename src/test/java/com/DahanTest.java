package com;


import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.bean.dahan.DaHanProductCarrierEnum;
import com.zhuwenda.marksix.bean.dahan.DaHanResult;
import com.zhuwenda.marksix.service.handler.DahanOrderHandler;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.ProductService;
import com.zhuwenda.marksix.service.RechargeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class DahanTest {

    @Autowired
    private RechargeService rechargeService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private DahanOrderHandler dahanOrderHandler;
    @Autowired
    private ProductService productService;


    @Test
    public void testCreateOrder() throws Exception{

        DaHanResult result = rechargeService.createOrder("18576410660","20160306175100", DaHanProductCarrierEnum.DAHAN_CUCC,"8a28e8c2501e45630150286e08901d83","20");
        System.out.println(result);
        Assert.assertEquals(0,result.getResultCode());

    }

    /**
     * 可以手动充值订单
     */
    @Test
    public void manualTopup() throws Exception{
        String orderid = "201601221359161257";

        Order order = orderService.queryOrderByOrderid(orderid);
        Product product = productService.queryByProductid(order.getProductid());

        dahanOrderHandler.handle(order,product,null);

    }
}
