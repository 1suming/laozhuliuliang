package com.zhuwenda.marksix.rest;

import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.OrderQueryParam;
import com.zhuwenda.marksix.bean.PaginationSupport;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.bean.result.Result;
import com.zhuwenda.marksix.bean.result.ResultBuilder;
import com.zhuwenda.marksix.dto.OrderInfoDto;
import com.zhuwenda.marksix.enums.ChannelType;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.ProductService;
import com.zhuwenda.marksix.util.BeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@SessionAttributes({"openid","isManager"})
public class ApiController {

    private static final Logger logger = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/orderlist")
    @ResponseBody
    public Result myorderlist(@ModelAttribute("isManager") boolean isManager, @ModelAttribute("openid") String openid,
                              String orderid,String keyword,String page,String pageSize){

        OrderQueryParam orderQueryParam = new OrderQueryParam();
        orderQueryParam.setNotPayStatus(Constant.ORDER_PAY_STATUS_NOT_PAY);

        if(StringUtils.hasText(orderid)){
            orderQueryParam.setOrderid(orderid);
        }
        if(StringUtils.hasText(keyword)){
            orderQueryParam.setKeyword(keyword);
        }
        if(!isManager){
            orderQueryParam.setOpenid(openid);
        }
        int pageInt = 1;
        try {
            pageInt = Integer.parseInt(page);
        }catch (NumberFormatException e){
            logger.warn("输入页码参数违法,参数值为:{}，取默认值1",page);
        }
        int pageSizeInt = 10;
        try {
            pageSizeInt = Integer.parseInt(pageSize);
        }catch (NumberFormatException e){
            logger.warn("输入每页数量参数违法,参数值为:{}，取默认值10",pageSize);
        }
        PaginationSupport<OrderInfoDto> ps = orderService.queryOrder(orderQueryParam,pageInt,pageSizeInt);
        Map<String,Object> result = new HashMap<>();
        result.put("ps", ps);
        result.put("isManager", isManager);
        return ResultBuilder.success(result);
    }

    @RequestMapping(value = "/channelOrderlist")
    @ResponseBody
    public Result channelOrderlist(@ModelAttribute("isManager") boolean isManager, @ModelAttribute("openid") String openid,
                              String orderid,String keyword,String page,String pageSize){

        OrderQueryParam orderQueryParam = new OrderQueryParam();
        orderQueryParam.setNotPayStatus(Constant.ORDER_PAY_STATUS_NOT_PAY);
        if(StringUtils.hasText(orderid)){
            orderQueryParam.setOrderid(orderid);
        }
        if(StringUtils.hasText(keyword)){
            orderQueryParam.setKeyword(keyword);
        }
        orderQueryParam.setChannelId(openid);
        orderQueryParam.setChannelType(ChannelType.DIST_IN_SYSTEM);
        int pageInt = 1;
        try {
            pageInt = Integer.parseInt(page);
        }catch (NumberFormatException e){
            logger.warn("输入页码参数违法,参数值为:{}，取默认值1",page);
        }
        int pageSizeInt = 10;
        try {
            pageSizeInt = Integer.parseInt(pageSize);
        }catch (NumberFormatException e){
            logger.warn("输入每页数量参数违法,参数值为:{}，取默认值10",pageSize);
        }
        PaginationSupport<OrderInfoDto> ps = orderService.queryOrder(orderQueryParam,pageInt,pageSizeInt);
        Map<String,Object> result = new HashMap<>();
        result.put("ps", ps);
        result.put("isManager", isManager);
        return ResultBuilder.success(result);
    }



}
