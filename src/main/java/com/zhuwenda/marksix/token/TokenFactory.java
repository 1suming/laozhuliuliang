package com.zhuwenda.marksix.token;


public interface TokenFactory<T> {

    public String generateToken(T tokenObject);

    public T decryptToken(String tokenString);
}
