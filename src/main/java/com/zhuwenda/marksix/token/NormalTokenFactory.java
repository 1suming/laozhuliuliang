package com.zhuwenda.marksix.token;


public class NormalTokenFactory implements TokenFactory<NormalTokenInfo> {

    private EncryptTokenFactory encryptTokenFactory;

    public NormalTokenFactory(){
        this.encryptTokenFactory = new EncryptTokenFactory();
    }

    @Override
    public String generateToken(NormalTokenInfo tokenObject) {
        String token = tokenObject.getUserId()+":"+tokenObject.getTokenTime();
        return encryptTokenFactory.generateToken(token);
    }

    @Override
    public NormalTokenInfo decryptToken(String tokenString) {
        String token = encryptTokenFactory.decryptToken(tokenString);
        String[] split = token.split(":");
        NormalTokenInfo normalTokenInfo = new NormalTokenInfo();
        normalTokenInfo.setUserId(split[0]);
        normalTokenInfo.setTokenTime(Long.parseLong(split[1]));
        return normalTokenInfo;
    }
}
