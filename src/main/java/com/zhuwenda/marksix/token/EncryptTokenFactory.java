package com.zhuwenda.marksix.token;

import java.nio.charset.Charset;
import java.util.Random;


public class EncryptTokenFactory implements TokenFactory<String> {

    /** 生成一个随机密钥 */
    private static final String encryptKey = "" + System.currentTimeMillis() + new Random().nextInt();

    private Charset charset = Charset.forName("UTF-8");

    @Override
    public String generateToken(String content) {
        return AesUtil.parseByte2HexStr(AesUtil.encrypt(content.getBytes(charset), encryptKey));
    }

    @Override
    public String decryptToken(String token) {
        return new String(AesUtil.decrypt(AesUtil.parseHexStr2Byte(token), encryptKey), charset);
    }
}
