package com.zhuwenda.marksix;


public class Constant {

    /** 推广二维码过期刷新时间 */
    public static final int QRCODE_MAX_EXPIRE_SECONDS = 2592000;

    /** 订单充值状态：已提交 */
    public static final int ORDER_TOPUP_STATUS_NOT_SUBMITED = 1;
    /** 订单充值状态：充值中 */
    public static final int ORDER_TOPUP_STATUS_TOPUPING = 3;
    /** 订单充值状态：充值成功 */
    public static final int ORDER_TOPUP_STATUS_SUCCESS = 5;
    /** 订单充值状态：充值失败 */
    public static final int ORDER_TOPUP_STATUS_FAILED = 7;

    /** 订单支付状态：未支付 */
    public static final int ORDER_PAY_STATUS_NOT_PAY = 0;
    /** 订单支付状态：支付成功 */
    public static final int ORDER_PAY_STATUS_SUCCESS = 1;
    /** 订单支付状态：支付已申请退款 */
    public static final int ORDER_PAY_STATUS_APPLY_REFUND = 3;
    /** 订单支付状态：支付已申请退款成功 */
    public static final int ORDER_PAY_STATUS_REFUND_SUCCESS = 5;
    /** 订单支付状态：支付已申请退款失败 */
    public static final int ORDER_PAY_STATUS_REFUND_FAILED = 7;

}
