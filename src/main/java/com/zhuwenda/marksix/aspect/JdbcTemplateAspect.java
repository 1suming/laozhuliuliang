package com.zhuwenda.marksix.aspect;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class JdbcTemplateAspect {

    @Pointcut("execution(* org.springframework.jdbc.core.JdbcTemplate.queryFor*(..))")
    public void JdbcTemplateAspectPointcut() {
    }

    @Around("JdbcTemplateAspectPointcut()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        try {
            return pjp.proceed();
        }catch (EmptyResultDataAccessException e){
            return null;
        }
    }


}
