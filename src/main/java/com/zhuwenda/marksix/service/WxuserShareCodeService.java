package com.zhuwenda.marksix.service;

import com.zhuwenda.marksix.bean.WxuserShareCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class WxuserShareCodeService {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public WxuserShareCode queryByShareCode(String shareCode,int shareType){
        String sql = "select openid from t_wxuser_share_code where share_code=? and share_type=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{shareCode,shareType}, new BeanPropertyRowMapper<WxuserShareCode>(WxuserShareCode.class));
    }
    public WxuserShareCode queryByShareCode(String shareCode){
        String sql = "select openid from t_wxuser_share_code where share_code=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{shareCode}, new BeanPropertyRowMapper<WxuserShareCode>(WxuserShareCode.class));
    }
    public WxuserShareCode queryByOpenid(String openid,int shareType){
        String sql = "select * from t_wxuser_share_code where openid=? and share_type=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{openid,shareType}, new BeanPropertyRowMapper<WxuserShareCode>(WxuserShareCode.class));
    }


    public String createIntroduceShareCode(String openid){
        WxuserShareCode wxuserShareCode = queryByOpenid(openid,WxuserShareCode.SHARE_TYPE_INTRODUCE);
        if (wxuserShareCode == null || wxuserShareCode.getShareCode() == null || "".equals(wxuserShareCode.getShareCode().trim())){
            String shareCode = null;
            while (true){
                //TODO 限定区间
                String randomShareCode = ""+ new Random().nextInt(1000000);
                if(queryByShareCode(randomShareCode) == null){
                    shareCode = randomShareCode;
                    break;
                }
            }
            String sql = "insert into t_wxuser_share_code(openid,share_code,share_type) VALUES (?,?,?)";
            return jdbcTemplate.update(sql, new Object[]{openid,shareCode,WxuserShareCode.SHARE_TYPE_INTRODUCE}) == 1 ? shareCode : null;
        }else {
            return wxuserShareCode.getShareCode();
        }
    }
}
