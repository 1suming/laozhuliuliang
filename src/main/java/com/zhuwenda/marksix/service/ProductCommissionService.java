package com.zhuwenda.marksix.service;

import com.zhuwenda.marksix.bean.ProductCommission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProductCommissionService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public ProductCommission queryByProductid(String productid){
        String sql = "select * from t_product_commission where productid=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{productid}, new BeanPropertyRowMapper<ProductCommission>(ProductCommission.class));
    }

}
