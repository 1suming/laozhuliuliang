package com.zhuwenda.marksix.service;

import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.*;
import com.zhuwenda.marksix.dto.OrderInfoDto;
import com.zhuwenda.marksix.enums.ChannelType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private WxTemplateMsgService wxTemplateMsgService;
    @Autowired
    private UserCommissionService userCommissionService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ParameterService parameterService;
    @Autowired
    private BillService billService;
    @Autowired
    private WxService wxService;


    public Order queryOrderByOrderid(String orderid){
        String sql = "select * from t_order where orderid = ?";
        List<Order> ret = jdbcTemplate.query(sql, new Object[]{orderid}, new BeanPropertyRowMapper<Order>(Order.class));
        return ret.size() == 1 ? ret.get(0) : null;
    }

    public Order queryOrderByOuterOrderid(String outerOrderid){
        String sql = "select * from t_order where topup_orderid = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{outerOrderid}, new BeanPropertyRowMapper<Order>(Order.class));
    }

    public Order queryOrder(String orderid,String openid){
        String sql = "select * from t_order where orderid = ? and openid = ? order by create_time desc";
        List<Order> ret = jdbcTemplate.query(sql, new Object[]{orderid, openid}, new BeanPropertyRowMapper<Order>(Order.class));
        return ret.size() == 1 ? ret.get(0) : null;
    }

    public List<Order> queryOrder(String openid){
        String sql = "select * from t_order where openid = ? order by create_time desc";
        List<Order> ret = jdbcTemplate.query(sql, new Object[]{openid}, new BeanPropertyRowMapper<Order>(Order.class));
        return ret;
    }



    public List<Order> queryAllOrder(){
        String sql = "select * from t_order order by create_time desc";
        List<Order> ret = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Order>(Order.class));
        return ret;
    }

    public PaginationSupport<OrderInfoDto> queryOrder(OrderQueryParam orderQueryParam,Integer page,Integer pageSize){
        if(page == null || page <= 0){
            throw new IllegalArgumentException("页码必须大于0");
        }
        if(pageSize == null || pageSize <= 0){
            throw new IllegalArgumentException("每页数量必须大于0");
        }
        int offset = (page-1) * pageSize;

        StringBuffer sqlBase = new StringBuffer();
        sqlBase.append("select t1.*,t2.name as productName from t_order t1 left join t_product t2 on t1.productid=t2.productid where 1=1 ");
        List sqlParam = new ArrayList<>();
        if(orderQueryParam != null){
            if(StringUtils.hasText(orderQueryParam.getKeyword())){
                sqlBase.append(" and (t1.orderid like ? or t1.topup_mobile like ? ) ");
                sqlParam.add("%" + orderQueryParam.getKeyword() + "%");
                sqlParam.add("%" + orderQueryParam.getKeyword() + "%");
            }
            if(StringUtils.hasText(orderQueryParam.getOrderid())){
                sqlBase.append(" and t1.orderid = ? ");
                sqlParam.add(orderQueryParam.getOrderid());
            }
            if(StringUtils.hasText(orderQueryParam.getOpenid())){
                sqlBase.append(" and t1.openid = ? ");
                sqlParam.add(orderQueryParam.getOpenid());
            }
            if(orderQueryParam.getPayStatus() != null){
                sqlBase.append(" and t1.pay_status = ? ");
                sqlParam.add(orderQueryParam.getPayStatus());
            }
            if(orderQueryParam.getNotPayStatus() != null){
                sqlBase.append(" and t1.pay_status != ? ");
                sqlParam.add(orderQueryParam.getNotPayStatus());
            }
            if(orderQueryParam.getChannelType() != null){
                sqlBase.append(" and t1.channel_type = ? ");
                sqlParam.add(orderQueryParam.getChannelType().toString());
            }
            if(orderQueryParam.getChannelId() != null){
                sqlBase.append(" and t1.channel_id = ? ");
                sqlParam.add(orderQueryParam.getChannelId());
            }
        }
        sqlBase.append(" order by t1.create_time desc ");


        StringBuffer sqlForList = new StringBuffer(sqlBase);
        sqlForList.append(" limit ").append(offset).append(",").append(pageSize);


        StringBuffer sqlForCount = new StringBuffer();
        sqlForCount.append("select count(*) from (");
        sqlForCount.append(sqlBase);
        sqlForCount.append(") t_count");

        List<OrderInfoDto> ret = jdbcTemplate.query(sqlForList.toString(), sqlParam.toArray(), new BeanPropertyRowMapper<OrderInfoDto>(OrderInfoDto.class));
        long count = jdbcTemplate.queryForLong(sqlForCount.toString(),sqlParam.toArray());

        return new PaginationSupport<>(ret,new Pager(page,pageSize,count));
    }

    /** 成功时返回 0 **/
    public int markOrderPaySuccess(String orderid){
        String sql = "update t_order set pay_status=1 where orderid = ?";
        int num = jdbcTemplate.update(sql, orderid);
        return num == 1 ? 0 : 1;
    }

    /** 成功时返回 0 */
    public int markOrderTopupStatus(String orderid,int status,String topupOrderid){

        String sql = "update t_order set topup_status=?,topup_orderid = ? where orderid = ?";
        int num = jdbcTemplate.update(sql,new Object[]{status,topupOrderid,orderid});
        return num == 1 ? 0 : 1;
    }
    /** 成功时返回 0 */
    public int markOrderTopupStatus(String orderid,int status){

        String sql = "update t_order set topup_status=? where orderid = ?";
        int num = jdbcTemplate.update(sql,new Object[]{status,orderid});
        return num == 1 ? 0 : 1;
    }

    /** 成功时返回 0 */
    public int markOrderTopupOrderid(String orderid,String topupOrderid){

        String sql = "update t_order set topup_orderid = ? where orderid = ?";
        int num = jdbcTemplate.update(sql,new Object[]{topupOrderid,orderid});
        return num == 1 ? 0 : 1;
    }

    /** 成功时返回 0 */
    public int markOrderPayid(String orderid,String payid){

        String sql = "update t_order set payid=? where orderid = ?";
        int num = jdbcTemplate.update(sql,new Object[]{payid,orderid});
        return num == 1 ? 0 : 1;
    }

    /** 成功时返回 0 */
    public int updateOuterOrderInfo(String orderid, String outerOrderid, String outerMessage){

        String sql = "update t_order set topup_orderid=?,outer_message=? where orderid = ?";
        int num = jdbcTemplate.update(sql,new Object[]{outerOrderid,outerMessage,orderid});
        return num == 1 ? 0 : 1;
    }

    /** 成功时返回 0 */
    public int updateOuterOrderInfo(String orderid, String outerMessage){

        String sql = "update t_order set outer_message=? where orderid = ?";
        int num = jdbcTemplate.update(sql, new Object[]{outerMessage, orderid});
        return num == 1 ? 0 : 1;
    }

    /** 成功时返回 0 */
    public int updateOuterOrderInfo(String orderid, int status,String outerMessage){

        String sql = "update t_order set outer_message=?,topup_status=? where orderid = ?";
        int num = jdbcTemplate.update(sql, new Object[]{outerMessage, status, orderid});
        return num == 1 ? 0 : 1;
    }

    public int createOrder(Order order) {
        String sql = "INSERT INTO `t_order`" +
                " (`orderid`, `fee_amount`, `num`, `productid`, `pay_status`" +
                ", `topup_status`, `topup_mobile`, `payid`, `create_time`, `ip`, `openid`, `source_openid`, `channel_type`, `channel_id`)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, now(), ?, ?, ?, ?,?)";
        int num = jdbcTemplate.update(sql, new Object[]{order.getOrderid(),order.getFeeAmount(),order.getNum(),order.getProductid(),Constant.ORDER_PAY_STATUS_NOT_PAY,
                order.getTopupStatus(),order.getTopupMobile(),order.getPayid(), order.getIp(),order.getOpenid(),order.getSourceOpenid()
                ,order.getChannelType(),order.getChannelId()});
        return num == 1 ? 0 : 1;
    }


    /** 充值失败通知 */
    @Transactional
    public void orderFailedNotification(Order order,Product product,String errorMsg,TopupFailType type){

        if(TopupFailType.PRODUCT_OR_ACCOUNT_EXCEPTION.equals(type)){
            logger.warn("id为{}的产品异常，错误消息：", product.getProductid(),errorMsg);
            if(product.getIsAutoDown() == 1){
                productService.downProduct(product.getProductid());
                updateOuterOrderInfo(order.getOrderid(), "（已自动下架）" + errorMsg);
                logger.info("符合自动下架规则，自动下架成功");
            }
        }

        updateOuterOrderInfo(order.getOrderid(), Constant.ORDER_TOPUP_STATUS_FAILED, errorMsg);
        wxTemplateMsgService.orderTopupFail(order.getOpenid(), order.getOrderid(), product != null ? product.getName() : "", order.getTopupMobile(), order.getCreateTime());

        //自动退款
        Parameter autoRefundSwitch = parameterService.queryByKey(Parameter.KEY_AUTO_REFUND_SWITCH);
        if(autoRefundSwitch != null && "ON".equalsIgnoreCase(autoRefundSwitch.getParamValue())){
            Parameter parameter = parameterService.queryByKey(Parameter.KEY_AUTO_REFUND_LIMIT_VALUE);
            if(parameter != null && StringUtils.hasText(parameter.getParamValue())){
                try {
                    int limit = Integer.parseInt(parameter.getParamValue());
                    if(order.getFeeAmount() <= limit){
                        try {

                            applyRefund(order.getOrderid());//申请退款
                            refund(order.getOrderid());//退款
                            //TODO 在自动退款成功的时候执行下面的逻辑
                            if(ChannelType.DIST_IN_SYSTEM.equals(order.getChannelType())){
                                logger.info("分销商订单，自动扣款");
                                // 如果是分销商渠道订单，则自动扣掉分销商的钱
                                if(!StringUtils.hasText(order.getChannelId())){
                                    logger.error("严重，找不到渠道ID信息，无法自动扣款");
                                }else {
                                    billService.distAutoPay(order.getChannelId(),order.getFeeAmount(),order.getOrderid(),"充值失败，自动退款");
                                }
                            }
                        }catch (RuntimeException e){
                            logger.error("退款异常",e);
                        }
                    }
                }catch (NumberFormatException e){
                    logger.error("自动退款限制值参数设置错误，不能转化为有效数字");
                }
            }
        }

    }

    /**
     * 申请退款
     * @param order 订单信息
     * @return 退款单号
     */
    public String applyRefund(Order order){
        logger.info("申请退款，订单号：{}", order.getOrderid());
        if(order.getPayStatus() == Constant.ORDER_PAY_STATUS_SUCCESS && order.getTopupStatus() == Constant.ORDER_TOPUP_STATUS_FAILED){
            Random random = new Random();
            int randomNumber = random.nextInt(999) + 1000;
            String refundNo = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + randomNumber;
            String sql = "update t_order set refund_no=?,pay_status=? where orderid = ?";
            int num = jdbcTemplate.update(sql, new Object[]{refundNo,Constant.ORDER_PAY_STATUS_APPLY_REFUND, order.getOrderid()});
            return refundNo;
        }else {
            logger.warn("订单不符合退款条件，订单号：{}",order.getOrderid());
            throw new IllegalStateException("订单不符合退款条件");
        }
    }

    /** 申请退款 */
    public void applyRefund(String orderId){
        Order order = queryOrderByOrderid(orderId);
        applyRefund(order);
    }

    /** 退款 */
    public void refund(String orderId){
        Order order = queryOrderByOrderid(orderId);
        refund(order);
    }

    /** 退款 */
    public String refund(Order order){
        if(order.getPayStatus() == Constant.ORDER_PAY_STATUS_APPLY_REFUND){

            WxRefundResult result = wxService.refund(order.getOrderid(), order.getRefundNo(), order.getFeeAmount());
            if ("SUCCESS".equalsIgnoreCase(result.getReturn_code()) && "SUCCESS".equalsIgnoreCase(result.getResult_code())) {
                String refundId = result.getRefund_id();
                String sql = "update t_order set refund_id=?,pay_status=? where orderid = ?";
                int num = jdbcTemplate.update(sql, new Object[]{refundId,Constant.ORDER_PAY_STATUS_REFUND_SUCCESS, order.getOrderid()});
                return refundId;
            }else {
                logger.warn("微信退款失败：orderid:{},returnMsg:{},err_code_des:{}",order.getOrderid(),result.getReturn_msg(),result.getErr_code_des());

                String sql = "update t_order set pay_status=? where orderid = ?";
                jdbcTemplate.update(sql, new Object[]{Constant.ORDER_PAY_STATUS_REFUND_FAILED, order.getOrderid()});
                throw new IllegalStateException("微信端退款失败");
            }
        }else {
            throw new IllegalStateException("订单状态异常，退款失败");
        }
    }

    /** 充值成功通知（注意，是充值平台充值回调时调用) */
    @Transactional
    public void orderSuccessNotification(Order order,Product product){
        markOrderTopupStatus(order.getOrderid(), Constant.ORDER_TOPUP_STATUS_SUCCESS);
        logger.info("标记订单充值状态为成功");
        wxTemplateMsgService.orderTopupSuccess(order.getOpenid(), order.getOrderid(), product != null ? product.getName() : "", order.getTopupMobile(), order.getCreateTime());
        userCommissionService.generateOrderCommission(order);
        logger.info("生成订单佣金成功");
        if(ChannelType.DIST_IN_SYSTEM.equals(order.getChannelType())){//分销商订单
            //分销商自动扣款
            billService.distAutoPay(order.getChannelId(),order.getFeeAmount(),order.getOrderid());
        }
    }

}
