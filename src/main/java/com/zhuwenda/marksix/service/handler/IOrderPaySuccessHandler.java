package com.zhuwenda.marksix.service.handler;


import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;

import java.util.Map;
import java.util.Observer;

public interface IOrderPaySuccessHandler {

    public boolean shouldHandle(Product product);

    public void handle(Order order,Product product,Map<String,Object> data);

}
