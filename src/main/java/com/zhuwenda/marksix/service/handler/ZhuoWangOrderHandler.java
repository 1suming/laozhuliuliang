package com.zhuwenda.marksix.service.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.bean.TopupFailType;
import com.zhuwenda.marksix.bean.zhuowang.ZhuoWangResult;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.ProductService;
import com.zhuwenda.marksix.util.HttpInterfaceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ZhuoWangOrderHandler implements IOrderPaySuccessHandler {

    private static final Logger logger = LoggerFactory.getLogger(ZhuoWangOrderHandler.class);

    @Value("${liuliang.zhuowang.api}")
    private String api;
    @Value("${liuliang.zhuowang.portalId}")
    private String portalId;
    @Value("${liuliang.zhuowang.companyCode}")
    private String companyCode;
    @Value("${liuliang.zhuowang.key}")
    private String key;
    @Value("${liuliang.zhuowang.portalType}")
    private String portalType;
    @Value("${liuliang.zhuowang.notifyUrl}")
    private String notifyUrl;




    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;

    @Override
    public void handle(Order order, Product product, Map<String, Object> data) {
        logger.info("准备提交流量充值请求，订单号：{}",order.getOrderid());

        String productid = product.getOuterProductid();
        String productValue = product.getOuterExtend1();
        String mobile = order.getTopupMobile();
        ZhuoWangResult result = charge(mobile,productid,productValue);
        if("0".equals(result.getCode())){
            logger.info("流量充值提交成功");
            orderService.markOrderTopupStatus(order.getOrderid(), Constant.ORDER_TOPUP_STATUS_TOPUPING, result.getSequence());
            logger.info("标记订单状态为充值中成功");
        }
        else if ("64002".equals(result.getCode()) || "10006".equals(result.getCode()) || "10007".equals(result.getCode())){
            /*
            64002 产品不存在
            10006 企业账户余额不足
            10007 用户账户余额不足
             */
            orderService.orderFailedNotification(order, product, result.getMessage(), TopupFailType.PRODUCT_OR_ACCOUNT_EXCEPTION);
        }
        else {
            logger.warn("流量充值提交失败，错误代码：{},原因：{}",result.getCode(),result.getMessage());
            orderService.orderFailedNotification(order, product, result.getMessage(), TopupFailType.NORMAL);
        }
    }

    @Override
    public boolean shouldHandle(Product product) {
        if(product != null && "卓望".equals(product.getInterfaceFlag())){
            logger.info("匹配到卓望流量平台产品,内部产品编号：{}",product.getProductid());
            return true;
        }
        return false;
    }

    public ZhuoWangResult charge(String mobile,String productid,String productValue){
        try{
            logger.info("调用卓望流量充值接口");
            String url = api;

            String oper_time = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

            String transactionID = oper_time+(10000000+new Random().nextInt(89999999));
            String method = "companyFlowPkgHandsel";
            String sequence = portalType+portalId+transactionID+(100000+new Random().nextInt(899999));
            Map<String,String> paramMap = new HashMap<>();
            paramMap.put("portalType",portalType);
            paramMap.put("portalID",portalId);
            paramMap.put("company_code",companyCode);
            paramMap.put("transactionID",transactionID);
            paramMap.put("method",method);
            paramMap.put("sequence",sequence);
            paramMap.put("notify_url",notifyUrl);

            Map topup1 = new HashMap<>();
            topup1.put("msisdn", mobile);
            topup1.put("product_id", productid);
            topup1.put("product_value", productValue);
            Map topup = new HashMap<>();
            topup.put("user_list", Arrays.asList(topup1));

            paramMap.put("oper_data_list", new ObjectMapper().writeValueAsString(topup));
            paramMap.put("oper_time", oper_time);
            paramMap.put("signType", "MD5");
            String toSignString = HttpInterfaceUtil.toOrderAndNotNullValueUrlString(paramMap);

            String toSing = toSignString+key;
            String sign = DigestUtils.md5DigestAsHex(toSing.getBytes("UTF-8"));
            paramMap.put("sign", sign);

            String postContent = HttpInterfaceUtil.toUrlString(paramMap);

            logger.info("请求URL：{}",url);

            URL ur = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) ur.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setReadTimeout(30000);
            connection.setConnectTimeout(30000);
            connection.connect();

            byte[] bypes = postContent.getBytes();
            // 输入参数
            connection.getOutputStream().write(bypes);
            InputStream inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String strMessage = null;
            StringBuffer buffer = new StringBuffer();
            while ((strMessage = reader.readLine()) != null) {
                buffer.append(strMessage);
            }
            logger.info("响应报文：{}", buffer.toString());
            ObjectMapper objectMapper = new ObjectMapper();
            ZhuoWangResult zhuoWangResult = objectMapper.readValue(buffer.toString(),ZhuoWangResult.class);
            zhuoWangResult.setSequence(sequence);
            reader.close();
            return zhuoWangResult;
        }catch (Exception e){
            logger.error("卓望流量充值异常，手机号:{},卓望产品id:{}",mobile,productid,e);
            return null;
        }
    }
}
