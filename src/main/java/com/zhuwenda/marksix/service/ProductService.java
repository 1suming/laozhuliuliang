package com.zhuwenda.marksix.service;

import com.zhuwenda.marksix.bean.Parameter;
import com.zhuwenda.marksix.bean.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Product queryByProductid(String productid){
        String sql = "select * from t_product where productid = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{productid}, new BeanPropertyRowMapper<Product>(Product.class));
    }

    public List<Product> listAll(){
        String sql = "select * from t_product order by order_num DESC ";
        return jdbcTemplate.query(sql, new Object[]{}, new BeanPropertyRowMapper<Product>(Product.class));
    }

    public int downProduct(String productid){
        String sql = "upadte t_product set on_sale=2 where productid=?";
        int num = jdbcTemplate.update(sql,new Object[]{productid});
        return num == 1 ? 0 : 1;
    }
}
