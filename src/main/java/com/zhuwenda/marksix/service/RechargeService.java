package com.zhuwenda.marksix.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.bean.dahan.DaHanProductCarrierEnum;
import com.zhuwenda.marksix.bean.dahan.DaHanResult;
import com.zhuwenda.marksix.restfulservice.FCOrderServlerResult;
import com.zhuwenda.marksix.restfulservice.Fcmsgtemplateservletresult;
import com.zhuwenda.marksix.restfulservice.Fcsearchproductservlet;
import com.zhuwenda.marksix.restfulservice.PlatformProduct;
import com.zhuwenda.marksix.util.DigestTools;
import com.zhuwenda.marksix.util.HttpUtil;
import com.zhuwenda.marksix.util.MapParam;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;


@Service
public class RechargeService {

    protected final Log logger = LogFactory.getLog(RechargeService.class);

    /** 流量红包基础单位(M) */
    public static final int UNIT = 50;//需要90积分

    @Value("${liuliang.dahan.api}")
    private String api;
    @Value("${liuliang.dahan.account}")
    private String account;
    @Value("${liuliang.dahan.password}")
    private String password;
    @Value("${liuliang.dahan.msg.templateid}")
    private String msgTemplateid;

    private String simpleSign;
    @Autowired
    private ProductService productService;

    @PostConstruct
    public void init(){
        simpleSign = DigestTools.md5String(account+DigestTools.md5String(password));
    }


    /**
     * 查询账户余额
     *
     * @return 账户余额
     */
    public long getBalance() {

        MapParam mapParam = new MapParam()
                .add("account", account)
                .add("sign", simpleSign);
        String url = api + "/FCQueryBalanceServlet" + "?" + mapParam.toParamString();
        String resultContent = HttpUtil.get(url);
        return Long.parseLong(resultContent);
    }


    public List<PlatformProduct> searchProduct(){

        try {

            MapParam mapParam = new MapParam()
                    .add("account", account);
            String url = api + "/FCSearchProductServlet" + "?" + mapParam.toParamString();
            String resultContent = HttpUtil.get(url);
            logger.info("响应报文：");
            logger.info(resultContent);
            ObjectMapper objectMapper = new ObjectMapper();
            Fcsearchproductservlet fcsearchproductservlet =objectMapper.readValue(resultContent, Fcsearchproductservlet.class);
            if ("00".equals(fcsearchproductservlet.getResultcode())){
                return fcsearchproductservlet.getInformation();
            }
            return null;
        }catch (Exception e){
            logger.error("获取产品列表时发生了一个异常",e);
            return null;
        }
    }

    private String calcSign(String account,String password,String mobiles,long timestamp){
        return DigestTools.md5String(account+DigestTools.md5String(password)+timestamp + (mobiles.length() > 32 ? mobiles.substring(0,32) : mobiles.substring(0,mobiles.length())));
    }



    /**
     * 创建红包订单
     *
     * @param phone 充值电话号码
     * @param orderid 订单号
     * @param productCarrierEnum 号码归属的运营商
     * @param productid 产品ID
     * @param packageSize 包大小
     * @return {code:0,data:12312312312} code:0 成功 -1 失败 data:成功为订单号，失败未原因
     */
    public DaHanResult createOrder(String phone, String orderid, final DaHanProductCarrierEnum productCarrierEnum,String productid,String packageSize) {

        try{
            long timestamp = System.currentTimeMillis();
            MapParam mapParam = new MapParam()
                    .add("timestamp",timestamp+"")
                    .add("account", account)
                    .add("mobiles", phone)
                    .add("sign",calcSign(account,password,phone,timestamp))
                    .add("clientOrderId",orderid)
                    .add("msgTemplateId", msgTemplateid);
            if(packageSize != null && !"".equals(packageSize.trim())){
                mapParam.add("packageSize",packageSize);
            }else {
                logger.warn("缺少packageSize参数");
            }

            switch (productCarrierEnum){

                case DAHAN_CMCC :
                    mapParam.add("mobileProductId", productid);
                    break;
                case DAHAN_CUCC :
                    mapParam.add("unicomProductId", productid);
                    break;
                case DAHAN_CTCC :
                    mapParam.add("telecomProductId", productid);
                    break;
            }

            String url = api + "/FCOrderServlet";
            String postData = mapParam.toJSON();
            logger.info("调用下单接口的post数据："+postData);
            String resultContent = HttpUtil.post(url,postData);
            logger.info("大汉响应的数据"+resultContent);
            ObjectMapper objectMapper = new ObjectMapper();
            DaHanResult result = objectMapper.readValue(resultContent,DaHanResult.class);
            return result;

        }catch (Exception e){
            logger.error("提交大汉充值订单异常",e);
            return null;
        }
    }

    public List<Fcmsgtemplateservletresult> searchMsgTemplate(){
        try {

            MapParam mapParam = new MapParam()
                    .add("account", account)
                    .add("sign",simpleSign);
            String url = api + "/FCMsgTemplateServlet" + "?" + mapParam.toParamString();
            String resultContent = HttpUtil.get(url);
            ObjectMapper objectMapper = new ObjectMapper();
            List<Fcmsgtemplateservletresult> msgTemplates =objectMapper.readValue(resultContent, List.class);
            return msgTemplates;
        }catch (Exception e){
            return null;
        }
    }

//
//    /**
//     * 查询订单状态
//     *
//     * @param orderNo 订单编号
//     * @return {code:0,data:12312312312}
//     * code:0 订单未提交 1：系统处理中 2：订单处理完成 3：处理失败 4：订单有问题 data：平台原阳返回
//     */
//    public Map<String, Object> getOrderState(String orderNo) {
//        Map<String, Object> ret = new HashMap<String, Object>();
//        String passWordFinal = PassWordUtil.getEncodePwd(password);
//        try {
//            OrderStub stub = new OrderStub();
//            OrderStub.TxOrderState orderStateData = new OrderStub.TxOrderState();
//            orderStateData.setUsername(username);
//            orderStateData.setPassword(passWordFinal);
//            orderStateData.setOrderno(orderNo);
//            OrderStub.TxOrderStateResponse response = stub.txOrderState(orderStateData);
//            String result = response.get_return();
//            logger.info("创建订单接口流量平台原样返回结果：" + result);
//            if (result.contains("|")) {
//                String code = result.split("\\|")[0];
//                if ("success".equalsIgnoreCase(code)) {
//                    ret.put("errcode", 0);
//                    ret.put("data", result.split("\\|")[1]);
//                } else {
//                    ret.put("errcode", -1);
//                    ret.put("data", result.split("\\|")[1]);
//                }
//            }
//        } catch (AxisFault axisFault) {
//            logger.error("调用流量平台接口出错:" + axisFault.getMessage(), axisFault);
//        } catch (RemoteException e) {
//            logger.error("调用流量平台接口出错:" + e.getMessage(), e);
//        }
//        return ret;
//    }

}
