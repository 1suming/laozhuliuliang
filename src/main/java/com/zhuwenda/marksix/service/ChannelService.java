package com.zhuwenda.marksix.service;

import com.zhuwenda.marksix.bean.ChannelDistributionProduct;
import com.zhuwenda.marksix.bean.ChannelProduct;
import com.zhuwenda.marksix.bean.ChannelDistributionProductInfoDto;
import com.zhuwenda.marksix.bean.ChannelUser;
import com.zhuwenda.marksix.enums.ChannelType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 渠道产品
 */
@Service
public class ChannelService {
    private static final Logger logger = LoggerFactory.getLogger(ChannelService.class);


    @Autowired
    private JdbcTemplate jdbcTemplate;


    public ChannelProduct get(ChannelType channelType,String productid){
        String sql = "select * from t_channel_product where channel_type=? and productid=?";
        return jdbcTemplate.queryForObject(sql,new Object[]{channelType.toString(),productid},new BeanPropertyRowMapper<ChannelProduct>(ChannelProduct.class));
    }

    /** 查询用户配置的分销产品信息 */
    public List<ChannelDistributionProductInfoDto> queryByUserid(String userid){
        String sql = "select t_product.*,t_dist.userid,t_dist.channel_dist_price as channelDistPrice,t_dist.userid as userid from t_channel_distribution_product t_dist inner join t_product t_product on t_dist.productid = t_product.productid where t_dist.userid = ?";
        List<ChannelDistributionProductInfoDto> ret = jdbcTemplate.query(sql, new Object[]{userid}, new BeanPropertyRowMapper<ChannelDistributionProductInfoDto>(ChannelDistributionProductInfoDto.class));
        return ret;
    }

    /** 查询用户可配置的产品信息（如果已经配置过，则附带配置信息） */
    public List<ChannelDistributionProductInfoDto> queryAvailableProductForUser(String userid,ChannelType channelType){
        List<ChannelDistributionProductInfoDto> ret = queryOnSaleProduct(channelType);
        List<ChannelDistributionProduct> userDist = queryChannelDistProduct(userid);
        ret.stream().forEach(e->{
            userDist.stream().filter(dist->dist.getProductid().equals(e.getProductid()))
                    .findFirst()
                    .ifPresent(t1->{
                        e.setUserid(t1.getUserid());
                        e.setChannelDistPrice(t1.getChannelDistPrice());
                    });
        });
        return ret;
    }

    /** 查询可分销的产品 */
    protected List<ChannelDistributionProductInfoDto> queryOnSaleProduct(ChannelType channelType){
        String sql = "select p.*,cp.channel_type, cp.channel_price from t_product p inner join t_channel_product cp on p.productid = cp.productid" +
                " where p.on_sale = 1 and cp.on_sale = 1 and cp.channel_type = ? order by p.order_num desc";
        List<ChannelDistributionProductInfoDto> ret = jdbcTemplate.query(sql, new Object[]{channelType.toString()}, new BeanPropertyRowMapper<ChannelDistributionProductInfoDto>(ChannelDistributionProductInfoDto.class));
        return ret;
    }

    /** 查询用户代销的产品 */
    protected List<ChannelDistributionProduct> queryChannelDistProduct(String userid){
        String sql = "select * from t_channel_distribution_product" +
                " where userid=?";
        List<ChannelDistributionProduct> ret = jdbcTemplate.query(sql, new Object[]{userid}, new BeanPropertyRowMapper<ChannelDistributionProduct>(ChannelDistributionProduct.class));
        return ret;
    }

    /** 下架用户的所有产品 */
    public void downChannelDistProduct(String userid){
        String sql = "delete from t_channel_distribution_product where userid=?";
        jdbcTemplate.update(sql,userid);
    }

    /** 上架产品(注意，会先下架该用户下所有的产品，再上架产品) */
    @Transactional
    public void updateChannelDistProduct(String userid,List<ChannelDistributionProduct> products){
        if(products != null){
            products.stream().forEach(e->{
                if(e.getChannelDistPrice() == null || e.getChannelDistPrice() <= 0){
                    throw new IllegalArgumentException("分销价格参数错误");
                }
                e.setUserid(userid);
            });
            downChannelDistProduct(userid);
            addChannelDistProduct(products);
        }
    }

    /** 新增记录 */
    protected void addChannelDistProduct(List<ChannelDistributionProduct> products){
        if(products != null && products.size() > 0){
            String sql = "insert into t_channel_distribution_product(userid,productid,channel_dist_price)" +
                    " values(?,?,?)";
            List<Object[]> list = products.stream().map(e -> {
                return new Object[]{e.getUserid(), e.getProductid(), e.getChannelDistPrice()};
            }).collect(Collectors.toList());
            jdbcTemplate.batchUpdate(sql, list);
        }
    }

    /** 查询用户配置的分销产品信息 */
    public ChannelDistributionProductInfoDto queryByUseridAndProductId(String userid,String productId){
        String sql = "select t_product.*,t_dist.userid,t_dist.channel_dist_price as channelDistPrice,t_dist.userid as userid from t_channel_distribution_product t_dist inner join t_product t_product on t_dist.productid = t_product.productid" +
                " where t_dist.userid = ? and t_dist.productid = ?";
        ChannelDistributionProductInfoDto ret = jdbcTemplate.queryForObject(sql, new Object[]{userid, productId}, new BeanPropertyRowMapper<ChannelDistributionProductInfoDto>(ChannelDistributionProductInfoDto.class));
        return ret;
    }
    /** 查询分销商用户配置的可供客户购买的分销产品信息 */
    public List<ChannelDistributionProductInfoDto> querySellableProducts(String userid){
        String sql = "select t_product.*,cp.channel_type as channelType,cp.channel_price as channelPrice,t_dist.userid,t_dist.channel_dist_price as channelDistPrice,t_dist.userid as userid " +
                "from t_channel_product cp INNER JOIN t_channel_distribution_product t_dist ON cp.productid = t_dist.productid " +
                "inner join t_product t_product on t_dist.productid = t_product.productid" +
                " where t_product.on_sale = 1 and cp.on_sale = 1 and t_dist.userid = ? ";

        List<ChannelDistributionProductInfoDto> ret = jdbcTemplate.query(sql, new Object[]{userid}, new BeanPropertyRowMapper<ChannelDistributionProductInfoDto>(ChannelDistributionProductInfoDto.class));
        return ret;
    }
    /** 查询分销商用户配置的可供客户购买的指定分销产品信息 */
    public ChannelDistributionProductInfoDto querySellableProducts(String userid,String productid){
        String sql = "select t_product.*,cp.channel_type as channelType,cp.channel_price as channelPrice,t_dist.userid,t_dist.channel_dist_price as channelDistPrice,t_dist.userid as userid " +
                "from t_channel_product cp INNER JOIN t_channel_distribution_product t_dist ON cp.productid = t_dist.productid " +
                "inner join t_product t_product on t_dist.productid = t_product.productid" +
                " where t_product.on_sale = 1 and cp.on_sale = 1 and t_dist.userid = ? and t_product.productid=?";

        ChannelDistributionProductInfoDto ret = jdbcTemplate.queryForObject(sql, new Object[]{userid,productid}, new BeanPropertyRowMapper<ChannelDistributionProductInfoDto>(ChannelDistributionProductInfoDto.class));
        return ret;
    }

    public void add(ChannelUser user){
        String sql = "insert into t_channel_user(userid,channel_type,create_time)" +
                "values(?,?,now())";
        jdbcTemplate.update(sql,new Object[]{user.getUserid(),user.getChannelType()});
    }


}
