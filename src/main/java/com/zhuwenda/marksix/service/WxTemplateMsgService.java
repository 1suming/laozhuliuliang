package com.zhuwenda.marksix.service;

import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.bean.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.WxMpTemplateMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class WxTemplateMsgService {

    private static final Logger logger = LoggerFactory.getLogger(WxTemplateMsgService.class);

    @Autowired
    private WxService wxService;

    @Value("${domain}")
    private String domain;

    //订单支付成功
    @Value("${weixin.templateMsg.orderPaySuccess.id}")
    private String orderPaySuccessTemplateId;
    //订单失败提醒
    @Value("${weixin.templateMsg.orderTopupFail.id}")
    private String orderTopupFailTemplateId;
    //订单成功提醒
    @Value("${weixin.templateMsg.orderTopupSuccess.id}")
    private String orderTopupSuccessTemplateId;
    //邀请关注成功通知
    @Value("${weixin.templateMsg.inviteSubscribe.id}")
    private String inviteSubscribeTemplateId;
    //会员升级通知
    @Value("${weixin.templateMsg.beVip.id}")
    private String beVipTemplateId;
    //分销订单提成通知
    @Value("${weixin.templateMsg.getCommission.id}")
    private String getCommissionTemplateId;

    /** 支付成功通知 */
    public String orderPaySuccess(String toUser,String moneySum,String productName){
        String templateId = orderPaySuccessTemplateId;
        String first = "我们已收到您的货款，正在为您充值，请耐心等待: )";
        String remark = "如有问题请致电18824107788或直接在微信留言，小易将第一时间为您服务！";
        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("orderMoneySum", moneySum));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("orderProductName", productName));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("Remark", remark));
        try {
            return wxService.templateSend(wxMpTemplateMessage);
        }catch (WxErrorException e){
            logger.warn("模板消息发送失败,错误原因:{}",e.getError().getErrorMsg());
            return null;
        }
    }

    /** 订单失败提醒 */
    public String orderTopupFail(String toUser,String orderId,String productName,String mobile,Date orderTime){
        String templateId = orderTopupFailTemplateId;
        String first = "您好，您的充值已失败。";
        String keyword1 = orderId;
        String keyword2 = productName;
        String keyword3 = mobile;
        String keyword4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(orderTime);
        String remark = "我们将于两小时内为您退款，请注意查收。有疑问请咨询在线客服";

        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1", keyword1));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2", keyword2));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3", keyword3));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword4",keyword4));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark",remark));

        try {
            return wxService.templateSend(wxMpTemplateMessage);
        }catch (WxErrorException e){
            logger.warn("模板消息发送失败,错误原因:{}",e.getError().getErrorMsg());
            return null;
        }
    }

    /** 订单成功提醒 */
    public String orderTopupSuccess(String toUser,String orderId,String productName,String mobile,Date orderTime) {

        String templateId = orderTopupSuccessTemplateId;
        String first = "亲，您的订单已经充值成功！感谢支持哦";
        String keyword1 = orderId;
        String keyword2 = productName;
        String keyword3 = mobile;
        String keyword4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(orderTime);
        String remark = "如有疑问，请咨询18824107788";

        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1", keyword1));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2",keyword2));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3",keyword3));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword4", keyword4));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark", remark));

        try {
            return wxService.templateSend(wxMpTemplateMessage);
        }catch (WxErrorException e){
            logger.warn("模板消息发送失败,错误原因:{}",e.getError().getErrorMsg());
            return null;
        }
    }

    /**
     * 邀请关注成功通知
     * @param toUser
     * @param recommended 被推荐人昵称
     * @param time
     * @param recommender 推荐人昵称
     * @return
     */
    public String inviteSubscribe(String toUser,String recommended,Date time,String recommender){
        String templateId = inviteSubscribeTemplateId;
        String first = "您好，以下会员是通过您的二维码关注我们的：";
        String keyword1 = recommended;
        String keyword2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(time);
        String keyword3 = recommender;
        String remark = "如有疑问，请播打我们的400号码";

        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1",keyword1));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2",keyword2));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3", keyword3));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark", remark));

        try {
            return wxService.templateSend(wxMpTemplateMessage);
        }catch (WxErrorException e){
            logger.warn("模板消息发送失败,错误原因:{}",e.getError().getErrorMsg());
            return null;
        }
    }

    /** 会员升级通知 */
    public String beVip(String toUser,String toUserName,String vipId ){
        //分销订单提成通知
        //（通知三级推荐人，分成金额按照比例折算）
        String templateId = beVipTemplateId;
        String first = String.format("尊敬的%s, 恭喜你成功升级为VIP会员。",toUserName);
        String keyword1 = vipId;
        String keyword2 = "永久";
        String remark = "vip用户享受最低价充值优惠。详询18824107788。";

        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1",keyword1));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2", keyword2));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark", remark));

        try {
            return wxService.templateSend(wxMpTemplateMessage);
        }catch (WxErrorException e){
            logger.warn("模板消息发送失败,错误原因:{}",e.getError().getErrorMsg());
            return null;
        }
    }

    /** 分销订单提成通知 */
    public String getCommission(String toUser,String orderId,String orderAmount,String commissionAmount,Date time){
        //（通知三级推荐人，分成金额按照比例折算）
        String templateId = getCommissionTemplateId;
        String first = "亲，您又成功分销出一笔订单了";
        String keyword1 = orderId;
        String keyword2 = orderAmount;
        String keyword3 = commissionAmount;
        String keyword4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(time);
        String remark = "【掌上流量平台】感谢有您，客服热线：18824107788";

        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
        wxMpTemplateMessage.setTemplateId(templateId);
        wxMpTemplateMessage.setToUser(toUser);
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("first", first));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword1", keyword1));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword2",keyword2));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword3",keyword3));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("keyword4", keyword4));
        wxMpTemplateMessage.getDatas().add(new WxMpTemplateData("remark", remark));

        try {
            return wxService.templateSend(wxMpTemplateMessage);
        }catch (WxErrorException e){
            logger.warn("模板消息发送失败,错误原因:{}",e.getError().getErrorMsg());
            return null;
        }
    }

}
