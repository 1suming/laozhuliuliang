package com.zhuwenda.marksix.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.thoughtworks.xstream.XStream;
import com.zhuwenda.marksix.bean.BatchUserinfoResult;
import com.zhuwenda.marksix.bean.WxRefundResult;
import com.zhuwenda.marksix.bean.Wxuser;
import com.zhuwenda.marksix.config.WxConfig;
import me.chanjar.weixin.common.bean.result.WxError;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.common.util.crypto.WxCryptUtil;
import me.chanjar.weixin.common.util.http.SimplePostRequestExecutor;
import me.chanjar.weixin.common.util.http.Utf8ResponseHandler;
import me.chanjar.weixin.common.util.xml.XStreamInitializer;
import me.chanjar.weixin.mp.api.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.result.WxMpPrepayIdResult;
import me.chanjar.weixin.mp.bean.result.WxMpQrCodeTicket;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.*;

@Service
public class WxService extends WxMpServiceImpl{

    private static Logger logger = Logger.getLogger(WxService.class);

    @Value("${pay.weixin.partnerid}")
    private String partnerId;

    @Autowired
    private WxConfig wxConfig;

    @PostConstruct
    public void init(){
        setWxMpConfigStorage(wxConfig);
    }

    public List<WxMpUser> userinfoBatchget(List<String> openids) throws WxErrorException {
        if(openids == null || openids.isEmpty()){
            throw new IllegalArgumentException("openids长度必须大于0");
        }

        List<WxMpUser> ret = new ArrayList<>();

        int MAX_LENGTH = 100;
        int maxBatch = openids.size()%MAX_LENGTH == 0 ? openids.size()/MAX_LENGTH : openids.size()/MAX_LENGTH+1;

        try {
            for(int i = 0; i < maxBatch; i++){
                int fromIndex = i * MAX_LENGTH;

                String accessToken = super.getAccessToken();

                String url = "https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token="+accessToken;
                Map<String,Object> mapJson = new HashMap<>();
                List<Map> list = new ArrayList<>();
                openids.stream().skip(fromIndex).limit(MAX_LENGTH).forEach(ele -> {
                    Map<String, String> map = new HashMap<>();
                    map.put("openid", ele);
                    list.add(map);
                });
                mapJson.put("user_list", list);



                ObjectMapper objectMapper = new ObjectMapper();
                String json = null;
                try {
                    json = objectMapper.writeValueAsString(mapJson);
                }catch (JsonProcessingException e){
                    throw new RuntimeException("json序列化错误");
                }

                HttpClient httpClient = HttpClientBuilder.create().build();
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(new StringEntity(json));
                HttpResponse response = httpClient.execute(httpPost);

                String responseContent = EntityUtils.toString(response.getEntity(), Charset.forName("utf8"));

                BatchUserinfoResult batchUserinfoResult = BatchUserinfoResult.fromJson(responseContent);
                List<WxMpUser> listTemp = batchUserinfoResult.getUser_info_list();
                ret.addAll(listTemp);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return ret;
    }


    /**
     * 服务商退款
     * @param amt 退款金额，单位：分
     * @return
     */
    public WxRefundResult refund(String outTradeNo, String outRefundNo, int amt) {
        String nonce_str = System.currentTimeMillis() + "";

        SortedMap<String, String> packageParams = new TreeMap<String, String>();
        packageParams.put("appid", wxMpConfigStorage.getAppId());
        packageParams.put("mch_id", wxMpConfigStorage.getPartnerId());
        if(wxMpConfigStorage.getSubPartnerId() != null){
            packageParams.put("sub_mch_id", wxMpConfigStorage.getSubPartnerId());
        }
        packageParams.put("nonce_str", nonce_str);
        packageParams.put("out_trade_no", outTradeNo);
        packageParams.put("out_refund_no", outRefundNo);

        packageParams.put("total_fee", amt  + "");
        packageParams.put("refund_fee", amt + "");
        packageParams.put("op_user_id", "system_auto");

        String sign = WxCryptUtil.createSign(packageParams, wxMpConfigStorage.getPartnerKey());
        String xml = "<xml>" +
                "<appid>" + wxMpConfigStorage.getAppId() + "</appid>" +
                "<mch_id>" + wxMpConfigStorage.getPartnerId() + "</mch_id>";
        if(wxMpConfigStorage.getSubPartnerId() != null){
            xml+= "<sub_mch_id>" + wxMpConfigStorage.getSubPartnerId() + "</sub_mch_id>";
        }
        xml += "<nonce_str>" + nonce_str + "</nonce_str>" +
                "<sign>" + sign + "</sign>" +
                "<out_trade_no>" + outTradeNo + "</out_trade_no>" +
                "<out_refund_no>" + outRefundNo + "</out_refund_no>"+
                "<total_fee>" + packageParams.get("total_fee") + "</total_fee>" +
                "<refund_fee>" + packageParams.get("refund_fee") + "</refund_fee>" +
                "<op_user_id>" + packageParams.get("op_user_id") + "</op_user_id>" +
                "</xml>";

        HttpPost httpPost = new HttpPost("https://api.mch.weixin.qq.com/secapi/pay/refund");

        if (httpProxy != null) {
            RequestConfig config = RequestConfig.custom().setProxy(httpProxy).build();
            httpPost.setConfig(config);
        }

        StringEntity entity = new StringEntity(xml, Consts.UTF_8);
        httpPost.setEntity(entity);
        try {

            CloseableHttpResponse response = getHttpclient().execute(httpPost);
            String responseContent = Utf8ResponseHandler.INSTANCE.handleResponse(response);
            XStream xstream = XStreamInitializer.getInstance();
            xstream.alias("xml", WxRefundResult.class);
            WxRefundResult WxRefundResult = (WxRefundResult) xstream.fromXML(responseContent);
            return WxRefundResult;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return new WxRefundResult();
    }

}
