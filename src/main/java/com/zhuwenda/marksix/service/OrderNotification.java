package com.zhuwenda.marksix.service;

import com.zhuwenda.marksix.service.handler.IOrderPaySuccessHandler;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Vector;

@Component
public class OrderNotification implements ApplicationContextAware{
    private static final Logger logger = LoggerFactory.getLogger(OrderNotification.class);

    private boolean changed = false;
    private Vector<IOrderPaySuccessHandler> obs;
    private ApplicationContext applicationContext;

    @PostConstruct
    public void init(){
        obs = new Vector<IOrderPaySuccessHandler>();
        obs.addAll(applicationContext.getBeansOfType(IOrderPaySuccessHandler.class).values());
    }



    public void notify(Order order,Product product) {
        notify(order, product, null);
    }

    public void notify(Order order,Product product,Map<String,Object> data) {
        /*
         * a temporary array buffer, used as a snapshot of the state of
         * current Observers.
         */
        Object[] arrLocal;

        synchronized (this) {
            if (!changed)
                return;
            arrLocal = obs.toArray();
            clearChanged();
        }
        boolean hasMatch = false;
        for (int i = arrLocal.length-1; i>=0; i--){
            IOrderPaySuccessHandler handler = (IOrderPaySuccessHandler)arrLocal[i];
            if(handler.shouldHandle(product)){
                hasMatch = true;
                handler.handle(order,product,data);
            }
        }
        if(!hasMatch){
            logger.error("找不到匹配的产品接口，订单编号:{}",order.getOrderid());
        }

    }


    public synchronized void setChanged() {
        changed = true;
    }

    protected synchronized void clearChanged() {
        changed = false;
    }

    public synchronized boolean hasChanged() {
        return changed;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
