package com.zhuwenda.marksix.service;

import com.zhuwenda.marksix.bean.Bill;
import com.zhuwenda.marksix.enums.FundFlowType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Service
public class BillService {


    private static final Logger logger = LoggerFactory.getLogger(BillService.class);


    @Autowired
    private JdbcTemplate jdbcTemplate;


    protected String add(Bill bill){
        String flowid = generateFlowid();
        String sql = "insert into t_bill(flowid,userid,fund_flow_type,amount,operator,operate_time,description,orderid)" +
                "values(?,?,?,?,?,now(),?,?)";
        jdbcTemplate.update(sql,new Object[]{flowid,bill.getUserid(),bill.getFundFlowType(), bill.getAmount(),bill.getOperator(),bill.getDescription(),bill.getOrderid()});
        return flowid;
    }

    public int getUserBalance(String userid){
        String sql = "select sum(amount) from t_bill where userid=?";
        return jdbcTemplate.queryForInt(sql,userid);
    }

    /** 分销商自动充值 */
    public String distAutoTopup(String userid,int amount,String orderid){
        if(amount <= 0){
            throw new IllegalArgumentException("充值金额必须大于0");
        }
        if(amount > 20000){
            //TODO 安全限制
            throw new IllegalArgumentException("安全限制：自动充值金额不能大于200元");
        }
        Bill bill = new Bill();
        bill.setFlowid(generateFlowid());
        bill.setUserid(userid);
        bill.setAmount(amount);
        bill.setFundFlowType(FundFlowType.INCOME);
        bill.setOperator("系统");
        bill.setOperateTime(new Date());
        bill.setDescription("系统自动充值");
        bill.setOrderid(orderid);
        add(bill);
        return bill.getFlowid();
    }

    /** 分销商自动扣款 */
    public String distAutoPay(String userid,int amount,String orderid){
        return distAutoPay(userid,amount,orderid,null);
    }
    /** 分销商自动扣款 */
    public String distAutoPay(String userid,int amount,String orderid,String desc){
        if(amount <= 0){
            throw new IllegalArgumentException("充值金额必须大于0");
        }
        if(amount > 20000){
            //TODO 安全限制
            throw new IllegalArgumentException("安全限制：自动充值金额不能大于200元");
        }
        Bill bill = new Bill();
        bill.setFlowid(generateFlowid());
        bill.setUserid(userid);
        bill.setAmount(amount);
        bill.setFundFlowType(FundFlowType.EXPENDITURE);
        bill.setOperator("系统");
        bill.setOperateTime(new Date());
        bill.setDescription(desc != null ? desc : "系统自动扣款");
        bill.setOrderid(orderid);
        add(bill);
        return bill.getFlowid();
    }


    protected String generateFlowid(){
        Random random = new Random();
        int randomNumber = random.nextInt(999) + 1000;
        String flowid = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + randomNumber;
        return flowid;
    }

}
