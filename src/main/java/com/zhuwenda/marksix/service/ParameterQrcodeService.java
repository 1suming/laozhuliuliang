package com.zhuwenda.marksix.service;

import com.zhuwenda.marksix.bean.ParameterQrcode;
import com.zhuwenda.marksix.bean.WxuserShareCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Random;

@Service
public class ParameterQrcodeService {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public ParameterQrcode queryBySceneid(String sceneid){
        String sql = "select * from t_parameter_qrcode where sceneid=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{sceneid}, new BeanPropertyRowMapper<ParameterQrcode>(ParameterQrcode.class));
    }


    public int add(ParameterQrcode qrcode){
        if (qrcode != null){
            qrcode.setLastUpdateTime(new Date());
            String sql = "insert into t_parameter_qrcode(sceneid, ticket, expire_seconds, url, last_update_time) VALUES (?,?,?,?,?)";
            return jdbcTemplate.update(sql, new Object[]{qrcode.getSceneid(),qrcode.getTicket(),qrcode.getExpireSeconds(),qrcode.getUrl(),qrcode.getLastUpdateTime()}) == 1 ? 0 : 1;
        }else {
            return 2;
        }
    }

    public int update(ParameterQrcode qrcode){
        if (qrcode != null && qrcode.getSceneid() != null){
            qrcode.setLastUpdateTime(new Date());
            String sql = "update t_parameter_qrcode set ticket=?, expire_seconds=?, url=?, last_update_time=? where sceneid=?";
            return jdbcTemplate.update(sql, new Object[]{qrcode.getTicket(),qrcode.getExpireSeconds(),qrcode.getUrl(),qrcode.getLastUpdateTime(),qrcode.getSceneid()}) == 1 ? 0 : 1;
        }else {
            return 2;
        }
    }

}
