package com.zhuwenda.marksix.service;

import com.zhuwenda.marksix.bean.Parameter;
import com.zhuwenda.marksix.bean.Withdraw;
import com.zhuwenda.marksix.bean.result.Errcode;
import com.zhuwenda.marksix.exception.BalanceNotEnoughException;
import me.chanjar.weixin.mp.bean.result.WxMpTransferResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class WithdrawService {

    private static final Logger logger = LoggerFactory.getLogger(WithdrawService.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private WxuserService wxuserService;
    @Autowired
    private ParameterService parameterService;
    @Autowired
    private WxService wxService;

    /**
     * 添加提现记录
     * @param withdraw
     * @return 成功时返回 提现ID
     */
    public int add(Withdraw withdraw){
        withdraw.setCreateTime(new Date());
        withdraw.setStatus(Withdraw.STATUS_APPLYING);

        long balance = wxuserService.queryUsableBalance(withdraw.getOpenid());
        if (balance >= withdraw.getAmount()){

            NamedParameterJdbcTemplate namedparam = new NamedParameterJdbcTemplate(jdbcTemplate);
            String sql = "INSERT INTO `t_withdraw`" +
                    " (`openid`, `amount`, `status`, `note`, `status_msg`, `create_time`, `account`)" +
                    " VALUES (:openid, :amount, :status, :note, :statusMsg, :createTime, :account)";
            KeyHolder keyHolder = new GeneratedKeyHolder();
            namedparam.update(sql,new BeanPropertySqlParameterSource(withdraw),keyHolder);
            Integer id = keyHolder.getKey().intValue();
            withdraw.setWithdrawId(id);
            return id;
        }else {
            throw new BalanceNotEnoughException();
        }
    }

    public void autoWithdraw(Integer withdrawId){
        int limit = getWithdrawLimit();
        if(limit > 0){

            Withdraw withdraw = queryByWithdrawId(withdrawId);
            if(withdraw.getAmount() < limit){

                WxMpTransferResult result = wxService.transfers(withdraw.getWithdrawId() + "", withdraw.getOpenid(), withdraw.getAmount(), "提现", "127.0.0.1");
                if("SUCCESS".equalsIgnoreCase(result.getResult_code())){
                    updateWithdraw(withdrawId,Withdraw.STATUS_SUCCESS,"微信自动提现成功",result.getPayment_no());
                }else {
                    logger.error("自动提现失败：withdraw_id:{},return_msg:{},err_code:{},err_code_des:{}",withdrawId,result.getReturn_msg(),result.getErr_code(),result.getErr_code_des());
                    updateWithdraw(withdrawId, Withdraw.STATUS_FAILED, result.getReturn_msg()+";"+result.getErr_code_des(),null);
                }
            }
        }
    }

    /** 更新提现状态 */
    public void updateWithdraw(Integer withdrawId,int status,String msg,String paymentNo){
        String sql = "update t_withdraw set status=?,status_msg=?,payment_no=? where withdraw_id=?";
        jdbcTemplate.update(sql,status,msg,paymentNo,withdrawId);
    }

    /** 获取自动提现限额，当不可自动提现时，返回-1 */
    private int getWithdrawLimit(){
        Parameter autoWithdrawSwitch = parameterService.queryByKey(Parameter.KEY_AUTO_WITHDRAW_SWITCH);
        if(autoWithdrawSwitch != null && "ON".equalsIgnoreCase(autoWithdrawSwitch.getParamValue())){
            Parameter parameter = parameterService.queryByKey(Parameter.KEY_AUTO_WITHDRAW_LIMIT_VALUE);
            if(parameter != null && org.springframework.util.StringUtils.hasText(parameter.getParamValue())) {
                int limit = Integer.parseInt(parameter.getParamValue());
                return limit;
            }
        }
        return -1;
    }

    public Withdraw queryByWithdrawId(Integer withdrawId){
        String sql = "select * from t_withdraw where withdraw_id=?";
        return jdbcTemplate.queryForObject(sql,new Object[]{withdrawId},new BeanPropertyRowMapper<Withdraw>(Withdraw.class));
    }

    public List<Withdraw> queryWithdrawByOpenid(String openid){
        String sql = "select * from t_withdraw where openid=? order by create_time desc";
        List<Withdraw> ret = jdbcTemplate.query(sql, new Object[]{openid}, new BeanPropertyRowMapper<Withdraw>(Withdraw.class));
        return ret;
    }

    /** 查询用户已提现金额 */
    public long queryUserFrozenBalance(String openid){
        String sql = String.format("select sum(amount) from t_withdraw where openid=? and status in('%s','%s')", Withdraw.STATUS_APPLYING, Withdraw.STATUS_SUCCESS);
        return jdbcTemplate.queryForLong(sql, new Object[]{openid});
    }
}
