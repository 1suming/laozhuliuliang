package com.zhuwenda.marksix.enums;

/**
 * Created by wendazhu on 16/4/28.
 */
public enum ErrorInfo {
    NO_ERROR(0,"正常"),

    UNKNOW_EXCEPTION(-1,"未知异常"),
    BALANCE_NOT_ENOUGH(40001,"余额不足"),
    INVALID_COMMISSION_TYPE(40002,"不合法的佣金类型"),
    INVALID_NUMBER(40004,"不合法的数字"),
    ILLEGAL_ARGUMENT_EXCEPTION(80001,"参数异常"),
    INFO_MISSING(80002,"信息缺失");

    private int errcode;
    private String errmsg;

    ErrorInfo(int errcode, String errmsg){
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    public int getErrcode(){
        return this.errcode;
    }
    public String getErrmsg(){
        return this.errmsg;
    }
}
