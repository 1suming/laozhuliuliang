package com.zhuwenda.marksix.enums;

/**
 * Created by wendazhu on 16/4/18.
 */
public enum FundFlowType {
    /** 收入 */
    INCOME,
    /** 支出 */
    EXPENDITURE
}
