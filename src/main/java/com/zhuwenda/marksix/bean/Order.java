package com.zhuwenda.marksix.bean;

import com.zhuwenda.marksix.enums.ChannelType;

import java.util.Date;


public class Order {

    private String id;
    private String orderid;
    private int feeAmount;
    private int num;
    private String productid;
    private int payStatus;
    private int topupStatus;
    private String topupMobile;
    private String topupOrderid;
    private String payid;
    private Date createTime;
    private String openid;
    private String ip;
    private String sourceOpenid;
    private String outerMessage;
    private ChannelType channelType;
    private String channelId;
    private String refundNo;
    private String refundId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public int getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(int feeAmount) {
        this.feeAmount = feeAmount;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getPayid() {
        return payid;
    }

    public void setPayid(String payid) {
        this.payid = payid;
    }

    public int getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(int payStatus) {
        this.payStatus = payStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getTopupStatus() {
        return topupStatus;
    }

    public void setTopupStatus(int topupStatus) {
        this.topupStatus = topupStatus;
    }

    public String getTopupMobile() {
        return topupMobile;
    }

    public void setTopupMobile(String topupMobile) {
        this.topupMobile = topupMobile;
    }

    public String getTopupOrderid() {
        return topupOrderid;
    }

    public void setTopupOrderid(String topupOrderid) {
        this.topupOrderid = topupOrderid;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getSourceOpenid() {
        return sourceOpenid;
    }

    public void setSourceOpenid(String sourceOpenid) {
        this.sourceOpenid = sourceOpenid;
    }

    public String getOuterMessage() {
        return outerMessage;
    }

    public void setOuterMessage(String outerMessage) {
        this.outerMessage = outerMessage;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getRefundNo() {
        return refundNo;
    }

    public void setRefundNo(String refundNo) {
        this.refundNo = refundNo;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }
}
