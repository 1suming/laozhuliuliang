package com.zhuwenda.marksix.bean;

import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.chanjar.weixin.mp.bean.result.WxMpUserList;
import me.chanjar.weixin.mp.util.json.WxMpGsonBuilder;

import java.util.List;

/**
 * Created by wendazhu on 15/12/27.
 */
public class BatchUserinfoResult {

    private List<WxMpUser> user_info_list;

    public List<WxMpUser> getUser_info_list() {
        return user_info_list;
    }

    public void setUser_info_list(List<WxMpUser> user_info_list) {
        this.user_info_list = user_info_list;
    }

    public static BatchUserinfoResult fromJson(String json) {
        return WxMpGsonBuilder.INSTANCE.create().fromJson(json, BatchUserinfoResult.class);
    }
}
