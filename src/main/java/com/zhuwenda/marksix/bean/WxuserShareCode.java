package com.zhuwenda.marksix.bean;


public class WxuserShareCode {

    /** 我要推广临时二维码 */
    public static final int SHARE_TYPE_INTRODUCE = 1000;
    /** 线下推广永久二维码 */
    public static final int SHARE_TYPE_PERMANENT_OFFLINE = 1001;

    private String openid;
    private String shareCode;
    private Integer shareType;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public Integer getShareType() {
        return shareType;
    }

    public void setShareType(Integer shareType) {
        this.shareType = shareType;
    }
}
