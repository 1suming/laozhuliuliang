package com.zhuwenda.marksix.bean.hotdata;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by wendazhu on 16/1/2.
 */
public class HotDataResult {


    @JsonProperty("Code")
    private String code;
    @JsonProperty("Message")
    private String message;
    @JsonProperty("Data")
    private Data data;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
