package com.zhuwenda.marksix.bean.hotdata;


import com.fasterxml.jackson.annotation.JsonProperty;

public class CallbackData {

    @JsonProperty("transactionID")
    private String transactionid;
    private String mobile;
    @JsonProperty("product_id")
    private String productid;
    private String status;
    private String message;

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
