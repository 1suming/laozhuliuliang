package com.zhuwenda.marksix.bean;

public class ProductCommission {

    private String productid;
    private Integer level1;
    private Integer level2;
    private Integer level3;
    private Integer sourceOpenid;

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public Integer getLevel1() {
        return level1;
    }

    public void setLevel1(Integer level1) {
        this.level1 = level1;
    }

    public Integer getLevel2() {
        return level2;
    }

    public void setLevel2(Integer level2) {
        this.level2 = level2;
    }

    public Integer getLevel3() {
        return level3;
    }

    public void setLevel3(Integer level3) {
        this.level3 = level3;
    }

    public Integer getSourceOpenid() {
        return sourceOpenid;
    }

    public void setSourceOpenid(Integer sourceOpenid) {
        this.sourceOpenid = sourceOpenid;
    }
}
