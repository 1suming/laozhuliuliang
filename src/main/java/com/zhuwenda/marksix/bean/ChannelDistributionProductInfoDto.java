package com.zhuwenda.marksix.bean;

import com.zhuwenda.marksix.enums.ChannelType;

/**
 * Created by wendazhu on 16/4/17.
 */
public class ChannelDistributionProductInfoDto extends Product{

    private String userid;
    private ChannelType channelType;
    private Integer channelPrice;
    private Integer channelDistPrice;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public Integer getChannelPrice() {
        return channelPrice;
    }

    public void setChannelPrice(Integer channelPrice) {
        this.channelPrice = channelPrice;
    }

    public Integer getChannelDistPrice() {
        return channelDistPrice;
    }

    public void setChannelDistPrice(Integer channelDistPrice) {
        this.channelDistPrice = channelDistPrice;
    }
}
