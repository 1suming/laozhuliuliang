package com.zhuwenda.marksix.bean;

import com.zhuwenda.marksix.enums.ChannelType;

/**
 * Created by wendazhu on 16/4/21.
 */
public class ChannelProduct {

    private ChannelType channelType;
    private String productid;
    private int channelPrice;
    private int onSale;

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public int getChannelPrice() {
        return channelPrice;
    }

    public void setChannelPrice(int channelPrice) {
        this.channelPrice = channelPrice;
    }

    public int getOnSale() {
        return onSale;
    }

    public void setOnSale(int onSale) {
        this.onSale = onSale;
    }
}
