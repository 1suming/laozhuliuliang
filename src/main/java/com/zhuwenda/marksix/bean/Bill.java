package com.zhuwenda.marksix.bean;

import com.zhuwenda.marksix.enums.FundFlowType;

import java.util.Date;

/**
 * Created by wendazhu on 16/4/18.
 */
public class Bill {

    private String flowid;
    private String userid;
    private FundFlowType fundFlowType;
    private int amount;
    private String operator;
    private Date operateTime;
    private String orderid;
    private String description;

    public String getFlowid() {
        return flowid;
    }

    public void setFlowid(String flowid) {
        this.flowid = flowid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public FundFlowType getFundFlowType() {
        return fundFlowType;
    }

    public void setFundFlowType(FundFlowType fundFlowType) {
        this.fundFlowType = fundFlowType;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }
}
