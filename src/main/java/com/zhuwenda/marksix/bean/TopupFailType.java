package com.zhuwenda.marksix.bean;

/** 充值失败类型 */
public enum TopupFailType {

    /** 一般错误 */
    NORMAL,
    /** 产品或余额异常 */
    PRODUCT_OR_ACCOUNT_EXCEPTION

}
