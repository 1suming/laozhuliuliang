package com.zhuwenda.marksix.bean;

/**
 * 分页对象
 * 特别的,当总记录数totalRecord=0时
 * @author 朱文达
 *
 */
public class Pager {
	private int curPage;
	private int from;
	private int to;
	private int pageSize;
	private int totalRecord;
	private int totalPage;
	
	private boolean isFirst;//是否为第一页
	private boolean isLast;// 是否为最后一页
	private boolean hasPrev;// 是否有前一页
	private boolean hasNext;// 是否有下一页
	
	public static int DEFAULT_PAGESIZE = 10;
	
	public Pager(int curPage, int pageSize, int totalRecord) {
		if (totalRecord<=0) {
			this.curPage=0;
			this.pageSize=0;
			this.totalRecord=0;
			this.from=0;
			this.totalPage=0;
			this.to=0;
		}else {
			if (curPage<=0) {
				curPage=1;
			}
			if (pageSize<=0) {
				pageSize=DEFAULT_PAGESIZE;
			}
			this.curPage=curPage;
			this.pageSize=pageSize;
			this.totalRecord=totalRecord;
			this.from=(this.curPage-1)*pageSize+1;
			this.totalPage=(this.totalRecord%this.pageSize!=0)?this.totalRecord/this.pageSize+1:this.totalRecord/this.pageSize;
			this.to=this.curPage==this.totalPage?this.totalRecord:this.curPage*this.pageSize;
			
		}
	}
	public Pager(int curPage, int pageSize, long totalRecord) {
		this(curPage,pageSize,(int)totalRecord);
	}
	
	/**
	 * 当总记录数totalRecord==0时,返回false
	 * @return
	 */
	public boolean isFirst() {
		if (this.curPage==1) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 当总记录数totalRecord==0时,返回false
	 * @return
	 */
	public boolean isHasNext() {
		if (this.curPage<this.totalPage) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 当总记录数totalRecord==0时,返回false
	 * @return
	 */
	public boolean isHasPrev() {
		if (this.curPage>1) {
			return true;
		}else {
			return false;
		}
	}

	/**
	 * 当总记录数totalRecord等于0时,返回false
	 * @return
	 */
	public boolean isLast() {
		if (this.curPage==this.totalPage && this.totalRecord > 0) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 当总记录数totalRecord==0时,返回0
	 * @return
	 */
	public int getCurPage() {
		return curPage;
	}
	
	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}
	

	/**
	 * 当总记录数totalRecord==0时,返回0
	 * @return
	 */
	public int getFrom() {
		return from;
	}
	public void setFrom(int from) {
		this.from = from;
	}
	
	/**
	 * 当总记录数totalRecord==0时,返回0
	 * @return
	 */
	public int getTo() {
		return to;
	}
	public void setTo(int to) {
		this.to = to;
	}

	/**
	 * 当总记录数totalRecord==0时,返回0
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 当总记录数totalRecord==0时,返回0
	 * @return
	 */
	public int getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}

	/**
	 * 当总记录数totalRecord==0时,返回0
	 * @return
	 */
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
}
