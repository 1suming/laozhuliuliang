package com.zhuwenda.marksix.bean;

import com.zhuwenda.marksix.enums.ChannelType;

import java.util.Date;

/**
 * Created by wendazhu on 16/4/19.
 */
public class ChannelUser {

    private String userid;
    private ChannelType channelType;
    private Date createTime;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
