package com.zhuwenda.marksix.bean;


public class Parameter {

    public static final String KEY_SHARE_TIPS = "share_tips";
    public static final String KEY_WELCOME_TIPS = "welcome_tips";
    public static final String KEY_COMBINATION_BG_IMAGE = "combination_bg_image";
    /** 自动退款限额（当退款金额不高于此值时，则自动退款），单位：分 */
    public static final String KEY_AUTO_REFUND_LIMIT_VALUE = "auto_refund_limit_value";
    /** 自动退款开关，当且仅当值为“ON”时开启自动退款功能 */
    public static final String KEY_AUTO_REFUND_SWITCH = "auto_refund_switch";
    /** 自动提现限额（当退款金额不高于此值时，则自动提现），单位：分 */
    public static final String KEY_AUTO_WITHDRAW_LIMIT_VALUE = "auto_withdraw_limit_value";
    /** 自动提现开关，当且仅当值为“ON”时开启自动提现功能 */
    public static final String KEY_AUTO_WITHDRAW_SWITCH = "auto_withdraw_switch";

    private String paramKey;
    private String paramValue;
    private String paramName;
    private String paramDesc;

    public String getParamKey() {
        return paramKey;
    }

    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamDesc() {
        return paramDesc;
    }

    public void setParamDesc(String paramDesc) {
        this.paramDesc = paramDesc;
    }
}
