package com.zhuwenda.marksix.bean;

/**
 * Created by wendazhu on 15/11/7.
 */
public class Product {

    private String productid;
    private int price;
    private Integer vipPrice;
    private Integer onSale;


    private String name;
    /** 适用运营商(1.中国移动，2.中国联通，3.中国电信 */
    private Integer carrier;
    /** 适用省份（广东，湖南，湖北。。。） */
    private String province;
    /** 外部接口产品ID */
    private String outerProductid;
    /** 外部接口所需扩展字段1 */
    private String outerExtend1;
    /** 接口标识 */
    private String interfaceFlag;
    /** 排序号（值越大优先级越高） */
    private Integer orderNum;
    /** 是否自动下架（1为自动下架） */
    private Integer isAutoDown;


    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Integer getVipPrice() {
        return vipPrice;
    }

    public void setVipPrice(Integer vipPrice) {
        this.vipPrice = vipPrice;
    }

    public Integer getOnSale() {
        return onSale;
    }

    public void setOnSale(Integer onSale) {
        this.onSale = onSale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCarrier() {
        return carrier;
    }

    public void setCarrier(Integer carrier) {
        this.carrier = carrier;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getOuterProductid() {
        return outerProductid;
    }

    public void setOuterProductid(String outerProductid) {
        this.outerProductid = outerProductid;
    }

    public String getOuterExtend1() {
        return outerExtend1;
    }

    public void setOuterExtend1(String outerExtend1) {
        this.outerExtend1 = outerExtend1;
    }

    public String getInterfaceFlag() {
        return interfaceFlag;
    }

    public void setInterfaceFlag(String interfaceFlag) {
        this.interfaceFlag = interfaceFlag;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getIsAutoDown() {
        return isAutoDown;
    }

    public void setIsAutoDown(Integer isAutoDown) {
        this.isAutoDown = isAutoDown;
    }
}
