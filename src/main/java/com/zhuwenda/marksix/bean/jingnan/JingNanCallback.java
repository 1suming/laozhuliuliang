package com.zhuwenda.marksix.bean.jingnan;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.zhuwenda.marksix.util.DigestTools;

public class JingNanCallback {

    private String taskid;
    private String mobile;
    private String id;
    @JsonProperty("oper_time")
    private String operTime;
    private String result;
    private String resultdesc;
    private String sign;

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOperTime() {
        return operTime;
    }

    public void setOperTime(String operTime) {
        this.operTime = operTime;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResultdesc() {
        return resultdesc;
    }

    public void setResultdesc(String resultdesc) {
        this.resultdesc = resultdesc;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public static boolean checkSing(JingNanCallback cb,String key){
        String toBeMd5 = null;
        if(cb.getResultdesc() == null){
            toBeMd5 = String.format("id=%s&mobile=%s&oper_time=%s&result=%s&taskid=%s&key=%s"
                    ,cb.getId(),cb.getMobile(),cb.getOperTime(),cb.getResult(),cb.getTaskid(),key);
        }else{
            toBeMd5 = String.format("id=%s&mobile=%s&oper_time=%s&result=%s&resultdesc=%s&taskid=%s&key=%s"
                    ,cb.getId(),cb.getMobile(),cb.getOperTime(),cb.getResult(),cb.getResultdesc(),cb.getTaskid(),key);
        }
        return DigestTools.md5String(toBeMd5.toLowerCase()).toUpperCase().equals(cb.getSign());
    }
}
