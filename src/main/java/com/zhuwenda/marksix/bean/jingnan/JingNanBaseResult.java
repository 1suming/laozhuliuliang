package com.zhuwenda.marksix.bean.jingnan;


import com.fasterxml.jackson.annotation.JsonProperty;

public class JingNanBaseResult {

    @JsonProperty("Code")
    private String code;
    @JsonProperty("Message")
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
