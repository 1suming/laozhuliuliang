package com.zhuwenda.marksix.bean.result;

import com.zhuwenda.marksix.enums.ErrorInfo;

/**
 * Created by wendazhu on 15/11/24.
 */
public class ResultBuilder {

    public static Result success(){
        return success(null, "");
    }

    public static Result success(Object data){
        return success(data, "成功");
    }
    public static Result success(Object data, String successMsg){
        Result result = new Result();
        result.setErrcode(0);
        result.setErrmsg(successMsg);
        result.setData(data);

        return result;
    }


    public static Result error(int errcode){
        if(errcode <= 0){
            throw new IllegalArgumentException("errcode 必须大于 0");
        }
        String errmsg = Errcode.getMsg(errcode);

        Result result = new Result();
        result.setErrcode(errcode);
        result.setErrmsg(errmsg);

        return result;
    }

    public static Result error(ErrorInfo errorInfo){
        Result result = new Result();
        result.setErrcode(errorInfo.getErrcode());
        result.setErrmsg(errorInfo.getErrmsg());
        return result;
    }

    public static Result unknownError(){
        Result result = new Result();
        result.setErrcode(-1);
        result.setErrmsg("未知异常");

        return result;
    }

}
