package com.zhuwenda.marksix.bean.result;

/**
 * Created by wendazhu on 15/11/24.
 */
public class Result{
    private int errcode;
    private String errmsg;
    private Object data;

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
