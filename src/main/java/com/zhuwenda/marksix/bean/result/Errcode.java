package com.zhuwenda.marksix.bean.result;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by wendazhu on 15/11/24.
 */
public class Errcode {


    private static final Logger logger = Logger.getLogger(Errcode.class);
    private static final Properties properties = new Properties();

    public static int BALANCE_NOT_ENOUGH = 40001;
    public static int INVALID_COMMISSION_TYPE = 40002;

    static {
        try {

            InputStream inputStream = Errcode.class.getClassLoader().getResourceAsStream("/error.properties");
            properties.load(inputStream);
            inputStream.close();

        }catch (IOException e){
            throw new IllegalArgumentException("/error.propertis 配置文件读取失败！");
        }
    }

    public static String getMsg(int errcode){
        String errmsg = properties.getProperty(Integer.toString(errcode));
        if (errmsg == null){
            logger.error("error.properties中不存在key为" + errcode + "的配置项");
        }
        return errmsg;
    }

}
