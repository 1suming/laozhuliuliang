package com.zhuwenda.marksix.dto;

/**
 * 分销产品配置
 */
public class DistProductConfigDto {

    private String productid;
    private int onSale;
    private int distPrice;

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public int getOnSale() {
        return onSale;
    }

    public void setOnSale(int onSale) {
        this.onSale = onSale;
    }

    public int getDistPrice() {
        return distPrice;
    }

    public void setDistPrice(int distPrice) {
        this.distPrice = distPrice;
    }
}
