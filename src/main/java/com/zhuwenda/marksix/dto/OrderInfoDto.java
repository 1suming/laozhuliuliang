package com.zhuwenda.marksix.dto;

import com.zhuwenda.marksix.bean.Order;

/**
 * Created by wendazhu on 16/4/6.
 */
public class OrderInfoDto extends Order {

    private String productName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
