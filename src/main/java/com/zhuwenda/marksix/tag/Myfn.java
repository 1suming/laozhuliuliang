package com.zhuwenda.marksix.tag;


import com.zhuwenda.marksix.bean.Withdraw;

public class Myfn {

    public static String withdrawStatus(int status){
        if(status == Withdraw.STATUS_APPLYING){
            return "申请中";
        }else if(status == Withdraw.STATUS_FAILED){
            return "失败";
        }else if(status == Withdraw.STATUS_SUCCESS){
            return "成功";
        }else {
            return "未知";
        }
    }
}
