package com.zhuwenda.marksix.config;

import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;

@Component
public class WxConfig extends WxMpInMemoryConfigStorage{

    @Value("${weixin.appid}")
    private String appid;
    @Value("${weixin.secret}")
    private String secret;
    @Value("${weixin.token}")
    private String token;
    @Value("${weixin.aeskey}")
    private String aeskey;

    @Value("${pay.weixin.partnerid}")
    private String partnerId;
    @Value("${pay.weixin.key}")
    private String payKey;
    @Value("${pay.weixin.sub.partnerid}")
    private String subPartnerId;
    @Value("${pay.weixin.certification}")
    private String certification;


    @PostConstruct
    public void init(){
        setAppId(appid);
        setSecret(secret);
        setToken(token);
        setAesKey(aeskey);
        setPartnerId(partnerId);
        setPartnerKey(payKey);
        setSubPartnerId(subPartnerId);
        if(certification != null && !"".equals(certification.trim())){
            try {

                KeyStore keyStore  = KeyStore.getInstance("PKCS12");
                FileInputStream instream = new FileInputStream(new File(certification));
                try {
                    keyStore.load(instream, partnerId.toCharArray());
                } finally {
                    instream.close();
                }
                // Trust own CA and all self-signed certs
                SSLContext sslcontext = SSLContexts.custom()
                        .loadKeyMaterial(keyStore, partnerId.toCharArray())
                        .build();
                setSSLContext(sslcontext);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
