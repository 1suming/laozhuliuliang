package com.zhuwenda.marksix.cache;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;


public class CacheManager {
    private static final Logger logger = Logger.getLogger(CacheManager.class);

    private static final Map<String,Object> cacheMap = new HashMap<String,Object>();

    private CacheManager(){

    }

    public synchronized static Object get(String key){
        if(cacheMap.containsKey(key)){
            return cacheMap.get(key);
        }
        return null;
    }

    public synchronized static void put(String key,Object value){
        cacheMap.put(key,value);
    }

    public synchronized static boolean exist(String key){
        return cacheMap.containsKey(key);
    }

    public synchronized static Object remove(String key){
        return cacheMap.remove(key);
    }

    public synchronized static void clear(){
        cacheMap.clear();;
    }
}
