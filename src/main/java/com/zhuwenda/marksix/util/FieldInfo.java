package com.zhuwenda.marksix.util;

import java.lang.reflect.Type;

public class FieldInfo {

    /** java字段名 */
    private String fieldName;
    /** 数据库字段名 */
    private String columnName;

    private Type type;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }


    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
