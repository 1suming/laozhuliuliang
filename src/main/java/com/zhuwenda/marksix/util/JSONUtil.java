package com.zhuwenda.marksix.util;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by legion on 2016/4/1.
 */
public class JSONUtil {

    public static String toJSON(Object object){
        try {
            String ret = new ObjectMapper().writeValueAsString(object);
            return ret;
        }catch (Exception e ){
            return null;
        }
    }

    public static void print(Object object){
        System.out.println(JSONUtil.toJSON(object));
    }

}
