package com.zhuwenda.marksix.util;

import java.util.*;

/**
 * Created by legion on 2016/3/31.
 */
public class HttpInterfaceUtil {

    public static String toOrderAndNotNullValueUrlString(Map<String,String> param){
        Map<String,String> notNullValueMap = new TreeMap<>();
        if(param != null && param.size()>0){
            Iterator<String> keys = param.keySet().iterator();
            while (keys.hasNext()){
                String key = keys.next();
                if(param.get(key) != null && !"".equals(param.get(key).trim())){
                    notNullValueMap.put(key, param.get(key));
                }
            }
        }
        return toUrlString(notNullValueMap);
    }

    public static String toUrlString(Map<String,String> param){
        StringBuffer sb = new StringBuffer();
        if(param.size() > 0){
            Iterator<Map.Entry<String,String>> it = param.entrySet().iterator();
            boolean isFirstLoop = true;
            while (it.hasNext()){
                Map.Entry<String,String> entry = it.next();
                if(isFirstLoop){
                    isFirstLoop = false;
                }else {
                    sb.append("&");
                }
                sb.append(entry.getKey());
                sb.append("=");
                sb.append(entry.getValue() != null ? entry.getValue() : "");
            }
        }
        return sb.toString();
    }
}
