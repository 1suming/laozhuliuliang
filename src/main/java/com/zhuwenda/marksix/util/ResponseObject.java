package com.zhuwenda.marksix.util;

/**
 * Created by wendazhu on 15/9/28.
 */
public class ResponseObject<T> {

    private int errcode;
    private T data;

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static ResponseObject error(int code,Object data){
        if(code == 0){
            throw new IllegalArgumentException("code 不能为 0.");
        }
        ResponseObject responseObject = new ResponseObject();
        responseObject.setErrcode(code);
        responseObject.setData(data);
        return responseObject;
    }

    public static ResponseObject ok(Object data){

        ResponseObject responseObject = new ResponseObject();
        responseObject.setErrcode(0);
        responseObject.setData(data);
        return responseObject;
    }
}
