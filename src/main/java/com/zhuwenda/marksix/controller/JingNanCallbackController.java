package com.zhuwenda.marksix.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.annotation.Anonymous;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.bean.TopupFailType;
import com.zhuwenda.marksix.bean.jingnan.JingNanCallback;
import com.zhuwenda.marksix.bean.jingnan.JingNanCbResponse;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.ProductService;
import com.zhuwenda.marksix.service.UserCommissionService;
import com.zhuwenda.marksix.service.WxTemplateMsgService;
import com.zhuwenda.marksix.util.ObjectMapperFactory;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletInputStream;

@Controller
@RequestMapping(value="/jingnan")
public class JingNanCallbackController {

    private static final Logger logger = LoggerFactory.getLogger(JingNanCallbackController.class);

    @Value("${liuliang.jingnan.key}")
    private String key;

    @Autowired
    private OrderService orderService;
    @Autowired
    private UserCommissionService userCommissionService;
    @Autowired
    private WxTemplateMsgService wxTemplateMsgService;
    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/callback",method = RequestMethod.POST)
    @ResponseBody
    @Anonymous
    public JingNanCbResponse callback(ServletInputStream inputStream){
        JingNanCbResponse ret = JingNanCbResponse.systemError();
        try{
            byte[] bytes = IOUtils.toByteArray(inputStream);
            String cb = new String(bytes,"UTF-8");
            ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();
            JingNanCallback callback = objectMapper.readValue(cb, JingNanCallback.class);

            //特殊处理，有条订单老是回调验证签名失败
            if(callback != null && "jncl178201571471458188372055933751".equals(callback.getTaskid())){
                return JingNanCbResponse.businessError("跳过此订单");
            }

            logger.info("回调报文：{}",cb);
            if(JingNanCallback.checkSing(callback,key)){
                Order order = orderService.queryOrderByOuterOrderid(callback.getTaskid());
                if(order != null){
                    if(order.getTopupStatus() == Constant.ORDER_TOPUP_STATUS_TOPUPING){

                        Product product = productService.queryByProductid(order.getProductid());
                        if ("0".equals(callback.getResult())) {
                            orderService.orderSuccessNotification(order, product);
                        } else {
                            logger.warn("京南流量平台返回的订单充值结果为失败");
                            String errMsg = String.format("result:%s.resultDesc:%s", callback.getResult(), callback.getResultdesc());
                            orderService.orderFailedNotification(order, product, errMsg, TopupFailType.NORMAL);
                        }
                        ret = JingNanCbResponse.success("成功");

                    }else {
                        logger.info("订单充值状态不为充值中");
                        ret = JingNanCbResponse.success("成功（状态不为充值中）");
                    }
                }else{
                    logger.error("找不到订单");
                    ret = JingNanCbResponse.businessError("找不到订单");
                }
            }else {
                logger.warn("验证签名失败");
                ret = JingNanCbResponse.businessError("验证签名失败");
            }
        } catch (Exception e){
            logger.error("京南充值回调异常",e);
            ret = JingNanCbResponse.systemError("异常：" + e.getMessage());

        }
        return ret;
    }

}
