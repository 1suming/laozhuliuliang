package com.zhuwenda.marksix.controller;

import com.zhuwenda.marksix.bean.result.Result;
import com.zhuwenda.marksix.bean.result.ResultBuilder;
import com.zhuwenda.marksix.enums.ErrorInfo;
import com.zhuwenda.marksix.exception.BalanceNotEnoughException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ControllerExceptionAdvice {

    private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionAdvice.class);

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public Result exception(RuntimeException e){
        logger.error(e.getMessage(),e);
        return ResultBuilder.error(ErrorInfo.UNKNOW_EXCEPTION);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    public Result exception(IllegalArgumentException e){
        logger.error(e.getMessage(),e);
        return ResultBuilder.error(ErrorInfo.ILLEGAL_ARGUMENT_EXCEPTION);
    }

    @ExceptionHandler(BalanceNotEnoughException.class)
    @ResponseBody
    public Result exception(BalanceNotEnoughException e){
        logger.error(e.getMessage(),e);
        return ResultBuilder.error(ErrorInfo.BALANCE_NOT_ENOUGH);
    }

    @ExceptionHandler(NullPointerException.class)
    @ResponseBody
    public Result exception(NullPointerException e){
        logger.error(e.getMessage(),e);
        return ResultBuilder.error(ErrorInfo.INFO_MISSING);
    }

    @ExceptionHandler(NumberFormatException.class)
    @ResponseBody
    public Result exception(NumberFormatException e){
        logger.error(e.getMessage(),e);
        return ResultBuilder.error(ErrorInfo.INVALID_NUMBER);
    }



}
