package com.zhuwenda.marksix.controller;

import com.aspire.portal.web.security.client.VerifySignature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.annotation.Anonymous;
import com.zhuwenda.marksix.bean.Order;
import com.zhuwenda.marksix.bean.Product;
import com.zhuwenda.marksix.bean.TopupFailType;
import com.zhuwenda.marksix.bean.jingnan.JingNanCbResponse;
import com.zhuwenda.marksix.service.OrderService;
import com.zhuwenda.marksix.service.ProductService;
import com.zhuwenda.marksix.service.UserCommissionService;
import com.zhuwenda.marksix.service.WxTemplateMsgService;
import com.zhuwenda.marksix.util.HttpInterfaceUtil;
import com.zhuwenda.marksix.util.ObjectMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping(value="/zhuowang")
public class ZhuoWangCallbackController {

    private static final Logger logger = LoggerFactory.getLogger(ZhuoWangCallbackController.class);

    @Value("${liuliang.zhuowang.key}")
    private String key;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;
    @Autowired
    private WxTemplateMsgService wxTemplateMsgService;
    @Autowired
    private UserCommissionService userCommissionService;


    @RequestMapping(value = "/callback",method = RequestMethod.POST)
    @ResponseBody
    @Anonymous
    public JingNanCbResponse callback(HttpServletRequest request,String sequence,String result){
        logger.info("卓望充值回调");
        JingNanCbResponse ret = JingNanCbResponse.systemError();
        try{
            Map<String,String[]> paramIn = request.getParameterMap();
            if(paramIn.size() > 0){
                logger.info("回调参数：{}", ObjectMapperFactory.getObjectMapper().writeValueAsString(paramIn));
            }
            Map<String,String> param = new HashMap<>();
            Enumeration<String> it = request.getParameterNames();
            String signToCheck = null;
            String resultDesc = null;
            while (it.hasMoreElements()){
                String name = it.nextElement();
                if("sign".equals(name)){
                    signToCheck = request.getParameter(name);
                }else if ("resultDesc".equals(name)) {
                    String value = new String(request.getParameter("resultDesc").getBytes("ISO-8859-1"),"utf-8");
                    param.put(name,value);
                    resultDesc = value;
                } else {
                    param.put(name, request.getParameter(name));
                }
            }
            String toSignString = HttpInterfaceUtil.toOrderAndNotNullValueUrlString(param);
            VerifySignature vs = new VerifySignature();
            String path = ZhuoWangCallbackController.class.getClassLoader().getResource("public.key").getFile();
            boolean isPassed = vs.verify(toSignString,signToCheck,path);

            if(isPassed){
                Order order = orderService.queryOrderByOuterOrderid(sequence);
                if(order != null){
                    if(order.getTopupStatus() == Constant.ORDER_TOPUP_STATUS_TOPUPING){

                        Product product = productService.queryByProductid(order.getProductid());
                        if ("0".equals(result)) {
                            orderService.orderSuccessNotification(order, product);
                        } else {
                            logger.warn("卓望流量平台返回的订单充值结果为失败");
                            String errMsg = String.format("result:%s.resultDesc:%s", result, resultDesc);
                            orderService.orderFailedNotification(order, product, errMsg, TopupFailType.NORMAL);
                        }
                        ret = JingNanCbResponse.success("成功");

                    }else {
                        logger.info("订单充值状态不为充值中，不再处理");
                        ret = JingNanCbResponse.success("成功（状态不为充值中）");
                    }
                }else{
                    logger.error("找不到订单");
                    ret = JingNanCbResponse.businessError("找不到订单");
                }
            }else {
                logger.warn("验证签名失败");
                ret = JingNanCbResponse.businessError("验证签名失败");
            }
        } catch (Exception e){
            logger.error("卓望充值回调回调异常",e);

        }
        return ret;
    }

}
