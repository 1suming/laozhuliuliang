package com.zhuwenda.marksix.controller;

import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.*;
import com.zhuwenda.marksix.enums.ChannelType;
import com.zhuwenda.marksix.service.*;
import com.zhuwenda.marksix.util.WebUtil;
import me.chanjar.weixin.mp.bean.result.WxMpPayCallback;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/** http://www.jb51.net/article/72035.htm */
@Controller
@RequestMapping(value = "/pay")
public class PayController {

    private static final Logger logger = Logger.getLogger(PayController.class);

    @Value("${weixin.appid}")
    private String appid;
    @Value("${pay.weixin.key}")
    private String payKey;
    @Value("${pay.weixin.partnerid}")
    private String mchId;
    @Value("${pay.weixin.notify.url}")
    private String notifyUrl;

    @Autowired
    private WxService wxService;
    @Autowired
    private WxTemplateMsgService wxTemplateMsgService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderNotification orderNotification;
    @Autowired
    private WxuserService wxuserService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ChannelService distributionProductService;
    @Autowired
    private BillService billService;
    @Autowired
    private ChannelService channelService;



    /** 创建订单 */
    @RequestMapping(value = "/wx/order",method = {RequestMethod.POST})
    @ResponseBody
    public Map createOrder(String productid,String phone,String sourceOpenid,ChannelType channelType,String channelId,HttpServletRequest request){
        logger.info("准备创建订单");
        Map<String,Object> responseResult = new HashMap<String, Object>();
        responseResult.put("errcode", -1);


        String openid = (String)request.getSession().getAttribute("openid");
        String tradeType = "JSAPI";
        String ip = WebUtil.getIpAddr(request);

        //---- 参数校验
        if(openid == null){
            return new HashMap();
        }

        Order order = new Order();
        order.setOpenid(openid);
        order.setIp(ip);
        Random random = new Random();
        int randomNumber = random.nextInt(999) + 1000;
        order.setOrderid(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + randomNumber);
        order.setCreateTime(new Date());
        order.setPayStatus(Constant.ORDER_PAY_STATUS_NOT_PAY);
        order.setTopupMobile(phone);
        order.setTopupStatus(Constant.ORDER_TOPUP_STATUS_NOT_SUBMITED);
        order.setProductid(productid);
        order.setNum(1);
        order.setSourceOpenid(sourceOpenid);

        Product product = productService.queryByProductid(productid);


        String productDesc = null;
        if(product != null){
            order.setFeeAmount(product.getPrice());
            order.setChannelType(channelType);
            order.setChannelId(channelId);
            if(product.getOnSale() != 1){
                return errResponse(-1,"平台产品已下架");
            }
            if(channelType != null){//渠道产品
                if(!StringUtils.hasText(channelId)){
                    return errResponse(-1,"渠道信息不全");
                }
                if(ChannelType.DIST_IN_SYSTEM.equals(channelType)){//分销渠道订单
                    ChannelDistributionProductInfoDto distPro = channelService.querySellableProducts(channelId,productid);
                    order.setFeeAmount(distPro.getChannelDistPrice());
                }else {
                    throw new IllegalArgumentException("未知渠道");
                }
            }else if(wxuserService.isVip(openid)){
                logger.info("这是个vip用户，openid：" + openid);
                if(product.getVipPrice() != null && product.getVipPrice() > 0){
                    order.setFeeAmount(product.getVipPrice());
                }else {
                    logger.info("此产品的vip价格未设置为 > 0，所以使用非vip价格，产品ID为："+product.getProductid());
                }
            }
            productDesc = product.getName();

        }else{
            logger.warn("---------未支持的产品");
            throw new IllegalArgumentException("不支持的产品");
        }
        int retCode = orderService.createOrder(order);
        if(retCode == 0){
            Map<String,String> payInfo = wxService.getJSSDKPayInfo(order.getOpenid(), order.getOrderid(), order.getFeeAmount(), productDesc, tradeType, order.getIp(), notifyUrl);
            String payid = payInfo.get("package").split("=")[1];
            orderService.markOrderPayid(order.getOrderid(),payid);
            responseResult.put("payInfo",payInfo);
            responseResult.put("orderid",order.getOrderid());
            responseResult.put("errcode",retCode);
        }


        return responseResult;
    }

    private Map<String,Object> errResponse(int errcode,String errmsg){
        if(errcode == 0){
            throw new IllegalArgumentException("errcode不能为0");
        }
        Map<String,Object> map = new HashMap<>();
        map.put("errcode", errcode);
        map.put("errmsg",errmsg);
        return map;
    }


    @RequestMapping(value = "/wx/callback",method = {RequestMethod.POST})
    public void wxPayCallback(HttpServletRequest request,HttpServletResponse response) throws IOException{
        logger.info("----------- 微信支付回调 --------");
        String xmlData = "";
        BufferedReader bf = null;
        try {
            bf = new BufferedReader(new InputStreamReader(request.getInputStream()));
            String line = null;
            while((line = bf.readLine()) != null){
                xmlData += line;
            }
        } catch (IOException e) {
            logger.error(e);
        } finally {
            bf.close();
        }
        WxMpPayCallback wxMpPayCallback = wxService.getJSSDKCallbackData(xmlData);
        String orderid = wxMpPayCallback.getOut_trade_no();
        Order order= orderService.queryOrderByOrderid(orderid);
        if(order != null){
            if(order.getPayStatus() == 1){
                logger.info("重复支付：订单已支付.订单号："+orderid);
            }else{

                if(order.getOpenid().equals(wxMpPayCallback.getOpenid())){
                    if( "SUCCESS".equalsIgnoreCase(wxMpPayCallback.getReturn_code())
                            && "SUCCESS".equalsIgnoreCase(wxMpPayCallback.getResult_code())){


                        int retCode = orderService.markOrderPaySuccess(order.getOrderid());

                        if(retCode == 0){

                            logger.info("修改支付状态成功");
                            Product product = productService.queryByProductid(order.getProductid());

                            DecimalFormat decimalFormat = new DecimalFormat("0.00");
                            BigDecimal totalFee = new BigDecimal(wxMpPayCallback.getTotal_fee());
                            BigDecimal amount = totalFee.divide(new BigDecimal("100"));
                            wxTemplateMsgService.orderPaySuccess(wxMpPayCallback.getOpenid(), decimalFormat.format(amount) + "元", product.getName());
                            logger.info("准备通知产品，ID为：" + product != null ? product.getProductid() : "null");
                            if(order.getChannelType() != null && ChannelType.DIST_IN_SYSTEM.equals(order.getChannelType()) && order.getChannelId() != null){
                                logger.info("这是一条分销订单");
                                billService.distAutoTopup(order.getChannelId(),totalFee.intValue(),orderid);//分销商自动充值
                            }
                            orderNotification.setChanged();
                            orderNotification.notify(order,product);

                        }else {
                            logger.info("修改支付状态失败");
                        }
                    }else {
                        logger.warn("微信通知支付结果失败");
                    }
                }else{
                    logger.info("支付回调时，信息不匹配");
                }
            }
        } else {
            logger.info("找不到该订单号：" + orderid);
        }
        logger.info(wxMpPayCallback.toString());
        logger.info("------------");

        String returnXml = "<xml><return_code><![CDATA[%s]]></return_code><return_msg><![CDATA[%s]]></return_msg></xml>";
        response.getWriter().write(String.format(returnXml, "SUCCESS", "OK"));
        return ;
    }

}
