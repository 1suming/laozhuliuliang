package com.zhuwenda.marksix.controller;

import com.zhuwenda.marksix.Constant;
import com.zhuwenda.marksix.bean.*;
import com.zhuwenda.marksix.bean.result.Errcode;
import com.zhuwenda.marksix.bean.result.Result;
import com.zhuwenda.marksix.bean.result.ResultBuilder;
import com.zhuwenda.marksix.config.WxConfig;
import com.zhuwenda.marksix.dto.DistProductConfigDto;
import com.zhuwenda.marksix.enums.ChannelType;
import com.zhuwenda.marksix.enums.ErrorInfo;
import com.zhuwenda.marksix.service.*;
import com.zhuwenda.marksix.util.EmojiFilter;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.common.util.StringUtils;
import me.chanjar.weixin.mp.api.*;
import me.chanjar.weixin.mp.bean.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.result.WxMpQrCodeTicket;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.chanjar.weixin.mp.bean.result.WxMpUserList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class WechatController {

    private static final Logger logger = LoggerFactory.getLogger(WechatController.class);

    /** 分享二维码过期自动刷新提前时间 */
    private static final long QRCODE_INEFFICIENT_PRE_TIME = 3L * 24 * 60 * 60 * 1000;

    @Value("${weixin.manager.openids}")
    private String managerOpenids;
    @Autowired
    private WxConfig wxConfig;
    @Autowired
    private WxService wxService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private WxuserService wxuserService;
    @Autowired
    private ParameterService parameterService;
    @Autowired
    private UserCommissionService userCommissionService;
    @Autowired
    private WithdrawService withdrawService;
    @Autowired
    private WxuserShareCodeService wxuserShareCodeService;
    @Autowired
    private ParameterQrcodeService parameterQrcodeService;
    @Autowired
    private ProductService productService;
    @Autowired
    private WxTemplateMsgService  wxTemplateMsgService;
    @Autowired
    private ChannelService channelService;



    private WxMpMessageRouter wxMpMessageRouter;

    @PostConstruct
    public void init(){

        wxMpMessageRouter = new WxMpMessageRouter(wxService);
        wxMpMessageRouter

                .rule()
                .async(false)
                .msgType(WxConsts.XML_MSG_EVENT)
                .event(WxConsts.EVT_SUBSCRIBE)//关注事件
                .handler(new WxMpMessageHandler() {
                    @Override
                    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager) throws WxErrorException {
                        logger.info(wxMessage.toString());
                        String eventKey = wxMessage.getEventKey();

                        if (eventKey != null && !"".equals(eventKey.trim())){//扫描带场景之的二维码关注

                            String eventKeyParsed = eventKey.substring("qrscene_".length());
                            String qrsceneKey = eventKeyParsed.substring(0, 4);
                            String qrsceneValue = eventKeyParsed.substring(4);

                            logger.info("qrsceneKey：" + qrsceneKey);
                            logger.info("qrsceneValue：" + qrsceneValue);
                            int keyInt = Integer.parseInt(qrsceneKey);
                            if (WxuserShareCode.SHARE_TYPE_INTRODUCE == keyInt || WxuserShareCode.SHARE_TYPE_PERMANENT_OFFLINE == keyInt) {//推荐人

                                WxMpUser wxMpUser = wxService.userInfo(wxMessage.getFromUserName(),null);
                                Wxuser wxuser = new Wxuser();
                                wxuser.setOpenid(wxMpUser.getOpenId());
                                wxuser.setSubscribe(1);
                                wxuser.setSubscribeTime(new Date(wxMpUser.getSubscribeTime() * 1000L));
                                wxuser.setHeadimgurl(wxMpUser.getHeadImgUrl());
                                wxuser.setNickname(EmojiFilter.filterEmoji(wxMpUser.getNickname()));

                                WxuserShareCode wxuserShareCode = wxuserShareCodeService.queryByShareCode(qrsceneValue,keyInt);
                                if (wxuserShareCode != null) {
                                    logger.info("推荐人用户openid：" + wxuserShareCode.getOpenid());
                                    wxuser.setRecommender(wxuserShareCode.getOpenid());
                                }
                                if (wxuser.getOpenid().equals(wxuser.getRecommender())) {
                                    logger.warn("推荐人openid和被推荐人openid一致，不新增记录");
                                } else {
                                    wxuserService.addUserNotExist(wxuser);

                                    //--------- 发送模板消息
                                    String recommenderOpenid = wxuser.getRecommender();
                                    String recommenderNickname = wxService.userInfo(recommenderOpenid,null).getNickname();
                                    if(recommenderNickname == null){
                                        Wxuser recommenderWxuser = wxuserService.queryUserByOpenid(recommenderOpenid);
                                        if(recommenderWxuser != null && recommenderWxuser.getNickname() != null){
                                            recommenderNickname = recommenderWxuser.getNickname();
                                        }
                                    }
                                    wxTemplateMsgService.inviteSubscribe(wxuser.getRecommender(),wxMpUser.getNickname(),new Date(),recommenderNickname);
                                    //--------
                                }
                            }

                            WxMpXmlOutMessage ret = WxMpXmlOutMessage.TEXT().content("欢迎关注掌上流量平台！").fromUser(wxMessage.getToUserName()).toUser(wxMessage.getFromUserName()).build();
                            logger.info(ret.toString());
                            return ret;
                        }else{//普通关注
                            if(!wxuserService.isExist(wxMessage.getFromUserName())){
                                WxMpUser wxMpUser = wxService.userInfo(wxMessage.getFromUserName(),null);
                                Wxuser wxuser = new Wxuser();
                                wxuser.setOpenid(wxMpUser.getOpenId());
                                wxuser.setSubscribe(1);
                                wxuser.setSubscribeTime(new Date(wxMpUser.getSubscribeTime()*1000L));
                                wxuser.setHeadimgurl(wxMpUser.getHeadImgUrl());
                                wxuser.setNickname(EmojiFilter.filterEmoji(wxMpUser.getNickname()));

                                wxuserService.addUser(wxuser);
                            }
                            WxMpXmlOutMessage ret = WxMpXmlOutMessage.TEXT().content("你好，欢迎！").fromUser(wxMessage.getToUserName()).toUser(wxMessage.getFromUserName()).build();
                            logger.info(ret != null ? ret.toString() : "");
                            return ret;
                        }
                    }
                })
                .end()

                .rule()
                .async(false)
                .msgType(WxConsts.XML_MSG_EVENT)
                .event(WxConsts.EVT_UNSUBSCRIBE)//取消关注事件
                .handler(new WxMpMessageHandler() {
                    @Override
                    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager) throws WxErrorException {
                        logger.info(wxMessage.toString());
                        Wxuser wxuser = wxuserService.queryUserByOpenid(wxMessage.getFromUserName());
                        if(wxuser != null){
                            wxuser.setSubscribe(0);
                            wxuserService.updateUser(wxuser);
                        }
                        return null;
                    }
                })
                .end()

                .rule()
                .async(false)
                .handler(new WxMpMessageHandler() {
                    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager) throws WxErrorException {
                        logger.info(wxMessage.toString());
                        return null;
                    }
                })
                .end();


        try {
            String isDebug = "on";
            if(!"on".equals(isDebug)){

                WxMpUserList list = wxService.userList(null);
                //TODO nextOpenid 10000
                List<String> openids = list.getOpenIds();
                if(openids != null && openids.size() > 0){

                    List<WxMpUser> listTemp = wxService.userinfoBatchget(openids);
                    if(listTemp != null && listTemp.size() > 0){
                        for(int j= 0; j < listTemp.size(); j++){
                            WxMpUser wxMpUser = listTemp.get(j);
                            Wxuser wxuser = new Wxuser();
                            wxuser.setOpenid(wxMpUser.getOpenId());
                            wxuser.setHeadimgurl(wxMpUser.getHeadImgUrl());
                            wxuser.setNickname(EmojiFilter.filterEmoji(wxMpUser.getNickname()));
                            wxuserService.addOrUpdate(wxuser);
                        }
                    }
                }
            }

        }catch (Exception e){
            logger.error("获取用户列表失败",e);
        }


    }

    @RequestMapping(value = "/shareCode")
    @ResponseBody
    public Result getShareCode(HttpServletRequest request){
        String openid = (String)request.getSession().getAttribute("openid");
        String shareCode = wxuserShareCodeService.createIntroduceShareCode(openid);
        return ResultBuilder.success(shareCode);
    }

    @RequestMapping(value = "/toMyuserlist")
    public String toMyuserlist(){
        return "/wechat/toMyuserlist";
    }

    @RequestMapping(value = "/myinfo")
    @ResponseBody
    public Result myinfo(HttpServletRequest request) {
        String openid = (String)request.getSession().getAttribute("openid");
        try{
            WxMpUser wxMpUser = wxService.userInfo(openid, null);
            return ResultBuilder.success(wxMpUser);
        }catch(WxErrorException e){
            logger.error("获取个人用户信息时发生了一个异常",e);
            return ResultBuilder.unknownError();
        }
    }

    @RequestMapping(value = "/refund")
    public Result refund(HttpServletRequest request,String orderid){
        String openid = (String)request.getSession().getAttribute("openid");
//        try{
//
//        }catch(WxErrorException e){
//            logger.error("获取个人用户信息时发生了一个异常",e);
//            return ResultBuilder.unknownError();
//        }

        return ResultBuilder.unknownError();
    }


    @RequestMapping(value = "/myuserlist")
    @ResponseBody
    public Result myuserlist(HttpServletRequest request,Integer page,Integer pageSize,Integer level){
        String openid = (String)request.getSession().getAttribute("openid");
        if(level == null){
            return ResultBuilder.error(ErrorInfo.INVALID_COMMISSION_TYPE);
        }
        PaginationSupport<Map> ps = null;
        if(level == 1){
            ps = wxuserService.queryListOfLevel1(openid, page, pageSize);
        }else if(level == 2){
            ps = wxuserService.queryListOfLevel2(openid, page, pageSize);
        }else if(level == 3){
            ps = wxuserService.queryListOfLevel3(openid, page, pageSize);
        }else {
            return ResultBuilder.error(ErrorInfo.INVALID_COMMISSION_TYPE);
        }

        ps.getList().stream().forEach(e -> e.put("timestr", new SimpleDateFormat("yyyy-MM-dd HH:mm").format(((Wxuser)e.get("wxuser")).getSubscribeTime())));
//        try {
//            List<WxMpUser> wxMpUsers = wxService.userinfoBatchget(openids);
//            ps.getList().stream().forEach(e->{
//                Wxuser wxuser = (Wxuser)e.get("wxuser");
//                Optional<WxMpUser> optional = wxMpUsers.stream().filter(e2->e2.getOpenId().equals(wxuser.getOpenid())).findFirst();
//                if(optional.isPresent()){
//                    e.put("wxMpUser",optional.get());
//                }
//            });
//        }catch (WxErrorException e){
//            logger.error("批量获取用户信息时发生了一个异常",e);
//        }


        return ResultBuilder.success(ps);
    }

    @RequestMapping(value = "/expireShare",method = {RequestMethod.POST})
    @ResponseBody
    public Result getShareCode(HttpServletRequest request,int shareCode){
        String openid = (String)request.getSession().getAttribute("openid");
        Long t = Long.parseLong("" + WxuserShareCode.SHARE_TYPE_INTRODUCE + shareCode);
        WxuserShareCode wxuserShareCode =  wxuserShareCodeService.queryByShareCode(shareCode + "", WxuserShareCode.SHARE_TYPE_INTRODUCE);
        if (t > Integer.MAX_VALUE){
            throw new IllegalStateException("shareid 参数值过大:" + shareCode);
        }
        if (wxuserShareCode == null){
            throw new IllegalStateException("wxuserShareCode 不存在 shareCode:" + shareCode);
        }
        ParameterQrcode parameterQrcode = parameterQrcodeService.queryBySceneid(t.intValue() + "");
        if(parameterQrcode != null && wxuserShareCode.getOpenid().equals(openid)){
            parameterQrcode.setExpireSeconds(-1);
            parameterQrcodeService.update(parameterQrcode);
        }
        return ResultBuilder.success();
    }

    @RequestMapping(value = "/share")
    public String toIntroduce(HttpServletRequest request,int shareCode,Model model){
        Long t = Long.parseLong(""+WxuserShareCode.SHARE_TYPE_INTRODUCE +shareCode);
        WxuserShareCode wxuserShareCode = wxuserShareCodeService.queryByShareCode(shareCode + "", WxuserShareCode.SHARE_TYPE_INTRODUCE);
        if (t > Integer.MAX_VALUE){
            throw new IllegalStateException("shareid 参数值过大:" + shareCode);
        }
        if (wxuserShareCode == null){
            throw new IllegalStateException("wxuserShareCode 不存在 shareCode:" + shareCode);
        }


        Parameter shareTipsParam = parameterService.queryByKey(Parameter.KEY_SHARE_TIPS);
        Parameter combinationBgImageParam = parameterService.queryByKey(Parameter.KEY_COMBINATION_BG_IMAGE);
        model.addAttribute("shareTips", (shareTipsParam == null || shareTipsParam.getParamValue() == null) ? "掌上流量平台" : shareTipsParam.getParamValue());


        try{

            ParameterQrcode parameterQrcode = parameterQrcodeService.queryBySceneid(t.intValue() + "");
            if(parameterQrcode == null){//没有生成过二维码
                WxMpQrCodeTicket ticket = wxService.qrCodeCreateTmpTicket(t.intValue(), Constant.QRCODE_MAX_EXPIRE_SECONDS);
                parameterQrcode = new ParameterQrcode();
                parameterQrcode.setSceneid(t.intValue()+"");
                parameterQrcode.setTicket(ticket.getTicket());
                parameterQrcode.setExpireSeconds(ticket.getExpire_seconds());
                parameterQrcode.setUrl(ticket.getUrl());
                parameterQrcode.setLastUpdateTime(new Date());

                parameterQrcodeService.add(parameterQrcode);
            }else{
                int expireSeconds = parameterQrcode.getExpireSeconds();
                //到期时间点
                long inefficientTimestamp = parameterQrcode.getLastUpdateTime().getTime() + (expireSeconds*1000L);
                if(expireSeconds < 0 || (System.currentTimeMillis()+QRCODE_INEFFICIENT_PRE_TIME) > inefficientTimestamp ){//过期前提前刷新
                    WxMpQrCodeTicket ticket = wxService.qrCodeCreateTmpTicket(Integer.parseInt(parameterQrcode.getSceneid()), Constant.QRCODE_MAX_EXPIRE_SECONDS);
                    parameterQrcode.setLastUpdateTime(new Date());
                    parameterQrcode.setTicket(ticket.getTicket());
                    parameterQrcode.setUrl(ticket.getUrl());
                    parameterQrcode.setExpireSeconds(ticket.getExpire_seconds());

                    parameterQrcodeService.update(parameterQrcode);
                }
            }
            String combinationBgImagePath = null;
            if(combinationBgImageParam == null || combinationBgImageParam.getParamValue() == null || "".equals(combinationBgImageParam.getParamValue().trim())){
                combinationBgImagePath = request.getServletContext().getRealPath("/static/image/defaultToCombinate.jpg");
            }else {
                combinationBgImagePath = combinationBgImageParam.getParamValue();
            }


            WxMpUser wxMpUser = wxService.userInfo(wxuserShareCode.getOpenid(), null);
            model.addAttribute("wxMpUser", wxMpUser);

            Date inefficientDate = new Date(parameterQrcode.getLastUpdateTime().getTime() + (parameterQrcode.getExpireSeconds()*1000L));
            String headImgUrl = wxMpUser.getHeadImgUrl().substring(0, wxMpUser.getHeadImgUrl().length() - 2)+"/132";
            String qrcodeUrl = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket="+parameterQrcode.getTicket();
            String time = "该二维码将在"+new SimpleDateFormat("yyyy-MM-dd").format(inefficientDate)+"失效";

            String serverPath = "/static/image/sharetemp";
            String imgDir = request.getServletContext().getRealPath(serverPath);
            File directory = new File(imgDir);
            if(!directory.exists()){
                directory.mkdir();
            }
            File fileTemp = new File(directory,wxMpUser.getOpenId()+".jpg");
            boolean isCreateSuccess = false;
            if(!fileTemp.exists()){
                isCreateSuccess = fileTemp.createNewFile();
            }
            OutputStream os = new FileOutputStream(fileTemp);

            combinateImage(combinationBgImagePath,headImgUrl,qrcodeUrl, wxMpUser.getNickname(),time,os);
            model.addAttribute("imgPath", serverPath+"/"+wxMpUser.getOpenId()+".jpg");

        }catch (Exception e){
            logger.error("生成个人推广图片异常",e);
        }
        return "/wechat/share";
    }

    private void combinateImage(String pathBg,String headerImgUrl,String qrcodeUrl,String nickname,String inefficientTime,OutputStream outputStream) throws Exception{

        int posyOfHeader = 36;
        int headerSize = 24;
        int posyOfNickname = 53;
        int posyOfQrcode = 78;
        int qrcodeSize = 28;
        int posyOfTime = 97;
        int fontSize = 22;

        //1.jpg是你的 主图片的路径
        InputStream is = new FileInputStream(pathBg);

        BufferedImage buffImg = ImageIO.read(is);


        int bgWidth = buffImg.getWidth();
        int bgHeight = buffImg.getHeight();

        //得到画笔对象
        Graphics g = buffImg.getGraphics();

        //创建你要附加的图象。
        //2.jpg是你的小图片的路径
        ImageIcon headerIcon = new ImageIcon(new URL(headerImgUrl));
        ImageIcon qrcodeIcon = new ImageIcon(new URL(qrcodeUrl));
        //得到Image对象。
        Image img = headerIcon.getImage().getScaledInstance(bgWidth*headerSize/100, bgWidth*headerSize/100, Image.SCALE_DEFAULT);
        Image qrcodeImg = qrcodeIcon.getImage().getScaledInstance(bgWidth*qrcodeSize/100, bgWidth*qrcodeSize/100, Image.SCALE_DEFAULT);

        headerIcon = new ImageIcon(img);
        qrcodeIcon = new ImageIcon(qrcodeImg);
        img = headerIcon.getImage();
        qrcodeImg = qrcodeIcon.getImage();


        int headerWidth = headerIcon.getIconWidth();
        int headerHeight = headerIcon.getIconHeight();
        int qrcodeWidth = qrcodeIcon.getIconWidth();
        int qrcodeHeight = qrcodeIcon.getIconHeight();

        //将小图片绘到大图片上。
        //5,300 .表示你的小图片在大图片上的位置。
        g.drawImage(img,(bgWidth-headerWidth)/2,bgHeight*posyOfHeader/100,null);
        g.drawImage(qrcodeImg, (bgWidth - qrcodeWidth) / 2, bgHeight * posyOfQrcode / 100, null);

        //设置颜色。
        g.setColor(Color.WHITE);

        //最后一个参数用来设置字体的大小
        Font f = new Font("宋体",Font.BOLD,fontSize);
        g.setFont(f);
        FontMetrics fontMetrics = g.getFontMetrics(f);
        int widthOfNickname = fontMetrics.stringWidth(nickname);
        int widthOfInefficientTime = fontMetrics.stringWidth(inefficientTime);
        nickname = new String(nickname.getBytes());
        inefficientTime = new String(inefficientTime.getBytes());
        g.drawString(nickname, (bgWidth-widthOfNickname)/2, bgHeight*posyOfNickname/100);
        g.drawString(inefficientTime, (bgWidth-widthOfInefficientTime)/2, bgHeight*posyOfTime/100);

        g.dispose();



        //创键编码器，用于编码内存中的图象数据。

        //JPEGImageEncoder en = JPEGCodec.createJPEGEncoder(os);
        //en.encode(buffImg);
        ImageIO.write(buffImg, "jpg", outputStream);

        is.close();
        outputStream.close();

//        FileInputStream fileInputStream = new FileInputStream(fileTemp);
//        byte[] b = new byte[fileInputStream.available()];
//        fileInputStream.read(b);
//        fileInputStream.close();
//
//        BASE64Encoder encoder = new BASE64Encoder();
//        String base64String = encoder.encode(b);
//        return base64String;
    }



    @RequestMapping(value = "/index")
    public String pageIndex(HttpServletRequest request,ChannelType channelType,String channelId){
        logger.debug("用户进入index页面");

        if(ChannelType.DIST_IN_SYSTEM.equals(channelType)){//分销商
            List<ChannelDistributionProductInfoDto> proList = channelService.querySellableProducts(channelId);
            proList = proList.stream()
                    .filter(e->ChannelType.DIST_IN_SYSTEM.equals(e.getChannelType()))
                    .peek(e->{
                        e.setVipPrice(null);
                        e.setPrice(e.getChannelDistPrice());
                    })
                    .collect(Collectors.toList());
            request.setAttribute("products", proList);
        }else {
            List<Product> list = productService.listAll();
            List<Product> onSaleList = list.stream().filter(ele->ele.getOnSale()!=null && ele.getOnSale() == 1).collect(Collectors.toList());
            request.setAttribute("products", onSaleList);
        }
        return "/wechat/index";
    }

    @RequestMapping(value = "/personal")
    public String  personal(Model model,HttpServletRequest request){
        String openid = (String)request.getSession().getAttribute("openid");
        Parameter welcomeTipsParam = parameterService.queryByKey(Parameter.KEY_WELCOME_TIPS);

        long userOfLevel1 = wxuserService.queryCountOfLevel1(openid);
        long userOfLevel2 = wxuserService.queryCountOfLevel2(openid);
        long userOfLevel3 = wxuserService.queryCountOfLevel3(openid);

        Wxuser wxuser = wxuserService.queryUserByOpenid(openid);
        WxMpUser wxMpUser = null;
        try {
            wxMpUser = wxService.userInfo(openid, null);
        } catch (WxErrorException e) {
            logger.error("获取用户据信息失败，openid："+openid,e);
            e.printStackTrace();
        }

        long commissionOfLevel1 = userCommissionService.queryUserCommissionByGrade(openid, UserCommission.GRADE_LEVEL1);
        long commissionOfLevel2 = userCommissionService.queryUserCommissionByGrade(openid, UserCommission.GRADE_LEVEL2);
        long commissionOfLevel3 = userCommissionService.queryUserCommissionByGrade(openid, UserCommission.GRADE_LEVEL3);

        long usableBalance = wxuserService.queryUsableBalance(openid);
        List<Withdraw> withdraws = withdrawService.queryWithdrawByOpenid(openid);


        model.addAttribute("welcomeTips",(welcomeTipsParam == null || welcomeTipsParam.getParamValue() == null) ? " 欢迎" : welcomeTipsParam.getParamValue());
        model.addAttribute("userOfLevel1",userOfLevel1);
        model.addAttribute("userOfLevel2",userOfLevel2);
        model.addAttribute("userOfLevel3",userOfLevel3);
        model.addAttribute("commissionOfLevel1",commissionOfLevel1);
        model.addAttribute("commissionOfLevel2",commissionOfLevel2);
        model.addAttribute("commissionOfLevel3",commissionOfLevel3);
        model.addAttribute("usableBalance",usableBalance);
        model.addAttribute("withdraws",withdraws);
        model.addAttribute("wxuser",wxuser);
        model.addAttribute("wxMpUser",wxMpUser);

        return "/wechat/personal";
    }

    @RequestMapping(value = "/toDist")
    public String  toDist(Model model,HttpServletRequest request){
        String openid = (String)request.getSession().getAttribute("openid");
        Parameter welcomeTipsParam = parameterService.queryByKey(Parameter.KEY_WELCOME_TIPS);

        model.addAttribute("welcomeTips",(welcomeTipsParam == null || welcomeTipsParam.getParamValue() == null) ? " 欢迎" : welcomeTipsParam.getParamValue());
        Wxuser wxuser = wxuserService.queryUserByOpenid(openid);
        WxMpUser wxMpUser = null;
        try {
            wxMpUser = wxService.userInfo(openid, null);
        } catch (WxErrorException e) {
            logger.error("获取用户据信息失败，openid："+openid,e);
            e.printStackTrace();
        }

        model.addAttribute("wxuser",wxuser);
        model.addAttribute("wxMpUser",wxMpUser);

        List<ChannelDistributionProductInfoDto> dists = channelService.queryAvailableProductForUser(openid, ChannelType.DIST_IN_SYSTEM);
        model.addAttribute("dists",dists);
        return "/wechat/dist";
    }

    @RequestMapping(value = "/distProductConfig",method = RequestMethod.POST)
    @ResponseBody
    public Result updateChannelDistProduct(@RequestBody List<DistProductConfigDto> content,HttpServletRequest request){
        String openid = (String)request.getSession().getAttribute("openid");
        if(content != null && content.size() > 0){

            content.stream().filter(e->e.getOnSale() == 1).forEach(e -> {
                ChannelProduct channelProduct = channelService.get(ChannelType.DIST_IN_SYSTEM, e.getProductid());
                if (e.getDistPrice() < channelProduct.getChannelPrice()) {
                    throw new IllegalArgumentException("价格设置异常");
                }
            });

            List<ChannelDistributionProduct> list = content.stream().filter(e -> e.getOnSale() == 1).map(e -> {
                ChannelDistributionProduct p = new ChannelDistributionProduct();
                p.setUserid(openid);
                p.setProductid(e.getProductid());
                p.setChannelDistPrice(e.getDistPrice());
                return p;
            }).collect(Collectors.toList());
            channelService.updateChannelDistProduct(openid, list);
        }
        return ResultBuilder.success();
    }

    @RequestMapping(value = "/withdraw")
    @ResponseBody
    public Result withdraw(HttpServletRequest request,int amount,String account){
        String openid = (String)request.getSession().getAttribute("openid");
        Withdraw withdraw = new Withdraw();
        withdraw.setOpenid(openid);
        withdraw.setAmount(amount);
        int id = withdrawService.add(withdraw);

        //自动提现
        withdrawService.autoWithdraw(id);

        return ResultBuilder.success();
    }


    @RequestMapping(value = "/callback",method = {RequestMethod.GET,RequestMethod.POST})
    public void callback(HttpServletRequest request, HttpServletResponse response)
            throws  IOException {

        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);

        String signature = request.getParameter("signature");
        String nonce = request.getParameter("nonce");
        String timestamp = request.getParameter("timestamp");

        if (!wxService.checkSignature(timestamp, nonce, signature)) {
            // 消息签名不正确，说明不是公众平台发过来的消息
            response.getWriter().println("非法请求");
            return;
        }

        String echostr = request.getParameter("echostr");
        if (StringUtils.isNotBlank(echostr)) {
            // 说明是一个仅仅用来验证的请求，回显echostr
            response.getWriter().println(echostr);
            return;
        }

        String encryptType = StringUtils.isBlank(request.getParameter("encrypt_type")) ?
                "raw" :
                request.getParameter("encrypt_type");

        if ("raw".equals(encryptType)) {
            // 明文传输的消息
            WxMpXmlMessage inMessage = WxMpXmlMessage.fromXml(request.getInputStream());
            WxMpXmlOutMessage outMessage = wxMpMessageRouter.route(inMessage);
            response.getWriter().write(outMessage == null ? "" : outMessage.toXml());
            return;
        }

        if ("aes".equals(encryptType)) {
            // 是aes加密的消息
            String msgSignature = request.getParameter("msg_signature");
            WxMpXmlMessage inMessage = WxMpXmlMessage.fromEncryptedXml(request.getInputStream(), wxConfig, timestamp, nonce, msgSignature);
            WxMpXmlOutMessage outMessage = wxMpMessageRouter.route(inMessage);
            response.getWriter().write(outMessage == null ? "" : outMessage.toEncryptedXml(wxConfig));
            return;
        }

        response.getWriter().println("不可识别的加密类型");
        return;
    }

    /**
     * jsapi的签名
     * @param url
     * @return
     */
    @RequestMapping(value = "/jssign", method = RequestMethod.GET)
    @ResponseBody
    public Map getSign(@RequestParam("url") String url) {
        Map<String,Object> ret = new HashMap<String, Object>();
        WxJsapiSignature jsapiSignature;
        try {
            jsapiSignature = wxService.createJsapiSignature(url);
            ret.put("sign", jsapiSignature);
        } catch (WxErrorException e) {
            logger.error("获取js sign 失败" + e.getMessage(), e);
        }
        ret.put("appId", wxConfig.getAppId());
        return ret;
    }

    @RequestMapping(value = "/authurl")
    @ResponseBody
    public Map getAuthUrl(String url, @RequestParam(value = "state", required = false) String state) {

        Map<String,Object> ret = new HashMap<String, Object>();
        String authUrl = wxService.oauth2buildAuthorizationUrl(url, WxConsts.OAUTH2_SCOPE_BASE, state);
        ret.put("authurl", authUrl);
        return ret;
    }

    private boolean isManager(String openid){
        if(openid != null && managerOpenids != null){
            String[] t = managerOpenids.split(",");
            for(int i = 0; i < t.length; i++){
                if(t[i].equals(openid)){
                    return true;
                }
            }
        }
        return false;
    }

    @RequestMapping(value = "/myorder")
    public String myorder(String orderid,HttpServletRequest request,Model model,HttpServletResponse response){
        response.setCharacterEncoding("UTF-8");
        String openid = (String)request.getSession().getAttribute("openid");
        boolean isManger = isManager(openid);
        model.addAttribute("isManager", isManger);
        return "/wechat/myorderlist";
    }
    @RequestMapping(value = "/channelOrder")
    public String channelOrder(String orderid,HttpServletRequest request,Model model,HttpServletResponse response){
        response.setCharacterEncoding("UTF-8");
        String openid = (String)request.getSession().getAttribute("openid");
        List<Order> orderList = new ArrayList<Order>();
        boolean isManger = isManager(openid);
        model.addAttribute("isManager", isManger);
        return "/wechat/channelorderlist";
    }


}
