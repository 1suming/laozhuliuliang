package com.zhuwenda.marksix.restfulservice;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Fcmsgtemplateservletresult {

    private String id;
    @JsonProperty("templateName")
    private String templatename;
    @JsonProperty("templateContent")
    private String templatecontent;
    @JsonProperty("createTime")
    private String createtime;
    private int type;
    @JsonProperty("userId")
    private String userid;
    private int status;
    @JsonProperty("belongTo")
    private int belongto;
    @JsonProperty("sendTime")
    private int sendtime;


    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }


    public void setTemplatename(String templatename) {
        this.templatename = templatename;
    }
    public String getTemplatename() {
        return templatename;
    }


    public void setTemplatecontent(String templatecontent) {
        this.templatecontent = templatecontent;
    }
    public String getTemplatecontent() {
        return templatecontent;
    }


    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }
    public String getCreatetime() {
        return createtime;
    }


    public void setType(int type) {
        this.type = type;
    }
    public int getType() {
        return type;
    }


    public void setUserid(String userid) {
        this.userid = userid;
    }
    public String getUserid() {
        return userid;
    }


    public void setStatus(int status) {
        this.status = status;
    }
    public int getStatus() {
        return status;
    }


    public void setBelongto(int belongto) {
        this.belongto = belongto;
    }
    public int getBelongto() {
        return belongto;
    }


    public void setSendtime(int sendtime) {
        this.sendtime = sendtime;
    }
    public int getSendtime() {
        return sendtime;
    }

}