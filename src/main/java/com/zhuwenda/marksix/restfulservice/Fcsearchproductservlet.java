package com.zhuwenda.marksix.restfulservice;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class Fcsearchproductservlet {

    @JsonProperty("resultCode")
    private String resultcode;
    @JsonProperty("resultMsg")
    private String resultmsg;
    private List<PlatformProduct> information;


    public void setResultcode(String resultcode) {
        this.resultcode = resultcode;
    }
    public String getResultcode() {
        return resultcode;
    }


    public void setResultmsg(String resultmsg) {
        this.resultmsg = resultmsg;
    }
    public String getResultmsg() {
        return resultmsg;
    }

    public List<PlatformProduct> getInformation() {
        return information;
    }

    public void setInformation(List<PlatformProduct> information) {
        this.information = information;
    }
}