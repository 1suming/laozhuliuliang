package com.zhuwenda.marksix.restfulservice;
import com.fasterxml.jackson.annotation.JsonProperty;


public class PlatformProduct {

    private String id;
    private String name;
    @JsonProperty("totalSize")
    private int totalsize;
    @JsonProperty("yysType")
    private int yystype;
    @JsonProperty("flowType")
    private int flowtype;


    /** 售价，单位：分 */
    private int price;
    private Integer vipPrice;
    private Integer onSale;

    public Integer getVipPrice() {
        return vipPrice;
    }

    public void setVipPrice(Integer vipPrice) {
        this.vipPrice = vipPrice;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }


    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }


    public void setTotalsize(int totalsize) {
        this.totalsize = totalsize;
    }
    public int getTotalsize() {
        return totalsize;
    }


    public void setYystype(int yystype) {
        this.yystype = yystype;
    }
    public int getYystype() {
        return yystype;
    }


    public void setFlowtype(int flowtype) {
        this.flowtype = flowtype;
    }
    public int getFlowtype() {
        return flowtype;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Integer getOnSale() {
        return onSale;
    }

    public void setOnSale(Integer onSale) {
        this.onSale = onSale;
    }
}