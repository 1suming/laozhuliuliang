<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
    <title>我的分销订单</title>

    <jsp:include page="common/header.jsp"/>
    <script type="text/template" id="orderListTemplate">
        {@each ps.list as item}
        <li data-role="list-divider" data-orderid="#{item.orderid}" data-topupMobile="#{item.topupMobile}">订单号：#{item.orderid}</li>
        <li data-orderid="#{item.orderid}" data-topupMobile="#{item.topupMobile}">
            <table>
                <tr>
                    <td>充值号码</td>
                    <td>#{item.topupMobile}</td>
                </tr>
                <tr>
                    <td>下单时间</td>
                    <td>#{item.createTime}</td>
                </tr>
                <tr>
                    <td>购买产品</td>
                    <td>#{item.productName}</td>
                </tr>
                <tr>
                    <td>支付状态</td>
                    <td>
                        {@if item.payStatus === 0 }
                        未支付
                        {@else if item.payStatus === 1 }
                        <span class="text-success">已支付</span>
                        {@else if item.payStatus === 3 }
                        退款中
                        {@else if item.payStatus === 5 }
                        <span class="text-success">退款成功</span>
                        {@else if item.payStatus === 7 }
                        <span class="text-error">退款失败</span>
                        {@else}
                        <span class="text-danger">未支付</span>
                        {@/if}
                    </td>
                </tr>
                <tr>
                    <td>充值状态</td>
                    <td>
                        {@if item.topupStatus === 1 }
                        待充值
                        {@else if item.topupStatus === 3 }
                        充值中
                        {@else if item.topupStatus === 5 }
                        <span class="text-success">已充值</span>
                        {@else if item.topupStatus === 7 }
                        充值失败
                        {@else}
                        未知
                        {@/if}
                    </td>
                </tr>
                {@if isManager }
                <tr>
                    <td>支付单号</td>
                    <td>#{item.payid}</td>
                </tr>
                <tr>
                    <td>外部系统单号</td>
                    <td>#{item.topupOrderid}</td>
                </tr>
                <tr>
                    <td>外部系统消息</td>
                    <td>#{item.outerMessage}</td>
                </tr>
                {@/if}
            </table>
        </li>
        <!-- Table -->
        {@/each}
    </script>
    <script>

    $(document).ready(function(){
        $.wx.regiseter();

        $("#submit").click(function(){
            var tel = $("#tel").val();
            var num = $("#num").val();
            if(tel && tel.length == 11){
                phoneInfo(tel,function(data){
                    console.log(data);
                    if(data.catName == "中国移动" && data.province == "广东"){
                        wxPay();
                    }else{
                        alert("请输入广东移动手机号");
                        $("#tel").focus();
                    }
                });
            }
        });
    });

    function ajaxOrderlist(param,success){
        $.ajax({
            "url":"/api/channelOrderlist",
            "data":param,
            "success":success
        });
    }
    function orderListViewMoreCb(ret){
        showData(ret);
    }
    function showData(ret,type){
        if(ret.errcode == 0){
            if(ret.data.ps.pager.hasNext){
                $("#viewMore").show();
            }else{
                $("#viewMore").hide();
            }
            if(type == "search"){
                $("#listContainer").empty().listview("refresh");
            }

            for(var i = 0; i < ret.data.ps.list.length; i++){
                var cur = ret.data.ps.list[i];
                var time = cur.createTime;
                cur.createTime = dateFormat(time,"yyyy-MM-dd hh:mm:ss");
            }


            var html = juicer("#orderListTemplate",ret.data);
            $("#listContainer").append(html).listview("refresh");
        }else{
            alert(ret.errmsg)
        }
    }
    function orderListSearchCb(ret){
        showData(ret,"search");
    }
    $(document).ready(function(){
        $("form").submit(function(e){
            e.preventDefault();
        });
        $("#viewMore").click(function(){
            var keyword = $("#searchInput").val();
            var page = $("#listContainer").attr("d-page");
            page = page || 0;
            if(typeof(page) == "string"){
                page = parseInt(page);
            }
            page++;
            $("#listContainer").attr("d-page",page);
            var param = {};
            param.keyword = keyword;
            param.page = page;
            param.pageSize = 10;

            ajaxOrderlist(param,orderListViewMoreCb);
        }).click();
        $("#searchBtn").click(function(){
            var keyword = $("#searchInput").val();

            $("#listContainer").attr("d-page","1");
            ajaxOrderlist({"page":"1","pageSize":"10","keyword":keyword},orderListSearchCb);
        });
    });

    $.mobile.document.one( "filterablecreate", "#listContainer", function() {
        $( "#listContainer" ).filterable( "option", "filterCallback", function( index, searchValue ) {
            var orderid = $(this).attr("data-orderid");
            var topupMobile = $(this).attr("data-topupMobile");
            if(orderid.indexOf(searchValue) > -1 || topupMobile.indexOf(searchValue) > -1){
                return false;
            }
            return true;
        });
    });
    wx.ready(function(){
    });
    </script>
</head>

<body>

<div data-role="page" id="orderListPage">

    <div role="main" class="ui-content">
        <form>
            <input id="searchInput" placeholder="请输入订单号或充值号码查询" type="tel" data-type="search">
            <button id="searchBtn">搜索</button>
        </form>
        <ul id="listContainer" data-role="listview" data-inset="true" >
        </ul>
        <button id="viewMore">显示更多</button>
    </div><!-- /content -->


</div><!-- /page -->

</body>
</html>
