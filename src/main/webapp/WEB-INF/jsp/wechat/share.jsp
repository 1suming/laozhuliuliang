﻿<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
    <title>${shareTips}</title>

    <jsp:include page="common/header.jsp"/>
    <style>
        #sharePage #expireShare{
            position: absolute;
            width: 3em;
            height: 3em;
            border-radius: 1.5em;
            left: 0.5em;
            bottom: 0.5em;
            border: 1px solid #666;
            background-color: white;
            color: blue;
            text-align: center;
            line-height: 3em;
        }
    </style>
    <script>

        $(document).ready(function(){
            $("#sharePage #expireShare").click(function(){
                $.ajax({
                    "url":"/expireShare?shareCode=${param.shareCode}",
                    "type":"post",
                    "success":function(rep){
                        if(rep.errcode == 0){
                            location.reload();
                        }else{
                            alert(rep.errmsg);
                        }
                    },
                    "beforeSend": function () {
                        $.mobile.loading("show");
                        isRequesting = true;
                    },
                    "complete": function () {
                        $.mobile.loading("hide");
                        isRequesting = false;
                    }
                })
            });
        });
    </script>
</head>

<body>


<div data-role="page" id="sharePage">
    <div role="main" class="ui-content" style="padding:0;">
        <!--
        <div><img width="100%" style="display:block;" src="data:image/gif;base64,"/></div>
        -->
        <div><img width="100%" style="display:block;" src="${imgPath}"/></div>
    </div><!-- /content -->
</div>

</body>
</html>
