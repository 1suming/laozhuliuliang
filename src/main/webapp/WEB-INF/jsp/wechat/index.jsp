﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
    <title>流量充值</title>

    <jsp:include page="common/header.jsp"/>

    <style>
        .hidden{
            display:none;
        }
        .custom-corners .ui-bar {
              -webkit-border-top-left-radius: inherit;
              border-top-left-radius: inherit;
              -webkit-border-top-right-radius: inherit;
              border-top-right-radius: inherit;
        }
        .custom-corners .ui-body {
              border-top-width: 0;
              -webkit-border-bottom-left-radius: inherit;
              border-bottom-left-radius: inherit;
              -webkit-border-bottom-right-radius: inherit;
              border-bottom-right-radius: inherit;
        }
    </style>
    <script>
        var isRequesting = false;

        function phoneInfo(tel,callback){

            if(!isRequesting) {
                $.ajax({
                    "url": "https://tcc.taobao.com/cc/json/mobile_tel_segment.htm?tel=" + tel,
                    "dataType": "jsonp",
                    "success": callback,
                    "beforeSend": function () {
                        $.mobile.loading("show");
                        isRequesting = true;
                    },
                    "complete": function () {
                        $.mobile.loading("hide");
                        isRequesting = false;
                    }
                });
            }
        }
        $(document).ready(function(){
            $.wx.regiseter();
            $("form").submit(function(){return false;});
            $("#submit").click(function(){
                var tel = $("#tel").val();

                if(!(tel && tel.length == 11)){

                    alert("请输入正确的手机号码");
                    $("#tel").focus();
                    return false;
                }

                var $yysTpype = $("input[name='productid']:checked");
                if($yysTpype.length == 0 || $yysTpype.val() == ""){
                    alert("请选择流量包");
                    return false;
                }
                if($("input[name='productid']:checked").closest(".ui-radio").is(":hidden")){
                    alert("请选择流量包!");
                    return false;
                }else{
                    createOrder();
                }


                return false;
            });

            $("label[data-yystype='1']").show();


            wx.ready(function(){

                var link = $.url.deleteUrlParameter("code");
                link = $.url.deleteUrlParameter("state",link)

                //--- 自定义分享给朋友和朋友圈链接 -----
                wx.onMenuShareTimeline({
                    "link": link
                });
                wx.onMenuShareAppMessage({
                    "link": link
                });
                //---
            });

        });

        $(document).on("pageinit",function(){
            hideAllProductList();

            $("#tel").on("input",function(e){
                var phone = $(e.target).val();
                if(/\d{7,11}/.test(phone)){
                    while(phone.length < 11){
                        phone += "0";
                    }
                    phoneInfo(phone,function(data){
                        var carrier = 0;
                        if(data.catName == "中国移动"){
                            carrier = 1;
                        }else if(data.catName == "中国联通"){
                            carrier = 2;
                        }else if(data.catName == "中国电信"){
                            carrier = 3;
                        }else{
                            alert("号码归属的运营商数据异常！");
                            return false;
                        }
                        updateProductList(carrier,data.province);
                    });
                }else{
                    hideAllProductList();
                }
            });
        });

        /**
        *
        * @param yystype 1移动，2联通，3电信
         * @param isGuangdong 是否是广东
         */
        function changeYys(yystype,isGuangdong){
            $("#ctlGroup .ui-radio").hide();
            if(yystype == 1 ){
                var $guangdong = $("label[data-isGuangdong='yes']");
                if(isGuangdong && $guangdong.length > 0){
                    $guangdong.closest(".ui-radio").show();
                }else{
                    var $yidong = $("label[data-yystype='1']:not(*[data-isGuangdong='yes'])");
                    $yidong.closest(".ui-radio").show();
                }
            }else{
                $("label[data-yystype='"+yystype+"']").closest(".ui-radio").show();
            }

        }

        /**
         * 隐藏所有产品
         */
        function hideAllProductList(){
            $("#ctlGroup .ui-radio").hide();
        }

        /**
        * 更新产品列表
        * @param carrier 运营商
        * @param province 省份
         */
        function updateProductList(carrier,province){
            hideAllProductList();
            carrier = ""+carrier;
            if(carrier != "1" && carrier != "2" && carrier != "3"){
                alert("运营商产品异常!");
                return false;
            }
            if($("label[data-carrier='"+carrier+"'][data-province='"+province+"']").length > 0){//如果运营商和省份双重匹配
                $("label[data-carrier='"+carrier+"'][data-province='"+province+"']").closest(".ui-radio").show();
            }else{
                if($("label[data-carrier='"+carrier+"'][data-province='']").length > 0){////运营商匹配
                    $("label[data-carrier='"+carrier+"'][data-province='']").closest(".ui-radio").show();
                }else{
                    //显示出所有运营商所有省份都适用的产品
                    $("label[data-carrier=''][data-province='']").closest(".ui-radio").show();
                }
            }
        }


    </script>
</head>
<body>

<div data-role="page">

    <div data-role="header" data-position="fixed" >

        <div data-role="navbar">
            <ul>
                <li><a class="ui-btn-active">流量充值${isVip ? "（VIP用户）" : ""}</a></li>
                <li><a href="myorder" data-ajax="false">我的订单</a></li>
            </ul>
        </div><!-- /navbar -->
    </div>
    <div role="main" class="ui-content">

        <form>
            <input type="hidden" name="sourceOpenid" value="${param.sourceOpenid}"/>
            <input type="hidden" name="channelType" value="${param.channelType}"/>
            <input type="hidden" name="channelId" value="${param.channelId}"/>
            <input type="tel" maxlength="11" id="tel" name="phone" placeholder="请输入充值号码">
            <div id="ctlGroup" data-role="controlgroup" data-type="vertical">
                <c:forEach items="${products}" var="item" varStatus="s">
                    <label class="" data-productid="${item.productid}" data-carrier="${item.carrier}" data-province="${item.province}" ><input type="radio" name="productid" value="${item.productid}" data-yystype="${item.carrier}"/>￥${(isVip && item.vipPrice != null && item.vipPrice > 0) ? item.vipPrice/100 : item.price/100} - ${item.name}</label>
                </c:forEach>
            </div>
            <button id="submit">提交</button>
        </form>
        <div class="ui-corner-all custom-corners">
            <div class="ui-bar ui-bar-a">
                <h3>温馨提醒：</h3>
            </div>
            <div class="ui-body ui-body-a">
				<ul>
				   <li>流量价格：输入要充值的手机号即可查看</li>
				   <li>支持用户：联通3G/4G用户，移动电信2G/3G/4G用户</li>
				   <li>生效时间：一般即时到账，运营商繁忙时24小时内到账</li>
			       <li>有效时间：全国流量包为当月有效，广东移动省内包为20天有效</li>
		           <li>使用范围：全国流量包可全国漫游，省内包只能在省内使用</li>
			   </ul>   
			</div>
        </div>
    </div><!-- /content -->
</div><!-- /page -->

</body>
</html>
