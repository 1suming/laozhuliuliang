<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="myfn" uri="http://liuliang.zhuwenda.com"%>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
    <title>下线会员</title>

    <jsp:include page="common/header.jsp"/>

    <style>
    </style>

    <script>
    $(document).ready(function(){

    });
    $(document).on("pageinit","#myuserlistPage",function(){

        var curPage = 0;
        var hasMore = true;
        $("#viewMore").click(function(){
            if(hasMore){
                var param = {
                    "page":++curPage,
                    "pageSize":10,
                    "level":${param.level}
                };
                $.ajax({
                    "url":"/myuserlist",
                    "data":param,
                    "success":function(response){
                        if(response.errcode == 0){
                            if(response.data.list.length > 0){
                                var list = response.data.list;
                                var html = "";
                                for(var i = 0; i < list.length; i++){
                                    var cur = list[i];
                                    html += ($("#template").html()
                                            .replace(/!\{time\}/g,cur.timestr ? cur.timestr : "无记录")
                                            .replace(/!\{nickname\}/g,cur.wxuser.nickname)
                                            .replace(/!\{amount\}/g,cur.sum/100));

                                }
                                console.log(html);
                                $("#myuserlistPage #userList").append(html).listview("refresh");
                            }
                            if(!response.data.pager.hasNext){
                                $("#viewMore").remove();
                                hasMore = false;
                            }
                        }else{
                            alert(response.errmsg);
                        }
                    },
                    "beforeSend": function () {
                        $.mobile.loading("show");
                        window.isRequesting = true;
                    },
                    "complete": function () {
                        $.mobile.loading("hide");
                        window.isRequesting = false;
                    }
                });
            }
        }).click();
    });
    </script>
</head>

<body>

<div data-role="page" id="myuserlistPage">
    <div data-role="header" data-position="fixed">
        <h2>下线会员</h2>
    </div>
    <div role="main" class="ui-content">

        <ul id="userList" data-role="listview" data-inset="true" >

        </ul>
        <button id="viewMore">显示更多</button>
    </div><!-- /content -->
    <div id="template" class="hidden">
        <li data-role="list-divider">关注时间：!{time}</li>
        <li>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 50%;">!{nickname}</td>
                    <td style="text-align:right;">!{amount}元</td>
                </tr>
            </table>
        </li>
    </div>
</div><!-- /page -->

</body>
</html>
