<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>


    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="http://cdn.bootcss.com/jquery-mobile/1.4.5/jquery.mobile.min.css" rel="stylesheet">
    <link href="/static/css/common.css" rel="stylesheet">

    <style>
        .text-success{
            color:green;
        }


    </style>

<style>
    .hidden{
        display:none;
    }
	
    .custom-corners .ui-bar {
          -webkit-border-top-left-radius: inherit;
          border-top-left-radius: inherit;
          -webkit-border-top-right-radius: inherit;
          border-top-right-radius: inherit;
    }
    .custom-corners .ui-body {
          border-top-width: 0;
          -webkit-border-bottom-left-radius: inherit;
          border-bottom-left-radius: inherit;
          -webkit-border-bottom-right-radius: inherit;
          border-bottom-right-radius: inherit;
		font-size:12px;

    }

    .userInfo {
        height: 60px;
    }
    .userInfo>div{
        height: 100%;
    }
    .userInfo .headerImg{
        border-radius: 30px;
        height: 100%;
    }
    .userInfo p {
        margin: 0.325em 0 0.325em 1em;
    }



</style>

    <script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/jquery-mobile/1.4.5/jquery.mobile.min.js"></script>

    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script src="/static/js/plugin/plugin-wx.js"></script>
    <script src="/static/js/plugin/plugin-util.js"></script>
<script src="/static/js/plugin/juicer.js"></script>
<script src="/static/js/plugin/dataformat.js"></script>
<script src="/static/js/common.js"></script>

    <script>
        function createOrder(){
            $.ajax({
                "url":"/pay/wx/order",
                "data":$("form").serialize(),
                "method":"POST",
                "dataType":"json",
                "success":function(data){
                    console.log(data);
                    if(data.errcode == 0){
                        if($ && $.mobile){
                            $.mobile.loading("hide");

                        }
                        var payInfo = data.payInfo;
                        wxPay(payInfo,function (res) {
                            alert("恭喜，支付成功！");
                            location.href = "myorder";
                        });
                    }else{
                        var msg = data.errmsg ? data.errmsg : "";
                        alert("创建订单失败"+msg);
                    }
                },
                "beforeSend": function () {
                    $.mobile.loading("show");
                },
                "complete": function () {
                    $.mobile.loading("hide");
                }
            });
        }

        function wxPay(payInfo,success){
            success = success || function(){};
            wx.chooseWXPay({
                timestamp: payInfo.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
                nonceStr: payInfo.nonceStr, // 支付签名随机串，不长于 32 位
                package: payInfo.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
                signType: payInfo.signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
                paySign: payInfo.paySign, // 支付签名
                success: success
            });
        }

    </script>
