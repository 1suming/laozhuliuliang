<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
    <title>我的订单</title>

    <jsp:include page="common/header.jsp"/>
    <script>
    function phoneInfo(tel,callback){
        $.ajax({
            "url":"https://tcc.taobao.com/cc/json/mobile_tel_segment.htm?tel="+phone,
            "dataType":"jsonp",
            "success":callback
        });
    }
    $(document).ready(function(){
        $.wx.regiseter();

        $("#submit").click(function(){
            var tel = $("#tel").val();
            var num = $("#num").val();
            if(tel && tel.length == 11){
                phoneInfo(tel,function(data){
                    console.log(data);
                    if(data.catName == "中国移动" && data.province == "广东"){
                        wxPay();
                    }else{
                        alert("请输入广东移动手机号");
                        $("#tel").focus();
                    }
                });
            }
        });
    });
    $.mobile.document.one( "filterablecreate", "#listContainer", function() {
        $( "#listContainer" ).filterable( "option", "filterCallback", function( index, searchValue ) {
            var orderid = $(this).attr("data-orderid");
            var topupMobile = $(this).attr("data-topupMobile");
            if(orderid.indexOf(searchValue) > -1 || topupMobile.indexOf(searchValue) > -1){
                return false;
            }
            return true;
        });
    });
    wx.ready(function(){
    });
    </script>
</head>

<body>

<div data-role="page">
    <div data-role="header" data-position="fixed">

        <div data-role="navbar">
            <ul>
                <li><a href="index" data-ajax="false">流量充值</a></li>
                <li><a class="ui-btn-active">我的订单${isVip ? "（VIP用户）" : ""}</a></li>
            </ul>
        </div><!-- /navbar -->
    </div>
    <div role="main" class="ui-content">
        <form>
            <input id="filter-input" placeholder="请输入订单号或充值号码查询" type="tel" data-type="search">
        </form>
        <ul id="listContainer" data-role="listview" data-inset="true" data-filter="true" data-input="#filter-input">
            <c:forEach items="${orderList}" var="item" varStatus="s">
                <li data-role="list-divider" data-orderid="${item.orderid}" data-topupMobile="${item.topupMobile}">订单号：${item.orderid}</li>
                <li data-orderid="${item.orderid}" data-topupMobile="${item.topupMobile}">
                    <table>
                        <tr>
                            <td>充值号码</td>
                            <td>${item.topupMobile}</td>
                        </tr>
                        <tr>
                            <td>下单时间</td>
                            <td><fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                        </tr>
                        <tr>
                            <td>购买产品</td>
                            <td>${item.productid}</td>
                        </tr>
                        <tr>
                            <td>支付状态</td>
                            <td>
                                <c:choose>
                                    <c:when test="${item.payStatus == 0}">
                                        未支付
                                    </c:when>
                                    <c:when test="${item.payStatus == 1}">
                                        <span class="text-success">已支付</span>
                                    </c:when>
                                    <c:when test="${item.payStatus == 3}">
                                        退款中
                                    </c:when>
                                    <c:when test="${item.payStatus == 5}">
                                        <span class="text-success">退款成功</span>
                                    </c:when>
                                    <c:when test="${item.payStatus == 7}">
                                        <span class="text-error">退款失败</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span class="text-danger">未知</span>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <td>充值状态</td>
                            <td>
                                <c:choose>
                                    <c:when test="${item.topupStatus == 1}">
                                        待充值
                                    </c:when>
                                    <c:when test="${item.topupStatus == 3}">
                                        充值中
                                    </c:when>
                                    <c:when test="${item.topupStatus == 5}">
                                        <span class="text-success">已充值</span>
                                    </c:when>
                                    <c:when test="${item.topupStatus == 7}">
                                        充值失败
                                    </c:when>
                                    <c:otherwise>
                                        未知
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <c:if test="${isManager}">

                            <tr>
                                <td>支付单号</td>
                                <td>${item.payid}</td>
                            </tr>
                            <tr>
                                <td>外部系统单号</td>
                                <td>${item.topupOrderid}</td>
                            </tr>
                            <tr>
                                <td>外部系统消息</td>
                                <td>${item.outerMessage}</td>
                            </tr>
                        </c:if>
                    </table>
                </li>
                <!-- Table -->


            </c:forEach>

        </ul>
    </div><!-- /content -->


</div><!-- /page -->

</body>
</html>
