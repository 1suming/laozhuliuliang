<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="myfn" uri="http://liuliang.zhuwenda.com"%>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
    <title>个人中心</title>

    <jsp:include page="common/header.jsp"/>


    <script>
    $(document).ready(function(){
        $.wx.regiseter();
        $("#toWithdraw").click(function(){
            var $amountInput = $("#amountInput");
            var amount = $amountInput.val();
            var usable = parseFloat($amountInput.attr("data-usable"));

            var amountFloat = 10000;
            try{
                amountFloat = parseFloat(amount);
            }catch(e){
                alert("请输入正确的提现金额");
                return false;
            }
            if(amount == null || amount == ""){
                alert("提现金额不能为空");
                return false;
            }
			/*
			if(account == null || account==""){
                alert("账号不能为空");
                return false;
            }
			*/
            if(amountFloat*100 > usable){
                alert("当前最大可提现金额为："+(usable/100)+"元");
                return false;
            }
            if(amountFloat <= 0){
                alert("提现金额必须大于0");
                return false;
            }
            if(confirm("确定要提现"+(amountFloat)+"元吗？")){
                $.ajax({
                    "url":"/withdraw",
                    "data":{"amount":parseInt(amountFloat*100)},
                    "type":"post",
                    "success":function(response){
                        if(response.errcode == 0){
                            alert("提现申请成功,将于24小时内到账您的微信“钱包”_“零钱”");
                            window.location.reload();
                        }else{
                            alert(response.errmsg);
                            window.location.reload();
                        }
                    },
                    "beforeSend": function () {
                        $.mobile.loading("show");
                        isRequesting = true;
                    },
                    "complete": function () {
                        $.mobile.loading("hide");
                        isRequesting = false;
                    }
                });
            }
        });
        $("#toIntroduce").click(function(){
            $.ajax({
                "url":"/shareCode",
                "success":function(response){
                    try{
                        if(response.errcode == 0){
                            window.location.href = "/share?shareCode="+response.data;
                        }else{
                            alert(response.errmsg);
                        }
                    }catch(e){
                        alert(JSON.stringify(e));
                    }
                },
                "beforeSend": function () {
                    $.mobile.loading("show");
                    isRequesting = true;
                },
                "complete": function () {
                    $.mobile.loading("hide");
                    isRequesting = false;
                }
            });
        });
        $.ajax({
            "url": "/myinfo",
            "success": function (response) {
                if (response.errcode == 0) {
                    var user = response.data;
                    $("#myname").text(user.nickname);
                } else {
                    alert("获取用户昵称错误："+response.errmsg);
                }
            }
        });
    });
    wx.ready(function(){
        //wx.hideOptionMenu();
    });
    </script>
</head>

<body>

<div data-role="page" id="personalPage">
    <div data-role="header" data-position="fixed">
        <h2>个人中心</h2>
    </div>
    <div role="main" class="ui-content">
        <div class="userInfo clearfix">
            <div class="float-left"><img class="headerImg" src="${fn:substring(wxMpUser.headImgUrl, 0, fn:length(wxMpUser.headImgUrl) - 2)}/64"/></div>
            <div class="float-left">
                <p id="myname"></p>
                <p>${welcomeTips}</p>
            </div>
        </div>

        <p><a data-role="button" href="/myorder" data-ajax="false">我的订单</a></p>
        <p><button id="toIntroduce">我要推广</button></p>

        <c:if test="${isManager}">
            <!-- 内测 -->
            <p><a  data-role="button" href="/toDist" data-ajax="false">我要分销</a></p>
        </c:if>

        <ul data-role="listview" data-inset="true">
            <li data-role="list-divider">我的会员：${userOfLevel1+userOfLevel2+userOfLevel3}人，会员提成：${(commissionOfLevel1+commissionOfLevel2+commissionOfLevel3)/100}元</li>
            <li>
                <table>
                    <tr>
                        <td>一级会员</td>
                        <td>
                            <c:choose>
                                <c:when test="${userOfLevel1>0}">
								<a data-ajax="false" href="/toMyuserlist?level=1">${userOfLevel1}</a>人
								</c:when>
                                <c:otherwise>${userOfLevel1}人</c:otherwise>
                            </c:choose>
                        </td>
                        <td>一级提成</td>
                        <td>${commissionOfLevel1/100}元</td>
                    </tr>

                    <tr>
                        <td>二级会员</td>
                        <td>
                            <c:choose>
                                <c:when test="${userOfLevel2>0}">
								<a data-ajax="false" href="/toMyuserlist?level=2">${userOfLevel2}</a>人
								</c:when>
                                <c:otherwise>${userOfLevel2}人</c:otherwise>
                            </c:choose>
                        </td>
                        <td>二级提成</td>
                        <td>${commissionOfLevel2/100}元</td>
                    </tr>

                    <tr>
                        <td>三级会员</td>
                        <td>
                            <c:choose>
                                <c:when test="${userOfLevel3>0}">
								<a data-ajax="false" href="/toMyuserlist?level=3">${userOfLevel3}</a>人
								</c:when>
                                <c:otherwise>${userOfLevel3}人</c:otherwise>
                            </c:choose>
                        </td>
                        <td>三级提成</td>
                        <td>${commissionOfLevel3/100}元</td>
                    </tr>
                </table>
            </li>
            <!-- Table -->

        </ul>

        <p>提成余额：${usableBalance/100}元</p>
        <p><input type="tel" id="amountInput" data-usable="${usableBalance}" placeholder="请输入提现金额（单位：元）"/>
            <button id="toWithdraw">点击提现</button>
        </p>
        <div data-role="collapsible" id="tixianCollapsible">
            <h4>提现记录</h4>
            <ul data-role="listview" data-inset="true" data-filter="true" data-input="#filter-input">
                <c:forEach items="${withdraws}" var="item" varStatus="s">
                    <li data-role="list-divider">提现时间：<fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/> </li>

                    <li>
                        <table style="width: 100%;">
                           
							<tr>
                 
					            <td><u>提现金额:</u></td>
                                <td><u>${item.amount/100}元</u></td>
								
                            </tr>
							<tr>
                                <td><u>到账时间:</u></td>
                                <td><u>24小时内</u></td>
                            </tr>
                            <tr>
                                <td><u>提现状态:</u></td>
                                <td><u>${myfn:withdrawStatus(item.status)}</u></td>
                            </tr>


                        </table>
                    </li>
                    <!-- Table -->



                </c:forEach>
            </ul>
        </div>

    </div><!-- /content -->


</div><!-- /page -->

</body>
</html>
