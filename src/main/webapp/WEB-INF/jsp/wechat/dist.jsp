<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="myfn" uri="http://liuliang.zhuwenda.com"%>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
    <title>分销产品管理</title>

    <jsp:include page="common/header.jsp"/>


    <script>
    $(document).ready(function(){
        $.wx.regiseter();


        $.ajax({
            "url": "/myinfo",
            "success": function (response) {
                if (response.errcode == 0) {
                    var user = response.data;
                    $("#myname").text(user.nickname);
                } else {
                    alert("获取用户昵称错误："+response.errmsg);
                }
            }
        });

        $("#distPage #saveDist").click(function(){
            var $tables = $("#listContainer table");
            if($tables.length > 0){
                var dists = [];
                for(var i = 0; i < $tables.length; i++){
                    var $cur = $tables.eq(i);
                    var distPrice = $cur.find("input[name='distPrice']").val();
                    if(distPrice == ""){
                        distPrice = "0";
                    }
                    distPrice = parseFloat(distPrice);
                    distPrice = Math.round(distPrice * 100);
                    var channelPrice = parseInt($cur.attr("d-channelPrice"));
                    var onSale = $cur.find("select[name='onSale']").val();
                    if(onSale == "1" && channelPrice > distPrice){
                        alert("分销价格不能低于最低价格");
                        return false;
                    }
                    var dist = {
                        "productid":$cur.attr("d-productid"),
                        "onSale":onSale,
                        "distPrice":distPrice
                    };
                    dists.push(dist);
                }
                console.log(dists);
                var postContent = JSON.stringify(dists);
                $.ajax({
                    "url":"/distProductConfig",
                    "type":"POST",
                    "contentType":"application/json",
                    "dataType":"json",
                    "data": postContent,
                    "success":function(ret){
                        if(ret.errcode == 0){
                            console.log(ret.data);
                            alert("保存成功，点击确定重新加载");
                            window.location.reload();
                        }else{
                            alert(ret.errmsg);
                        }
                    }
                });
            }

        });
    });
    wx.ready(function(){
        //wx.hideOptionMenu();
    });
    </script>
</head>

<body>

<div data-role="page" id="distPage">
    <div data-role="header" data-position="fixed">
        <h2>分销产品管理</h2>
    </div>
    <div role="main" class="ui-content">
        <div class="userInfo clearfix">
            <div class="float-left"><img class="headerImg" src="${fn:substring(wxMpUser.headImgUrl, 0, fn:length(wxMpUser.headImgUrl) - 2)}/64"/></div>
            <div class="float-left">
                <p id="myname"></p>
                <p>${welcomeTips}</p>
            </div>
        </div>
        <a data-ajax="false" data-role="button" href="/channelOrder">查看分销订单</a>

        <ul id="listContainer" data-role="listview" data-inset="true" data-filter="true" data-input="#filter-input">
            <c:forEach items="${dists}" var="item" varStatus="s">
                <li data-role="list-divider">${item.name}</li>
                <li>
                    <table d-channelPrice="${item.channelPrice}" d-productid="${item.productid}">
                        <tr>
                            <td>是否上架</td>
                            <td>
                                <select name="onSale" data-role="flipswitch" data-mini="true">
                                    <option value="0">下架</option>
                                    <option value="1" ${item.channelDistPrice > 0 ? 'selected' : ''}>上架</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>最低价格</td>
                            <td>
                                ${item.channelPrice/100}元
                            </td>
                        </tr>
                        <tr>
                            <td>分销价格</td>
                            <td>
                                <input data-mini="true" type="number" name="distPrice" pattern="[0-9]*" value="${item.channelDistPrice/100}" placeholder="单位：元">
                            </td>
                        </tr>
                    </table>
                </li>
            </c:forEach>
        </ul>


        <button id="saveDist">保存</button>
        <a data-ajax="false" data-role="button" href="/index?channelType=DIST_IN_SYSTEM&channelId=${wxMpUser.openId}">去分享</a>
        <div>去分享：点击“去分享”后，可点击微信右上角按钮分享或“复制链接”。</div>

    </div><!-- /content -->


</div><!-- /page -->

</body>
</html>
