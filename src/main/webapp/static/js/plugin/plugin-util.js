;(function($){


    $.url = {
        "getUrlParameter": function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURIComponent(r[2]);
            }
            return null;
        },
        //更改参数值
        "changeUrlParameter": function(name, newValue,url)
        {
            var str = "";
            if($.string.isEmpty(url)){
                url = location.href;
            }
            newValue = encodeURIComponent(newValue);
            if (url.indexOf('?') != -1)
                str = url.substr(url.indexOf('?') + 1);
            else
                return url + "?" + name + "=" + newValue;
            var returnurl = "";
            var setparam = "";
            var arr;
            var modify = "0";
            if (str.indexOf('&') != -1) {
                arr = str.split('&');
                for (i in arr) {
                    if (arr[i].split('=')[0] == name) {
                        setparam = newValue;
                        modify = "1";
                    }
                    else {
                        setparam = arr[i].split('=')[1];
                    }
                    returnurl = returnurl + arr[i].split('=')[0] + "=" + setparam + "&";
                }
                returnurl = returnurl.substr(0, returnurl.length - 1);
                if (modify == "0")
                    if (returnurl == str)
                        returnurl = returnurl + "&" + name + "=" + newValue;
            }
            else {
                if (str.indexOf('=') != -1) {
                    arr = str.split('=');
                    if (arr[0] == name) {
                        setparam = newValue;
                        modify = "1";
                    }
                    else {
                        setparam = arr[1];
                    }
                    returnurl = arr[0] + "=" + setparam;
                    if (modify == "0")
                        if (returnurl == str)
                            returnurl = returnurl + "&" + name + "=" + newValue;
                }
                else
                    returnurl = name + "=" + newValue;
            }
            return url.substr(0, url.indexOf('?')) + "?" + returnurl;
        },
        //删除参数值
        "deleteUrlParameter":
            function (name,url) {
                if($.string.isEmpty(url)){
                    url = location.href;
                }
                var str = "";
                if (url.indexOf('?') != -1) {
                    str = url.substr(url.indexOf('?') + 1);
                }
                else {
                    return url;
                }
                var arr = "";
                var returnurl = "";
                var setparam = "";
                if (str.indexOf('&') != -1) {
                    arr = str.split('&');
                    for (i in arr) {
                        if (arr[i].split('=')[0] != name) {
                            returnurl = returnurl + arr[i].split('=')[0] + "=" + arr[i].split('=')[1] + "&";
                        }
                    }
                    return url.substr(0, url.indexOf('?')) + "?" + returnurl.substr(0, returnurl.length - 1);
                }
                else {
                    arr = str.split('=');
                    if (arr[0] == name) {
                        return url.substr(0, url.indexOf('?'));
                    }
                    else {
                        return url;
                    }
                }
            }
    };

    $.string = {
        "isEmpty": function(str){
            if(str == undefined || str == null || str == ""){
                return true;
            }
            return false;
        }
    };


})($);